module Exceptions
  class ErrorWithDefaultMessage < StandardError
    def initialize(*args)
      msg = args.shift
      msg ||= self.class.default_message
      super(msg, *args)
    end

    def self.default_message(message_text = nil)
      if message_text
        @message_text = message_text
      else
        @message_text
      end
    end
  end

  class JsonError < StandardError; end
  class InvalidCredentials < StandardError; end
  class ServiceAlreadyConnected < StandardError; end

  class RecordInvalid < ErrorWithDefaultMessage
    default_message "Invalid record"
  end

  class NotAuthenticated < ErrorWithDefaultMessage
    default_message "Not logged in"
  end

  class NotAllowed < ErrorWithDefaultMessage
    default_message "Operation not allowed"
  end

  class InvalidParams < ErrorWithDefaultMessage
    default_message "Invalid Params"
  end

  class InvalidUidAccessToken < ErrorWithDefaultMessage
    default_message "Uid and access token does not match"
  end

  class InvalidAppToken < ErrorWithDefaultMessage
    default_message "Token mismatch"
  end

  class TokenError < ErrorWithDefaultMessage
    default_message "Wrong token"
  end

  class TokenSecretError < ErrorWithDefaultMessage
    default_message "Wrong token or secret"
  end

  class EmailIsTaken < ErrorWithDefaultMessage
    default_message "Email is taken"
  end

  class EmailNotFound < ErrorWithDefaultMessage
    default_message "Email not found"
  end

  class TokenNotAuthorized < ErrorWithDefaultMessage
    default_message "Invalid token"
  end

  class MessageError < ErrorWithDefaultMessage
    default_message "Cannot view this messages"
  end

  class SearchRequestFailed < ErrorWithDefaultMessage
    default_message "Search request failed"
  end

  class LeaveTribeFailed < ErrorWithDefaultMessage
    default_message "Cannot leave your own tribe."
  end
  
  class JoinTribeFailed < ErrorWithDefaultMessage
    default_message "User already joined this tribe."
  end

  class NotTribeMemberError < ErrorWithDefaultMessage
    default_message "User does not belong to this tribe."
  end

  class AuthenticationFailed < ErrorWithDefaultMessage
    default_message 'The username or password is not correct'
  end

  class NotImplemented < ErrorWithDefaultMessage
    default_message "Feature not implemented"
  end

  class NoTwitterCredential < ErrorWithDefaultMessage
    default_message "Twitter credential not found"
  end

  class AbstractMethodCalled < ErrorWithDefaultMessage
    default_message "Tried to call abstract method"
  end

  class RoutingError < StandardError
    attr_reader :request

    def initialize(request, *args)
      @request = request

      super(build_message, *args)
    end

    def build_message
     "Method not allowed for resource"
    end
  end

end