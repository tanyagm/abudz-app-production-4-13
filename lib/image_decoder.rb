module ImageDecoder
  extend ActiveSupport::Concern

  def decode_string(image_data, content_type, filename)
    decoded_data = Base64.decode64(image_data)
 
    data = StringIO.new(decoded_data)
    data.class_eval do
      attr_accessor :content_type, :original_filename
    end
 
    data.content_type = content_type
    data.original_filename = File.basename(filename)
 
    return data
	end

end