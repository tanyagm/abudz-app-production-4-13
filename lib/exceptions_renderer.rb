module ExceptionsRenderer
  extend ActiveSupport::Concern

  def render_error_json(exception, status, more_attributes = {})
    json = more_attributes.reverse_merge!(error: exception.message)
    render json: json, status: status
  end

  def render_bad_request_error(exception)
    render_error_json exception, :bad_request
  end

  def render_unauthorized_error(exception)
    render_error_json exception, :unauthorized
  end

  def render_forbidden_error(exception)
    render_error_json exception, :forbidden
  end

  def render_not_found_error(exception)
    render_error_json exception, :not_found
  end

  def render_method_not_allowed_error(exception)
    render_error_json exception, :method_not_allowed
  end

  def render_conflict_error(exception)
    render_error_json exception, :conflict
  end

  def render_unprocessable_entity_error(exception)
    render_error_json exception, :unprocessable_entity
  end
end