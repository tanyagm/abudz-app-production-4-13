namespace :notif_setting do
  desc 'Assign default notification setting value'
  task set_default_notif: :environment do
    User.all.each do |user|
      NotifSettingService.new(user)
    end
  end
end
