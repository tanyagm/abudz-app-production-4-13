namespace :activity do
  task reset: :environment do
    $redis.keys("act:*").each { |key| $redis.expire(key,0) }
  end
end