require 'csv'

namespace :user do
  task seed_from_csv: :environment do

    CSV.foreach('public/gotribal-data.csv', :col_sep => ',', :headers => true) do |row|
      email = "activebox.me+#{row['first_name']}#{row['last_name']}@gmail.com"
      puts email
      entries = Dir.entries "public/images/profile/#{row['gender']}"

      random_filename = ""
      while not random_filename.end_with?(".jpg")
        random_filename = entries[rand(entries.count - 2)+2]
        random_filename ||= ""
      end
      avatar_base64 = Base64.encode64(open("public/images/profile/#{row['gender']}/#{random_filename}") { |io| io.read })
      params = ActionController::Parameters.new({
        email: email, 
        first_name: row['first_name'], 
        last_name: row['last_name'], 
        password: row['password'], 
        password_confirmation: row['password'],
        avatar_filename: "avatar.jpg",
        avatar_content: avatar_base64
        })

      begin
        user = UserCreationService.new(params).create
        if user
          user_detail_service = UserDetailService.new(user)
          user_detail_service.set('lat', value: rand * 180 -90)
          user_detail_service.set('lon', value: rand * 360 -180)
          user_detail_service.set('gender', value: row['Q2'])
          user_detail_service.set('gender_preference', value: row['Q5'])
          user_detail_service.set('role', value: row['Q3'])
          user_detail_service.set('role_preference', value: row['Q4'].split(/, */))
          user_detail_service.set('fitness_level', value: row['Q7'])
          user_detail_service.set('weekly_activities', value: row['Q8'])
          user_detail_service.set('lifestyle', value: row['Q9'])
          user_detail_service.set('age_group', value: row['Q10'])
          user_detail_service.set('workout_time', value: row['Q1'])
          user_detail_service.set('fitness_pursuit', value: row['Q6'].split(/, */))
          user_detail_service.set('living_place', value: [row['Q11']])
        end
      rescue
        puts "Failed to create user"
      end
    end
  end

  task reset: :environment do
    User.destroy_all
    UserDetail.destroy_all
  end

  task seed: :environment do
    100.times do
      params = ActionController::Parameters.new({
        email: Faker::Internet.email, 
        first_name: Faker::Name.first_name, 
        last_name: Faker::Name.last_name, 
        password: "password01", 
        password_confirmation: "password01",
        avatar_filename: "dog.jpg",
        avatar_content: "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxQSEhUUEBQUFBQUFBUQFBQUFBQXFRUUFBQWFhQVFRUYHCggGBolHBUUITEhJSkrMC8uFx80ODMsNygtLisBCgoKDg0OGhAQGywlHCQsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLP/AABEIALEBHAMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAADBAECAAUGBwj/xAA/EAABAwIEAwYDBgUDAwUAAAABAAIRAyEEEjFBBVFhBhMicYGhBzKRI0LB0eHwFBVSsfFicpKCotIkM0NEVP/EABoBAQEBAQEBAQAAAAAAAAAAAAABAgMEBQb/xAAhEQEBAAIDAAIDAQEAAAAAAAAAAQIRAyExElEEIkETMv/aAAwDAQACEQMRAD8A1dBiepNS1EJ2i1EMU2ozGqKQRwERZgTNIITGphgRRWIzVRoRWBaiCsR2oLUVqqChWaqSpQFBVgUMK4QXBWSqrEVaVMqixBeViopQWWKsrJQSqkKywhAEhUcEctQ3NRC7ggOCacEF4QKvCWqNTjwgVAikajUrVanqjUvUapVa2q1JVmrZ1GpOs1Q21dVqVcE/ValHNUU/QKfopCiE9RRKcplMMCXpBNMCINTCOAh0witCLBqaMEJgRgFYlWajBCCuCtIKCrNKHKs0oChWCo1abi3aelQOUnM7+lu3mdlLZPVkt8b1YSvOMd8SHGW0aeV0kS65AG60PEe0+JcP/cLSeXKR9LAe6z/pP438K9dxOOp0/ne1vmUGrxmg3LNRvigAAyb6LxTG8RdJLqjnkQJcTm0Mb/uArOxnhzCc0NOvOSBprYabbLNzrUwj3KhimP8AkcDaY3g6GEZonReb8A422o1orPc18DxA5XZjYQQNyTa9/QHYu43WEMqVPC75HtLYdFw0kD5tehAtKzeayeLOLd9dXxDiDKBioQBlc7NNvDEidzcFK4jtFh6fzvA94tMW8wtT/FCue7xDW/KWNcDLfmm82Bvcf6eQXL9pqVTB+Jrc9FznXHivlgRvY78ztosY82VavFjHT8S7f0aY+xY+qb3iGxoDJ1krmn/EutnlzGtYPunnuTvoDZeeYvjD32BgcmiP3qVqjUaZkkyuv7Vysxj2bDfFajYPpuFrkEEk8o0H+FvuG9ucHWsKgaeTrAdJK+fWVWeSI1oOjlrdiaj6da5rhLSHDmCChvYvn/gfaLEYY/ZVHAb3BH0K6mn8TMS0XbSqDmWuB9YKfNPi9Pe1L1AuT4d8SqD2/btcx3+kFw+i6XAcTpYhuei8OHuPMaha+UqaVqBLPCcqBLPSlI1QlKoTtUJSqoNdWCTc1P1glHKNGqQTlIJSknaQRKbpBNUwl6SapoGGBGYEJqOwIkFaFdVarqxESpDlVxWp4/xhuHpkkjMR4RuT+St6PW1rYxjPnc1vmQEfhWKZVJIJyN1MWMAmPZeWYKqcS+arpc8gAaEDc8ohdljcYabWYbDkyQASzMbR8wI6bcwuGfLfI748X2ntd2wmaOFDWgeF9UanTws5akE8153UkyS0uJ1MTtMdD/5QvQKXBKdIQ6DIEQRmBGnyzeLmAQb2Flqsa4TYgzDWmbPgGxJAGpNjvPymZxtvTz1+IiddIy3mb2M63/eiWqVTnaCHQ0NgDr4svuR6LruJdnhUl7fCTLoAMgyDYnzK1fBcBDnaEte6mDMEFgJbHKbQenmt7jNxovBeBmqS5+YA5nBsG0hpbPMn00C6zC9n6IcJyGwNSTYCCDBEATFjsDpJlaXG4wUwchsA1oF/F4SZ1s2bj/p1lO8O4VjarM/eCk1twACdj18Wjtd2wsW1vUjoD2bp1BGUh0QC2WmLjM0EWbc3O172nVY7h9agzK8GpRJBcRnzN6wDMC+npEGFmcRxWHq5BWY8gAua+ziZMSY1iNf7LqOG9pXf/ZoP8UNDgczcpHl+PKdEt+zX0V4XRFVn2bu+YQBLzD4NgHECZ0k2PhgnQqcVwyoWljn52nMHZoDmyB4WunrHW/rs8LgsK5xqUKjGuJMttEaZXN2uBy0Gt1v8HQzNhwadrRG8aT+z9eW+23z12n7NuoEuYC1v9JufU891zL6RGoX0zxjgYcwtcBBB8RvE6kga+2i8X7Vdmzh8Q2m4zTcZD4236/gu+Ge3HLGOOo0MxhWxGFdTW+4lwrupeyIbci5iTAjmL6quDe2qIfEbkzqukrGmioYjYpvDVb5funUnmrcX4YGGaZkcv3qtfTelibP1GlriDsYXonwodLq3LK30N15+wZmTu2x6jYr0P4SUvDXfvLW+0qT1a7iqlXpyqlKi6uZWqEnVCcqpSqFAhWCUeLp2slHBRqGKKdpJKknaSIcppqmlKaZpoU2xHYUvTKOxVB2orAhMC4ntt2wNOaOGcM2j6gPy9Gnmm9QkbjtT2qpYXwsipW/pB8Lerj+C824rxR1Q97iHSTdrJ1jS2zVrqVSc1WoSYvc3JOn1K0WLxLqjy5xkn/AAXO7ybmsXpXY2pUINUH7RxLKbCIaWxJOYXEeRXQ4PFtoNc+oC+s8+Fs+J3N0DQTfbZJ9mmnDYNmYML3NmHZg1ojVxi3lvyXL9oePgE5DmqE3qja/yskAt8h0XCftenousZ27LD8KxGMDn1avdtMloYZgbBxGoPQ+xWpOCfh6ppVSXGM7DchzASHOB1nwm3+Ex2I7RBuHNIOk/NpfxEXIG0zfrdD7dY4d2HBxL6Rhp0zAw17bX1G+4VsSVsRky3d4HAOBE2y+GJ01PsFxvCXAF5MQamYxNiHReOUz6JqtXqvacoAzEjxRDRLt99DpzHVazhvDauYvEOBL6ZA2IJE8xcgjz81ZC1usJhhUxLGPOZrQXmL2YIOlpkT1sF1/G+NsYy0iG5XS7SCc0bQQ50CPvBee4l9TD1W14IGU0nAWyO0zN1zCw57JTj/Fe8ZAdM5gHajRogHUaMN1qRi0jx3iwfWNWmYkk3uXDY3/Fb3hHb11MBuR8fKcg1aGxDidYnpquEZRLz+/otlSoukMpB5eRZlIOc82gm14/MreoxuvUeF9ssO4jvcM4NIBAytGW+oawkuFrH812/A+J0q7Zw7jmHzMcCHAWvBFtl4Vj+DY6g0PfhMSxsTndTfAFzJjTU6pbhnH3U3BwmWkEODiCIgWI2tosZ8W3THkfSrcRnad411EbXC0fEeENxjDTqZbXL3Zi6RcXBuNFpezPaz+Iyl4bmtcDXWCZ0P6259S+s0RJF9TGUHlOZefvGuvVeO8Y4bWwhdTrtzUXExUAs6D4QTsucqYAteWtgSJ8JMfWV9D46iyowte1rg4aEW9AAvJO0PZ3u6rv4fMwiXAE20khs6jRdseT7c7g4h9OrTP2mbLBE6hei/D3geBxmDc19FnfNJa99899HTsuUbxYu8D4BFjLSfW+y7L4ZtDHVsgEOIJiwHkus9crOnD8f4A/AVzTfem+e7qbPadR/uH6rvfhgyMM886pE+QC6viuBp4imaddgew3g7EaEHY9UHAYGnQZ3dFoa2ZgczuVv49s29L1UtVR6hS9QqoWqpSqmqqUqlQpOslHJqslXIsHpJ2kkaRTtIoHKSZppWmUywolNU0yxKMKYpuRHP8AxF4q/D4X7M5TUd3ZduBBJj6LyPvpF1698QuGGvgn5bupxVA3OXUD0XiHeKX1qH8bU+zA5uJPpoPcpLA0M9RrbXcNdIm8o1SpLAj8HpGXOH3WOP0WfIs7rfcf7R54Y0ywCCBF4EDrp1WjrllQS3wuvIyiCOYMpOg5pqTVDntBu1pgn12Xb8N4VgMWyKAfQrNEjxEkHbM0khwtskxmMa3c65DhWMdTkEwJgj9+S3WLxtSqQ6p8gu1p1JGaPcT+yq4LhUVHOqtFnFkWuWnVvP6bJbiVeXwYDdRHLl5WUvZOvT2I4wT8t7W6DQD/AIk+yVp8SqAEOJ8RzTN8xgkzGtvdK4Z0CpqRmgDy+ZUxNcmqQAYaBbYWTS/I2/jDpOY3MgTy5hL1sG2qTkOXcA2B6AeU+cJerUaSRfKSDpp1EEwg0sQWGxJaN9bdQbFVNneH0BTL+8sQ2RuDDhp7rqeEcbZw2iH0QH16ky9wvPXoOQK0GExNOpAjUyT6/wBOgN0DjOFcAIlzRoRJsf8ACm7tqakrZca7UYjFsz161R3iyWqZQDEwKbSDEbxHWVzdVpLxBzE6E6noTulw/wDymMFRc54DYnUSYEi4k7LtlluOGM7P8M4m+g4ESAYdAnUiRp5gr0rgPak4g3aZAkyeUCSZ8QidfNeX8Wqh9R3dfI1xFOBEUgYb5ag+q3nYFzm1S4BxMEbwDsTaI19YXn5MZY74ZWXT2VuLOXMDlBEEhskRrcQD6LkuPVAHCoCAWyYkwQdZE2Guq2Ja5zS7QxMkHMfJ0RpP0Wl4gM9ajTgkZmudO95/crzT16L1C+D7L/xdcVH95TYWgl2WM/8AscRp19l3uAwFKgzJRaGt13JJ5km5KZBsEJ7l7sZqPFbtjnIT3KZ5oT3LSaUeUvUKK8oDyooFQpSqmaiUqFEpWqUs4piqlnFSqLTKcpFJUymqRSB6mUywpOmUzTKobYUdjkqwozSgcZUHRedduPh/mzV8GPEfE+lsTuWcj0XfNciNedoHr+GiI+eKDSxzmVAWkeEtNiD1Cf4W1wNRoDi0scCQLAkWk6CYXpXxA7MjEUnVqdMd9TGbMy2do1aWkXMaEFcb8O6metWoHTEYZ7ZvZ7PEwmNtbrNjUrk3iA0a69Lz/hNcIrOZWGU3MtPr+sK/FMM6k4hwsSbdWmDHIgzdUwTYl3pPOdk3vE1rJvK+MIGVhLZmb6Tq4gAX6X09VqKdbM4zBGni5dSg165dMWGvX9VfBUr7qNb2PWcGtLaZtOURInnrzv7qTUkXsR+CV4jZ8X531g3Bja0JqiQ4H1sL26pSEKlMSYJ0B6QUudVtHEB3iAuCJ/shVIkACfvX0kXKbNE2OIMtsZ2W+weNzscCMz9bmzdzrPW28rVGINrz6RpEKaZcx0tsfM2vrrB5XlDxNZrA7xsM7+KCfZCbWtlpiJ1iZNtymcUDVAfmk/KQTpHK+l/JMYelmIlhJkNhoaAXZTAywLyddPOLtpoTDcOyi4OYi3MzyjYjy0MEr1T4f8FZEikXmSC4xln72XcwSRFvPYcTw/AF9WKhPh8AI5gNnMc3WNbZutvaOyWHa2i1waGt2mL21nedfVYy76dJ121/FGsw9QNqAAPDskbGL6T72XM4Nn/qsztriXchAA566Bdp22wHe0A9lnMc14uQfDrEFcXw3ETWBygSDJ3Jje567rnqStbtjpHVCdAfM29tf7Kkc7n96LDXadCD0Fz9AqFxOgjqfyH6L1POxxQiZViOdz1/JUcUA3lAeUV5QHlAGoUpUKYqFK1SgWqFLOR6hSzipQWmU1TKSpFM0yoH6ZTLCkqTk1TK0G2FGaUswozEDDSiNKC1XzAalAy1y1uG4NhsO99WlSFOo6TmbY9Q3aDyRatcxIlo/qsLak+LQQDsUtUY5zTlDX7jO01HHyeQGt9ZSpp5127wfjY6AS8EvIAu4mSbfvRctXGXwhdd2xf4AcuUnWCInyAj6ALinmfxXLDx1z9Xp3uVlSvHy66WQqjrI+FhoE66+X7gfRbZQ3BudcqWYKoLtI+pHon6eIERyh0721U0sUIjl7qLoh3jmu+0Ex6gx13CmhVhzi23Iee3knqsVJ33EwPNamq3JG83t+KF6FdUadbGPdULuov9fVYHAjRELgBayIii51J82O5GoK6XCV21G+PKS0WcSbHbNDSBaNZ0Fxdcz3s73mdN1ssPxEBmQi9zmtMHlbnrf0UrUdTwnBxVh05TElscpNoIMxzXomB7QUqWWmwuuLzAi4sOXX8dV49heKuAyNnoSdJ5QbkXFlsMDxxwhjbOmc4Lpd53v+nRc9Om5p7Xi+KTTO5iNeW+gO643BYdn8QA0DQm4GaSQg4fib6jGtuXEQANfSAPeVueB9nazT3lUsBvILpI87fiueWWmpicc5DJWyw+Ba9odIvMGTHQ22SvFMJ3N3EARIMiD05zcLrjz41yvDlCbihuKsXITiu7ko8oDyiuKA8oAVClapTFUpSqgXqFLOKPVSzisg1Jiap01ybOLVf6/YfkmKfG6o+/7N/JXQ7CjSKNVxVGiM1eoGgWjVxJ6BcnT7QVv6/+1v5INXEZ7vvcnU6nVTKWzprHW+3Z4PidFzvES2mYyu66yRrCLX4tRogmoSQXZW5QTPUNFzr7Lig9pbECNdT/AHRe8aQ2R8ohutlxvFnetuv+mE/jqqfH6Jqhk+F/yuJtJ+6fb6rdupfdaPPkB157W5cl5uaVOxyiRca28k9/OsRtWeItaF1wlk1XLKy3p3n8JJk+Itg3562Gg2TItsZOgi58vz0Xmb+LYj/9FUeTyP7KtKpi6hJZVxLuZa+odNLgrVsnrOtkfiLRdTqljrCS4codfWL3nRcJ3sErv8fw6s8zXFZ5FpqZ3R6u0WtPDG7AfQLOMn8aycmKkkdEbvF0h4YOQ+gVf5eOQWrGWgLouCq55Elb92BHL2VP5f0H0TS7aI1Xc9NFdlaQQ652W3PDxy9lB4aOQU0bc7caK5fOtlu/5aOQVHcKbyCqNK2pyTVOqIuPI6weqf8A5Q3Ye5VxwcdVFa/D1XEgMEne31Hlouk4BgTdzzldIte7dxy+vulsLhDT+Qx9JTWHruZpfzJWcpWsbJ69i7I8Gptpio4gveLE7t5gbCynH4sd82iNHnK4c2gEuFtrQvOsD2xrUqjXkBwY3I2nJDAIiwVqnavNU73KWvAOW8gE7+68ufBbdvTjy4zp7ASxtMlpEBs+Vv7fkvPuMYp+KLKYdlDKkHMbtB1d6CfZaTg/bWpQbUa4OrF83eYiegELS/x5Ly+XBx1M8xBVx4LGbzTb2E0KTKbQ8w2Ia7X1PokK+Eg2uNQRuFwP89quYGOcSBOpvfmd0XBcfrUgAx1m2AdeOl/NdeLjzwY5M8cnYOw55IL6B5Fc9U7XYk/faPKmz8kpV7SYk/8Ayn0DR/YLvquDpKmGPIpWrhXciuYrcZrHWtU/5uSNbGvOr3HzcSmh09ehGpA8yAknuZN3t/5Bcy+qgF6mgNrkVrko1yMwrQaa5GY9KNciNcgcbURBUSjXIrXID510vZ3s2a4D6ri1nJvzEc76Bcux17/Rd7wbiLKgBZUaBu20gm2i8f5nLnx4z4vT+Px4539m2pcKwtEeCkHO1Ga5gbklNYXHz4GtY0bZREc7brUV8d4w0CTpIUO40A64Gdogc/0XxM8s8u7X08cMZNSN73jgCWnPGoFtOhXL9reF03NNWnlY8DMQLZxvYbpp3EOZIlt7gklc9xTi1NjHGo6CZDW6yXW9F0/HyzxznxY5cMbjfk0iiFVrpVsy/RvjIyqparLEFC1VLAiLEAu7VhSCIplQVyhVIVpUFAMtUhikqUEGkChmgEaVCoF3QVg0K8qCUGSsLlBKGSgu56E+qqOKE4oLPqILnqHFDcUGOehl6hxQi5QY0orHJcFEaUDLSitKWaUZpVBwVcFBBV2lAYmy57EtfTeXMc5p5greyg1qYKzRr8P2jxLNHzH1+qYHaurcloLjqZQamCCEcEuV4eK+4us5eSeUxU7T1iIaA2epKUoZ6rw6o4uPX8kdmCWwwlANWscMMf8AmJlnll7WyYIAVgqNNlIK6RzWUyq5lMqiViiVkqbFpUFVlYXIJlZKpKmVBJUSqkqJQXlQSqyoJQSSqlyglVc5XYkuQ3OWEoZKbGOchOcpJQ3FUQ5yE5ylxQnFQQ5yGSpJQyVBZpV2lBBV2lUMMcitcl2lEaVQdrkRpQAVcOUBw5ZKGCrBBJCzKsCsFBLWojVQK4KKMHKZQmlXzIi4KmVSVibF5WSqyslBMrJUKpKCykKisCgxyqpJVJQTKglVJVSUEucqkqCVUlBjiqErCVQlUQ4oTlZxQnFBDihuKlxQ3FBDihkrHFUJUFwrtWLFQRqKFixUXarhQsUBGq4WLFBYKQsWILqwWLEFgpWLFRYKVKxQQpCxYgwqixYkaxSFJUrESqlUWLERBVSsWKihVXLFigqUMqViAZQnLFiAbkNyxYgE5UWLEH//2Q=="
        })
      user = UserCreationService.new(params).create
      if user
        user_detail_service = UserDetailService.new(user)
        user_detail_service.set('lat', value: rand * 180 -90)
        user_detail_service.set('lon', value: rand * 360 -180)
        user_detail_service.set('gender', value: rand(2))
        user_detail_service.set('gender_preference', value: rand(3))
        user_detail_service.set('role', value: rand(3))
        user_detail_service.set('role_preference', value: [rand(3),rand(3)])
        user_detail_service.set('fitness_level', value: rand(4))
        user_detail_service.set('weekly_activities', value: rand(3))
        user_detail_service.set('lifestyle', value: rand(3))
        user_detail_service.set('age_group', value: rand(3))
        user_detail_service.set('workout_time', value: rand(3))
        user_detail_service.set('fitness_pursuit', value: [Faker::Lorem.word,Faker::Lorem.word])
        user_detail_service.set('living_place', value: Faker::Address.city)
      end
    end

  end
end