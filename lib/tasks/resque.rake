require 'resque/tasks'
require 'resque_scheduler/tasks'

task 'resque:setup' => :environment

task 'resque:clear_workers' => :environment do
  Resque.workers.each {|w| w.unregister_worker}
end
