namespace :user_token do
  desc 'Remove expired token'
  task remove_expired: :environment do
    UserToken.expired.each do |token|
      p "Deleting token #{token.token}"
      token.destroy
    end
  end
end