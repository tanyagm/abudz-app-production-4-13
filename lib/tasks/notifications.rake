namespace :notifications do
  task send_scheduled_notifications: :environment do
    NotificationService.new.schedule_all_notification
  end
end