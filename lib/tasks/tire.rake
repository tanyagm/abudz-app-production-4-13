namespace :tire do
  task init: :environment do
    SearchService.init_index(ENV['VERSION'])
    SearchService.init_type(ENV['VERSION'])
    SearchTribeService.init_type(ENV['VERSION'])
    User.find_each do |user|
      puts "user: #{user.first_name}"
      SearchService.new(user).create(ENV['VERSION'])
    end
    Tribe.find_each do |tribe|
      puts "tribe: #{tribe.name}"
      SearchTribeService.new(tribe).create(ENV['VERSION'])
    end
    SearchService.set_index_alias(ENV['VERSION'])
  end

  task reindex_user: :environment do
    User.find_each do |user|
      puts "user: #{user.first_name}"
      SearchService.new(user).create
    end
  end

  task init_tribe: :environment do
    SearchTribeService.init_type
  end

  task reindex_tribe: :environment do
    Tribe.find_each do |tribe|
      puts "tribe: #{tribe.name}"
      SearchTribeService.new(tribe).create
    end
  end

  task seed: :environment do
    users = []
    gender = ["male","female"]
    role = ["mentee","mentor","buddy"]
    workout_time = ["morning","night"]
    favorite_fitness = ["fitness","running","cycling","surfing","swimming"]
    fitness_ability = ["professional","advanced","intermediate","beginner"]
    weekly_activity = ["training","sleeping"]
    lifestyle = ["single","married","fullhouse"]
    age_group = ["20s","30s","40s","50s","60s"]
    living_place = ["bandung","jakarta","yogyakarta","surabaya"]

    20000.times do 
      favorite_fitnesses = []
      3.times do 
        random_favorite_fitness = favorite_fitness[rand(favorite_fitness.length)]
        favorite_fitnesses << random_favorite_fitness if favorite_fitnesses.exclude? random_favorite_fitness
      end

      workout_times = []
      2.times do
        random_workout_time = workout_time[rand(workout_time.length)]
        workout_times << random_workout_time if workout_times.exclude? random_workout_time
      end

      users << {
        type: 'user',
        first_name: Faker::Name.first_name,
        last_name: Faker::Name.last_name,
        email: Faker::Internet.email,
        gender: gender[rand(2)],
        role: role[rand(role.length)],
        workout_time: workout_times,
        favorite_fitness: favorite_fitnesses,
        fitness_ability: fitness_ability[rand(fitness_ability.length)],
        weekly_activities: weekly_activity[rand(weekly_activity.length)],
        lifestyle: lifestyle[rand(lifestyle.length)],
        age_group: age_group[rand(age_group.length)],
        living_place: living_place[rand(living_place.length)],
        location: {
          lat: Faker::Address.latitude,
          lon: Faker::Address.longitude
        }
      }
    end

    Tire.index AppConfig.elasticsearch.index do
      import users
    end
  end
end