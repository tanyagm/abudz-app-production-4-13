namespace :friendship do
  desc 'Friendship rake task'
  task accept_all_pending_request: :environment do
    FriendRelation.includes(:to_user, :from_user).pending.each do |relation|
      to_user = relation.to_user
      from_user = relation.from_user
      p "Accepting friend request from #{from_user.fullname} to #{to_user.fullname}"
      FriendshipService.new(to_user).add(from_user)
    end
  end
end