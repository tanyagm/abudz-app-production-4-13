module EsAdmin
  class Admin
    def initialize(index_name)
      @index_name = index_name
    end

    def self.all_indices
      aliases = Tire::Configuration.client.get(Tire::Configuration.url + '/_aliases').body
      MultiJson.load(aliases).keys.sort
    end

    def self.all_types(index_name)
      types = Tire::Configuration.client.get(Tire::Configuration.url + "/#{index_name}/_mapping").body
      MultiJson.load(types)[index_name].keys.sort
    end

    def self.all_keys(index_name, type_name)
      Tire.index(index_name).mapping[type_name]["properties"].keys.sort
    end

    def self.simple_search(index_name, type_name, key_name, keywords)
      query = {
        :from => 0,
        :size => 1000000,
        :query => {
          :query_string => {
            :fields => [key_name],
            :query => keywords
          }
        }
      }
      tire_results = Tire.search "#{index_name}/#{type_name}", query
      tire_results.results
    end

    def all_keys(type_name)
      EsAdmin::Admin.all_keys(@index_name)
    end

    def simple_search(type_name, key_name, keywords)
      EsAdmin::Admin.simple_search(@index_name, type_name, key_name, keywords)
    end
  end
end