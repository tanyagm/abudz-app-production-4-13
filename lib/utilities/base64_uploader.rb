module Base64Uploader
  extend ActiveSupport::Concern

  DEFAULT_ACCESSORS = {
    image_data: :image_data,
    filename: :filename,
    content_type: :content_type
  }

  included do
    cattr_accessor :carrierwave_accessors
  end

  private

  def decode_image_data
    column    = self.carrierwave_accessors[:column]
    accessors = self.carrierwave_accessors[:accessors]
    
    decoded_data = Base64.decode64(self.send(accessors[:image_data]))
 
    data = StringIO.new(decoded_data)
    data.class_eval do
      attr_accessor :content_type, :original_filename
    end
 
    data.content_type = self.send(accessors[:content_type])
    data.original_filename = File.basename(self.send(accessors[:filename]))
 
    self.send("#{column.to_s}=", data)
  end

  def image_data?
    accessors = self.carrierwave_accessors[:accessors]
    self.send(accessors[:image_data]) && self.send(accessors[:filename]) && self.send(accessors[:content_type])
  end

  module ClassMethods
    def mount_base64_uploader(column, uploader, accessors={})
      # Set the default accessors
      accessors.reverse_merge! DEFAULT_ACCESSORS

      # Set the accessors to a class variable for future reference when decoding the data.
      self.carrierwave_accessors = { column: column, accessors: accessors }

      # Add the accessors to included class.
      self.instance_eval do
        attr_accessor accessors[:image_data], accessors[:filename], accessors[:content_type]
      end

      # Add a callback to decode the base64 image data.
      before_save :decode_image_data, if: :image_data?

      # The original carrierwave uploader.
      # Need to be placed after the decode callback or it will fail to update the record
      # After carrierwave successfully uploading the image.
      mount_uploader column, uploader
    end
  end

end