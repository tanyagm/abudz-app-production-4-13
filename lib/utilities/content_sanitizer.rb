module ContentSanitizer
	extend ActiveSupport::Concern

	included do
		cattr_accessor :attribute_to_sanitized
	end

	private

	def sanitize_contents
		self.class.attribute_to_sanitized.to_a.each do |attribute|
			self.send("#{attribute}=", Sanitize.clean(self.send(attribute)))
		end
	end

	module ClassMethods
    def sanitize_attributes(*attributes)    	
    	self.attribute_to_sanitized = attributes;
    	before_validation :sanitize_contents
    end
  end
		
end