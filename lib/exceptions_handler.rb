module ExceptionsHandler
  extend ActiveSupport::Concern

  included do
    include ExceptionsRenderer
    rescue_from StandardError, with: :render_bad_request_error unless Rails.env.development?
    rescue_from Exceptions::NotAuthenticated, with: :render_unauthorized_error
    rescue_from Exceptions::NotAllowed, with: :render_forbidden_error
    rescue_from Exceptions::InvalidParams, with: :render_bad_request_error
    rescue_from Exceptions::InvalidUidAccessToken, with: :render_bad_request_error
    rescue_from Exceptions::TokenSecretError, with: :render_bad_request_error
    rescue_from Exceptions::TokenError, with: :render_bad_request_error
    rescue_from Exceptions::EmailIsTaken, with: :render_conflict_error
    rescue_from Exceptions::EmailNotFound, with: :render_bad_request_error
    rescue_from Exceptions::InvalidAppToken, with: :render_bad_request_error
    rescue_from Exceptions::TokenNotAuthorized, with: :render_unauthorized_error
    rescue_from Exceptions::JsonError, with: :render_bad_request_error
    rescue_from Exceptions::RoutingError, with: :render_method_not_allowed_error
    rescue_from Exceptions::RecordInvalid, with: :render_unprocessable_entity_error
    rescue_from Exceptions::MessageError, with: :render_unprocessable_entity_error
    rescue_from Exceptions::SearchRequestFailed, with: :render_unprocessable_entity_error
    rescue_from Exceptions::JoinTribeFailed, with: :render_unprocessable_entity_error
    rescue_from Exceptions::LeaveTribeFailed, with: :render_unprocessable_entity_error
    rescue_from Exceptions::NotTribeMemberError, with: :render_forbidden_error
    rescue_from Exceptions::AuthenticationFailed, with: :render_unauthorized_error
    rescue_from Exceptions::NotImplemented, with: :render_bad_request_error
    rescue_from Exceptions::NoTwitterCredential, with: :render_forbidden_error
    rescue_from Exceptions::AbstractMethodCalled, with: :render_bad_request_error

    rescue_from Twitter::Error, with: :render_bad_request_error
    rescue_from ActiveRecord::RecordNotFound, with: :render_active_record_not_found
    rescue_from ActiveRecord::RecordInvalid, with: :render_invalid_record
    rescue_from ActiveRecord::RecordNotUnique, with: :render_not_unique_record
  end

  def render_invalid_record(e)
    message = e.record.errors.full_messages.join(", ")
    error = Exceptions::RecordInvalid.new message

    render_error_json error, :unprocessable_entity
  end

  def render_not_unique_record(e)
    error = StandardError.new("Duplicated entry")

    render_error_json error, :unprocessable_entity
  end

  def render_active_record_not_found(e)
    e = StandardError.new("Resource not found")
    render_error_json e, :not_found
  end

end