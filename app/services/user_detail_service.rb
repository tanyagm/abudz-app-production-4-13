class UserDetailService
  def initialize(user)
    @user = user
    @details = user.details.to_a
    @question_set = QuestionSet.instance
    @available_fieldnames = @question_set.fieldnames
  end

  def set_mass(params)
    user_detail_params = params.select{|key, value| @available_fieldnames.include? key }
    UserDetail.transaction do
      user_detail_params.each { |fieldname, detail_params| self.set fieldname, detail_params }
    end
  end

  def set(fieldname, params)
    raise Exceptions::InvalidParams unless params.kind_of? Hash
    @fieldname = fieldname
    @value = params[:value]
    @is_private = params[:private].to_i
    create_or_update_user_detail
  end

  def get(fieldname)
    return nil unless @question_set.exist? fieldname
    @fieldname = fieldname
    @detail = fetch_user_detail
    @detail.value if @detail.present?
  end

  def get_private(fieldname)
    return nil unless @question_set.exist? fieldname
    @fieldname = fieldname
    @detail = fetch_user_detail
    @detail.value if @detail.present? && !@detail.is_private?
  end

  private
  
  def fetch_or_initalize_user_detail
    key = @question_set.get_key @fieldname
    detail = fetch_user_detail
    detail = @user.details.where(key:key).first_or_initialize unless detail
    detail
  end

  def fetch_user_detail
    key = @question_set.get_key @fieldname
    @details.find {|detail| detail.key == key }
  end

  def create_or_update_user_detail
    @detail = fetch_or_initalize_user_detail
    @detail.fieldname = @fieldname
    @detail.value = @value if @value
    @detail.private = (@is_private != 0) if @is_private
    @detail.save!
    update_detail_cache
    @detail
  end

  def update_detail_cache
    @details.delete_if {|detail| detail.key == @detail.key }
    @details <<  @detail
  end
end
