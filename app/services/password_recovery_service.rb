class PasswordRecoveryService

  def self.enqueue_job(user_id)
    Resque.enqueue_in(
      AppConfig.enque_delay.second,
      Jobs::EmailPasswordRecovery, 
      user_id)
  end

  def initialize(user)
    @user = user
  end

  def send_reset_password_instruction
    @user.issue_password_recovery_token.tap do |token| 
      UserMailer.reset_password_instruction(@user, token).deliver!
    end
  end
end
