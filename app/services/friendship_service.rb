class FriendshipService
  def initialize(from_user)
    @user = from_user
  end

  def add(to_user)
    @to_user = to_user
    if request
      if request.is_approved?
        :friend
      else
        request.accept 
        NotificationService.send_friend_request(@user, to_user, :accepted)
        :accepted
      end
    else
      if sent_request
        resend_request
        NotificationService.send_friend_request(@user, to_user, :sent)
        NotificationMailer.friend_requests(to_user.id).deliver if to_user.notification_setting.activebudz_request_freq.to_sym == :immediate
        @user.notify to_user
        :resent
      else
        create_new_request
        NotificationService.send_friend_request(@user, to_user, :sent)
        NotificationMailer.friend_requests(to_user.id).deliver if to_user.notification_setting.activebudz_request_freq.to_sym == :immediate
        @user.notify to_user
        :sent
      end
    end
  end

  def remove(to_user)
    @to_user = to_user
    if request
      if request.is_approved?
        request.destroy
        :unfriended
      else
        request.reject
        :rejected
      end
    else
      if sent_request
        sent_request.destroy
        :removed
      else
        add_to_hidden_user_list
        :hidden
      end
    end
  end

  private

  def request
    @request ||= @user.friend_relations_to.from_user(@to_user.id).first
  end

  def sent_request
    @sent_request ||= @user.friend_relations.to_user(@to_user.id).first  
  end

  def resend_request
    sent_request.destroy
    create_new_request
  end

  def create_new_request
    @user.friend_relations.to_user(@to_user.id).create!
  end

  def add_to_hidden_user_list
    hidden_user = @user.user_hidden_users.where(hidden_user_id: @to_user.id).first_or_initialize
    hidden_user.update_expiry
  end

end
