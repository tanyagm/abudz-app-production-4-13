class MessageService
  
  PAGINATION_DEFAULT = {
    per_page: AppConfig.per_page,
    max_id: nil,
    min_id: nil
  }

  def initialize(sender, sender_socket_id=nil)   
    @sender = sender
    @sender_socket_id = sender_socket_id
  end

  def sender_socket_id=(value)
    @sender_socket_id = value
  end

  def list_of_conversation(pagination_options={})
    # set default value
    pagination_options.reverse_merge!(PAGINATION_DEFAULT)
    pagination_options.merge!(cursor: MessageMapping.arel_table[:sort_order])
    
    return MessageMapping.list_of_conversation(@sender.id)
      .paginate(pagination_options)
      .preload(:first_user, :second_user)
  end

  def conversation(mapping, pagination_options={})
    return nil unless mapping

    raise Exceptions::MessageError unless mapping.participant_ids.include? @sender.id

    update_read_status(mapping)

    # set default value
    pagination_options.reverse_merge!(PAGINATION_DEFAULT)    

    return Message.conversations(mapping.message_id)
      .paginate(pagination_options)
      .preload(:sender).reverse
  end

  def send_message(receiver, body)
    return nil unless receiver

    @mapping = find_mapping(@sender.id, receiver.id)

    if @mapping
      reply_to(@mapping.message, body, receiver)
      remove_excluded_conversations
    else
      create_message(receiver_id: receiver.id, body: body)
    end    
  end

  def init_conversation(receiver_id)
    raise Exceptions::InvalidParams unless receiver_id

    channel_info = {
      channel_name: MessageService.message_channel(@sender.id, receiver_id),
      event_name: MessageService.message_event
    }
  end

  def init_notification
    channel_info = {
      channel_name: MessageService.notification_channel(@sender.id),
      event_name: MessageService.notification_event
    }
  end

  def delete_conversation(mapping)
    return false unless mapping

    if mapping.participant_ids.include? @sender.id
      @sender.deleted_conversations.where(message_mapping_id: mapping.id)
        .first_or_create.persisted?
    else
      raise Exceptions::MessageError.new('Cannot delete this messages') 
    end
  end
  
  private  

  def find_mapping(first_user_id, second_user_id)
    user_ids = [first_user_id, second_user_id]

    # Always find by smallest user id first
    return MessageMapping.find_by(first_user_id: user_ids.min, 
      second_user_id: user_ids.max)
  end

  def create_message(params)
    message = @sender.sent_messages.build(params)

    ActiveRecord::Base.transaction do
      message.save!
      create_or_update_mapping!({ message_id: message.id, 
        user_ids: [@sender.id, params[:receiver_id]],
        last_message: params[:body] })
      push_message(message)
    end
    user = User.find_by(id: params[:receiver_id])
    NotificationMailer.messages(params[:receiver_id]).deliver if user.notification_setting.message_freq.to_sym == :immediate
    message.notify user

    return message
  end

  def reply_to(message, body, receiver)  
    return nil unless message

    receiver_id = if receiver
      receiver.id
    elsif @sender.id == message.sender_id
      message.receiver_id
    else
      message.sender_id
    end
    
    raise Exceptions::MessageError.new('Cannot reply to this message') unless message.participant_ids.include? @sender.id

    root_id = message.root_id ? message.root_id : message.id

    return create_message(receiver_id: receiver_id, body: body, root_id: root_id)
  end

  def create_or_update_mapping!(params)
    @mapping ||= find_mapping(params[:user_ids].min, params[:user_ids].max)

    if @mapping      
      update_params = if @sender.id == @mapping.first_user_id
        { first_user_opened: true, second_user_opened: false }
      else
        { first_user_opened: false, second_user_opened: true }
      end

      @mapping.update_attributes!(update_params.merge(last_message: params[:last_message]))
    else
      MessageMapping.create!(first_user_id: params[:user_ids].min, 
        second_user_id: params[:user_ids].max,
        message_id: params[:message_id], 
        last_message: params[:last_message],
        first_user_opened: @sender.id == params[:user_ids].min,
        second_user_opened: @sender.id == params[:user_ids].max)
    end
  end

  def push_message(message)
    Resque.enqueue(Jobs::PushMessage, message.id, @sender_socket_id)
  end

  def update_read_status(mapping)
    return nil unless mapping

    if @sender.id == mapping.first_user_id
      mapping.update_column(:first_user_opened, true)
    elsif @sender.id == mapping.second_user_id
      mapping.update_column(:second_user_opened, true)
    end
  end

  def remove_excluded_conversations
    DeletedMessageMapping.delete_all message_mapping_id: @mapping.id
  end

  # Message pusher constant
  def self.message_channel(first_uid,second_uid)
    user_ids = [first_uid.to_i,second_uid.to_i]
    "presence-#{AppConfig.env}-#{user_ids.min}-#{user_ids.max}"
  end

  def self.message_event
    "received_message"
  end

  def self.notification_channel(user_id)
    "presence-#{AppConfig.env}-#{user_id}"
  end

  def self.notification_event
    "notification"
  end

end
