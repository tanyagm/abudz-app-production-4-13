class ScheduledNotificationService

  def schedule_all_notification
    notification_service = NotificationService.new
    User.has_notifications.find_each do |user|

      notification_settings = user.notification_setting
      yield_by_date(notification_settings.comment_freq.to_sym) do 
        notification_service.send_bulk_comment_notification(user) if notification_settings.comment_type.include? :phone
        send_comments_email(user) if notification_settings.comment_type.include? :email
        counter = user.counters.find_or_create_by(counter_type: Counter::COMMENT_NOTIFICATIONS)
        counter.count = 0
        counter.save
        user.received_notifications.friend_requests.destroy_all
      end

      yield_by_date(notification_settings.activebudz_request_freq.to_sym) do 
        notification_service.send_bulk_friend_notification(user) if notification_settings.activebudz_request_type.include? :phone
        send_friend_requests_email(user) if notification_settings.activebudz_request_type.include? :email
        counter = user.counters.find_or_create_by(counter_type: Counter::FRIEND_NOTIFICATIONS)
        counter.count = 0
        counter.save
        user.received_notifications.comments.destroy_all
      end

      # yield_by_date(notification_settings.activebudz_suggested_freq.to_sym) do 
      #   search_service = SearchService.new(user)
      #   notification_service.send_bulk_suggested_friend_notification(user, search_service) if notification_settings.activebudz_suggested_type.include? :phone
      #   send_suggested_friend_email(user, search_service) if notification_settings.activebudz_suggested_type.include? :email
      #   search_service.search_suggested
      # end

      yield_by_date(notification_settings.message_freq.to_sym) do 
        notification_service.send_bulk_message_notification(user) if notification_settings.message_type.include? :phone
        send_messages_email(user) if notification_settings.message_type.include? :email
        counter = user.counters.find_or_create_by(counter_type: Counter::MESSAGE_NOTIFICATIONS)
        counter.count = 0
        counter.save
        user.received_notifications.messages.destroy_all
      end

      yield_by_date(notification_settings.tribe_post_freq.to_sym) do
        notification_service.send_bulk_tribe_post_notification(user) if notification_settings.tribe_post_type.include? :phone
        send_tribe_posts_email(user) if notification_settings.tribe_post_type.include? :email
        counter = user.counters.find_or_create_by(counter_type: Counter::TRIBE_POST_NOTIFICATIONS)
        counter.count = 0
        counter.save
        user.received_notifications.tribe_posts.destroy_all
      end

      yield_by_date(notification_settings.tribe_invitation_freq.to_sym) do
        notification_service.send_bulk_tribe_invitation_notification(user) if notification_settings.tribe_invitation_type.include? :phone
        send_tribe_invitations_email(user) if notification_settings.tribe_invitation_type.include? :email
        counter = user.counters.find_or_create_by(counter_type: Counter::TRIBE_INVITATION_NOTIFICATIONS)
        counter.count = 0
        counter.save
        user.received_notifications.tribe_invitations.destroy_all
      end

      if user.changed?

        # Delete all saved notification details
        # user.notifications.destroy_all

        user.save!
      end
    end
  end

  def send_friend_requests_email(user)
    if user.friend_notifications_count > 0
      NotificationMailer.friend_requests(user.id).deliver!
    end
  end

  def send_suggested_friend_email(user, search_service = nil)
    search_service ||= SearchService.new(user)
    new_suggested_users = search_service.compare_suggested_results
    if new_suggested_users.present?
      NotificationMailer.suggested_users(user.id, new_suggested_users).deliver!
    end
  end

  def send_comments_email(user)
    if user.comment_notifications_count > 0
      NotificationMailer.comments(user.id).deliver!
    end
  end

  def send_messages_email(user)
    if user.message_notifications_count > 0
      NotificationMailer.messages(user.id).deliver!
    end
  end

  def send_tribe_posts_email(user)
    if user.tribe_post_notifications_count > 0
      NotificationMailer.tribe_posts(user.id).deliver!
    end
  end

  def send_tribe_invitations_email(user)
    if user.tribe_invitation_notifications_count > 0
      NotificationMailer.tribe_invitations(user.id).deliver!
    end
  end

  def yield_by_date(frequency)
    current_date = Date.today.strftime("%d")

    case frequency

    when :daily
      yield
    when :weekly
      yield if current_date.to_i % 7 == 1
    when :monthly
      yield if current_date.to_i == 1
    end
  end
end