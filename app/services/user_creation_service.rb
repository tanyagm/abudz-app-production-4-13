class UserCreationService
  def initialize(params)
    @params = params
    @user_params = prepare_params(params)
  end

  def create
    User.transaction do
      @user = User.create!(@user_params)
      UserDetailService.new(@user).set_mass(@params)
      NotifSettingService.new(@user)
      # TODO: Hack to return the newly created user details.
      # Without this it will return empty.
      # Please fix this.
      @user.details.reload
    end
    NotificationService.new.send_auto_badge_notification(@user)
    SearchService.create(@user.id)
    send_welcome_email
    Bookkeeping::BookkeepingAttachService.enqueue_job(@user.id, ProspectiveUser::SOURCE_EMAIL, @user.email)
    @user
  end

  private

  def send_welcome_email
    Resque.enqueue_in(
      AppConfig.enque_delay.second,
      Jobs::EmailNewUser, @user.id)
  end

  def prepare_params(params)
    # set the required content_type attribute for uploading avatar.
    params.merge!(content_type: 'image/jpg')
    params.permit(
      :email, :first_name, :last_name,
      :password, :password_confirmation,
      :avatar_content, :avatar_filename,
      :content_type, :remote_avatar_url)
  end
end
