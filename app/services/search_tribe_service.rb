class SearchTribeService
  def initialize(tribe)
    @tribe = tribe
  end

  def self.create(tribe_id)
    Resque.enqueue(Jobs::AddTribeToIndex, tribe_id)
  end

  def self.update(tribe_id)
    Resque.enqueue(Jobs::UpdateIndexedTribe, tribe_id)
  end

  def self.init_type(version=1)
    current_index_name = version.present? ? "#{AppConfig.elasticsearch.index}_v#{version}" : AppConfig.elasticsearch.index
    Tire.index current_index_name do
      mapping AppConfig.elasticsearch.type_tribe, :properties => {
            :name => {
              :type => 'string'
            },
            :description => {
              :type => 'string',
              :analyzer => 'english_stemmed'
            },
            :location_name => {
              :type => 'string',
              :analyzer => 'keyword_lowercase'
            },
            :activity => {
              :type => 'string',
              :analyzer => 'english_stemmed'
            }
          }
    end
  end

  def create(version=nil)
    tribe = {
      type: AppConfig.elasticsearch.type_tribe,
      id: @tribe.id,
      owner_id: @tribe.owner.id,
      name: @tribe.name,
      activity: @tribe.activity,
      description: @tribe.description,
      location_name: @tribe.location_name,
      image_url: @tribe.image.url,
      lat: @tribe.lat,
      lon: @tribe.lon
    }

    index_name = version.present? ? "#{AppConfig.elasticsearch.index}_v#{version}" : AppConfig.elasticsearch.index
    response = Tire.index index_name do
      import [tribe]
    end

    response = response.as_json.try(:[],"response").try(:as_json)

    if response.try(:[],"code") == 200
      return tribe
    else
      return nil
    end
  end

  def update(params)
    Tire.index AppConfig.elasticsearch.index do
      update(AppConfig.elasticsearch.type_tribe,@tribe.id,{
        :doc => params,
        :doc_as_upsert => true
        })
    end
  end

  def destroy
    Tire.index(AppConfig.elasticsearch.index).remove(AppConfig.elasticsearch.type_tribe,@tribe.id)
  end

  def self.search(current_user, params={})
    return [] if params[:keyword].blank?

    queries = {
      :from => params[:from] || 0,
      :size => params[:size] || 10,
      :query => {
        :bool => {
          :should => [
            {
              :match => {
                :name => params[:keyword]
              }
            },
            {
              :match => {
                :description => params[:keyword]
              }
            },
            {
              :match => {
                :activity => params[:keyword]
              }
            }
          ]
        }
      },
      :script_fields => {
        :id => {
          :script => "_source.id"
        },
        :name => {
          :script => "_source.name"
        },
        :description => {
          :script => "_source.description"
        },
        :image => {
          :script => "_source.image_url"
        },
        :activity => {
          :script => "_source.activity"
        },
        :owner_id => {
          :script => "_source.owner_id"
        },
        :location_name => {
          :script => "_source.location_name"
        },
        :lat => {
          :script => "_source.lat"
        },
        :lon => {
          :script => "_source.lon"
        }
      }
    }

    Rails.logger.info queries.to_json

    tire_output = Tire.search "#{AppConfig.elasticsearch.index}/#{AppConfig.elasticsearch.type_tribe}", queries
    serialize(current_user, tire_output.results)
  end

  private

  def self.serialize(current_user, tribes)
    tribes = tribes.results.as_json
    tribe_ids = tribes.map { |tribe| tribe['id'] }
    owner_ids = tribes.map { |tribe| tribe['owner_id'] }
    owners = User.where(id: owner_ids)
    current_user_tribe_ids = current_user.tribes.pluck(:id)

    # Problematic eager loading. Need refactor
    tribe_records = Tribe.where(id: tribe_ids).includes(:users)

    tribes.each_with_index do |tribe,idx|
      tribe = tribe.as_json
      owner = owners.find { |user| user.id == tribe['owner_id'] }
      tribes[idx][:owner] = UserSerializer.new(owner).as_json

      tribes[idx][:joined] = current_user_tribe_ids.include? tribe['id']

      tribe_record = tribe_records.find { |t| t.id == tribe['id'] }
      tribes[idx][:users] = ActiveModel::ArraySerializer.new(tribe_record.users.limit(5), each_serializer: UserSerializer)
      tribes[idx].except! 'owner_id'
    end

    return tribes
  end
end