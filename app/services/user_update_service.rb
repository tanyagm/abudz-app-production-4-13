class UserUpdateService
  def initialize(user)
    @user = user
    @user_detail_service = UserDetailService.new(@user)
  end

  def update(params)
    user_params = prepare_params(params)
    User.transaction do
      @user.update!(user_params)
      @user_detail_service.set_mass(params)
      # TODO: Hack to return the newly updated user details.
      # Without this it will return the old user details.
      # Please fix this.
      @user.details.reload
    end
    SearchService.update(@user.id)
    @user
  end

  private 

  def prepare_params(params)
    params.merge!(content_type: 'image/jpg')
    params.permit(:email, :first_name, :last_name, :password, :password_confirmation, :avatar_content, :avatar_filename, :content_type, :remote_avatar_url)
  end

end