class FacebookService

  def initialize(token)
    @token = token
    @graph = Koala::Facebook::API.new(@token)
  end

  def sign_in
    params = authenticate
    if params
      credential = SocialCredential.find_by(
        uid: params[:uid],
        social_type: SocialCredential::FACEBOOK)
      
      if credential
        credential.update_attribute(:token, @token)
        NotificationService.new.send_auto_badge_notification(credential.user)
        return UserUpdateService.new(credential.user).update(params)
      else
        user, status = find_or_create_user(params)

        params[:social_type] = SocialCredential::FACEBOOK
        params[:token] = @token

        user.social_credentials.create!(credential_params(params))
        NotificationService.new.send_auto_badge_notification(user)
        return user, status
      end
    else
      fail Exceptions::InvalidParams
    end
  end

  def get_facebook_uid
    begin
      data = @graph.get_object('me', 'fields' => %w( id ))
    rescue Koala::Facebook::AuthenticationError
      raise Exceptions::TokenNotAuthorized, 'Invalid social token.'
    end

    return data['id'].to_i
  end

  private

  def authenticate
    begin
      data = @graph.get_object('me', 'fields' => %w(
        id first_name last_name birthday username email))
    rescue Koala::Facebook::AuthenticationError
      raise Exceptions::TokenNotAuthorized, 'Invalid social token.'
    end

    params = ActionController::Parameters.new
    params[:uid] = data['id'].to_i
    params[:first_name] = data['first_name']
    params[:last_name] = data['last_name']

    if data['birthday']
      params[:birthdate] = {
        value: Date.strptime(data['birthday'], '%m/%d/%Y').strftime('%Y/%m/%d')
      }
    end

    params[:email] = data['email']
    params[:remote_avatar_url] = "http://graph.facebook.com/#{data['username']}/picture?type=large"

    return params
  end

  def find_or_create_user(params)
    user = User.find_by(email: params[:email])

    if user
      return UserUpdateService.new(user).update(params)
    else
      password = SecureRandom.uuid
      params[:password] = password
      params[:password_confirmation] = password
      return UserCreationService.new(params).create, :created
    end
  end

  def credential_params(params)
    params.permit(:uid, :token, :user_id, :social_type)
  end
end
