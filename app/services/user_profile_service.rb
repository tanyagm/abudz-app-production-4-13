class UserProfileService
  def initialize(current_user, user)
    @current_user = current_user
    @user = user
  end

  def serialized_user_profile_hash
    friends = @user.friends.preload(:public_details).limit(AppConfig.per_page)
    
    serialized_user = if @user.id == @current_user.id
      UserWithPrivateDetailsSerializer.new(@user)
    else
      UserWithPublicDetailsSerializer.new(@user, scope: @current_user)
    end

    return { 
      user: serialized_user
    }
  end  
end
