class FriendshipInvites::EmailInviteService < BaseService::Invites::EmailInviteService 
  protected
  def existing_user_action(params)
    FriendshipService.new(@user).add(@existing_user)
  end

  def new_user_action(params)
    InviteMailer.send_friendship_invite(@user.fullname, @target).deliver!
    Bookkeeping::BookkeepingCreateService.new(@user).create(ProspectiveUser::SOURCE_EMAIL, @target)
  end
end
