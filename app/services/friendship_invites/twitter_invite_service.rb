class FriendshipInvites::TwitterInviteService < BaseService::Invites::TwitterInviteService 
  protected
  def existing_user_action(params)
    FriendshipService.new(@user).add(@existing_user)
  end
  
  def new_user_action(params)
    super(params)
    Bookkeeping::BookkeepingCreateService.new(@user).create(ProspectiveUser::SOURCE_TWITTER, @target)
  end

  def get_message(params)
    "Looking for training buddies or mentors who share your fitness pursuits and ability levels? Try @Activebudz."
  end
end
