class Bookkeeping::BookkeepingAttachService

  def self.enqueue_job_for_credential(user_id, social_credential)
    case social_credential.social_type
    when SocialCredential::TWITTER
      self.enqueue_job(user_id, ProspectiveUser::SOURCE_TWITTER, social_credential.uid)
    end
  end

  def self.enqueue_job(user_id, source_type, source_id)
    Resque.enqueue_in(
      AppConfig.enque_delay.second,
      Jobs::BookkeepingAttach, 
      user_id, 
      source_type,
      source_id)
  end

  def initialize(user)
    @user = user
  end

  def attach(source_type, source_id)
    prospective_users = ProspectiveUser.preload(:inviter).source(source_type, source_id)
    prospective_users.each do |prospective_user|
      attach_action(prospective_user)
      prospective_user.destroy
    end
  end

  private

  def attach_action(prospective_user)
    case prospective_user.inviter_type
    when 'User'
      user_attach_action(prospective_user.inviter)
    when 'Tribe'
      tribe_attach_action(prospective_user.inviter)
    end
  end 

  def user_attach_action(user)
    FriendshipService.new(user).add(@user)
  end

  def tribe_attach_action(tribe)
    TribeService.new(nil, @user).join(tribe)
  end
end
