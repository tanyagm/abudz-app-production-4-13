class Bookkeeping::BookkeepingCreateService
  def initialize(inviter)
    @inviter = inviter
  end

  def create(source_type, source_id)
    @inviter.invited_users.find_or_create(source_type, source_id)
  end
end
