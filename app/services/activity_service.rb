class ActivityService

  def initialize(user)
    @user = user
  end

  def self.add_tracker
    Comment.tracker :user, -> (comment) do
      to_user = comment.commentable.user_id
      to_user unless comment.user_id == to_user
    end

    FriendRelation.tracker :user, -> (relation) do
      relation.to_user_id
    end

    Post.tracker :user, -> (post) do
      post.user_id
    end

    User.tracker :user, -> (user) do
      user.id
    end

    Tribe.tracker :user, -> (tribe) do
      tribe.owner_id
    end

  end

  def all_activities(from, size)
    activities = friendship_activities(from, size) | comment_activities(from, size) | like_activities(from, size) | endorse_activities(from, size) | tribe_post_activities(from, size)
    activities = activities.sort_by { |activity| activity[:timestamp] }.reverse
    {
      data: activities
    }
  end

  def tribe_post_activities(from, size)
    Tribe.fetch_activities(:user, @user.id, preload_class: User, preload_attribute: :poster_id, from: from, size: size) do |item, poster|
      {
        user: JSON.parse(UserSerializer.new(poster).to_json),
        action_type: 0,
        action_id: item[:post_id].to_i,
        message: "posted on your tribe",
        timestamp: item[:timestamp].to_i
      }
    end
  end

  def friendship_activities(from, size)
    FriendRelation.fetch_activities(:user, @user.id, preload_class: User, preload_attribute: :from_user_id, from: from, size: size) do |item, user|
      if item[:to_user_id] == @user.id.to_s
        {
          user: JSON.parse(UserSerializer.new(user).to_json),
          action_type: 1,
          action_id: item[:from_user_id].to_i,
          message: "accepted your request to connect",
          timestamp: item[:timestamp].to_i
        }
      end
    end
  end

  def comment_activities(from, size)
    Comment.fetch_activities(:user, @user.id, preload_class: User, preload_attribute: :user_id, from: from, size: size) do |item, user|
      if item[:user_id] != @user.id.to_s
        {
          user: JSON.parse(UserSerializer.new(user).to_json),
          action_type: 0,
          action_id: item[:post_id].to_i,
          message: "commented on your post",
          timestamp: item[:timestamp].to_i
        }
      end
    end
  end

  def like_activities(from, size)
    Post.fetch_activities(:user, @user.id, preload_class: User, preload_attribute: :user_id, from: from, size: size) do |item, user|
      if item[:user_id] != @user.id.to_s
        {
          user: JSON.parse(UserSerializer.new(user).to_json),
          action_type: 0,
          action_id: item[:user_id].to_i,
          message: "liked your post",
          timestamp: item[:timestamp].to_i
        }
      end
    end
  end


  def endorse_activities(from, size)
    User.fetch_activities(:user, @user.id, preload_class: User, preload_attribute: :user_id, from: from, size: size) do |item, user|
      if item[:user_id] != @user.id.to_s
        {
          user: JSON.parse(UserSerializer.new(user).to_json),
          action_type: 2,
          action_id: item[:user_id].to_i,
          message: "has endorsed you",
          timestamp: item[:timestamp].to_i
        }
      end
    end
  end

  def self.subscribe_notification
    $redis.psubscribe('__keyevent*__:*' ) do |on|
      on.psubscribe do |event, total|
        puts "Subscribed to ##{event} (#{total} subscriptions)"
      end

      on.pmessage do |pattern, event, message|
        # Message processing here
        puts "Processing pattern #{pattern}, event #{event}, message #{message}"
        remove_expired_keys(:user,message)
      end

      on.punsubscribe do |event, total|
        puts "Unsubscribed for ##{event} (#{total} subscriptions)"
      end
    end
  end

  private

  def self.remove_expired_keys(key,message)
    user_ids = User.all.pluck(:id)
    user_ids.each do |id|
      $redis.zrem("act:#{key.to_s}:#{id}",message)
    end
  end
end