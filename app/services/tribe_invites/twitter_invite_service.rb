class TribeInvites::TwitterInviteService < BaseService::Invites::TwitterInviteService 
  protected
  def existing_user_action(params)
    # TODO Actually give @existing_user a choice to join the tribe
    TribeService.new(nil, @existing_user).join(params[:tribe])
  end

  def new_user_action(params)
    super(params)
    Bookkeeping::BookkeepingCreateService.new(params[:tribe]).create(ProspectiveUser::SOURCE_TWITTER, @target)
  end

  def get_message(params)
    "I just created the #{params[:tribe].name} Tribe on the Activebudz app and I want you to be a part of it! Join me!"
  end
end
