class TribeInvites::GotribalInviteService
  include BaseService::Invites::InviteService

  def send_invite(user_id, tribe)
    user = User.find(user_id)
    TribeService.new(nil, user).join(tribe)
    NotificationService.new.tribe_invitation_push_notification(@user, user, tribe)
    NotificationMailer.tribe_invitations(user.id).deliver if user.notification_setting.tribe_invitation_freq.to_sym == :immediate
    tribe.notify user
  end
end