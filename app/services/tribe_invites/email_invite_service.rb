class TribeInvites::EmailInviteService < BaseService::Invites::EmailInviteService 
  protected
  def existing_user_action(params)
    # TODO Actually give @existing_user a choice to join the tribe
    TribeService.new(nil, @existing_user).join(params[:tribe])
  end

  def new_user_action(params)
    InviteMailer.send_tribe_invite(@user.fullname, @target, params).deliver!
    Bookkeeping::BookkeepingCreateService.new(params[:tribe]).create(ProspectiveUser::SOURCE_EMAIL, @target)
  end
end
