class NotificationService

  def send_auto_badge_notification(receiver)
      alert = "You have logged in"
      send_push_notification(receiver, alert, {}, "auto")
  end

  def send_bulk_comment_notification(receiver)
    notification_count = receiver.comment_notifications_count
    if notification_count > 0
      alert = "You have #{notification_count} new comments"
      send_push_notification(receiver, alert)
    end
  end

  def send_bulk_friend_notification(receiver)
    notification_count = receiver.friend_notifications_count
    if notification_count > 0
      alert = "You have #{notification_count} pending friend request"
      send_push_notification(receiver, alert)
    end
  end

  def send_bulk_suggested_friend_notification(receiver, search_service = nil)
    search_service ||= SearchService.new(receiver)
    new_suggested_results = search_service.compare_suggested_results
    if new_suggested_results.present?
      alert = "You have #{new_suggested_results.count} new suggested Activebudz"
      send_push_notification(receiver, alert)
    end
  end

  def send_bulk_message_notification(receiver)
    notification_count = receiver.message_notifications_count
    if notification_count > 0
      alert = "You have #{notification_count} unread messages"
      send_push_notification(receiver, alert)
    end
  end

  def send_bulk_tribe_post_notification(receiver)
    notification_count = receiver.tribe_post_notifications_count
    if notification_count > 0
      alert = "#{notification_count} people posted on your Tribe"
      send_push_notification(receiver, alert)
    end
  end

  def send_bulk_tribe_invitation_notification(receiver)
    notification_count = receiver.tribe_invitation_notifications_count
    if notification_count > 0
      alert = "You have #{notification_count} Tribe invitation"
      send_push_notification(receiver, alert)
    end
  end

  def self.send_endorse_notification(sender, receiver)
    Resque.enqueue(Jobs::PushEndorseNotification, sender.id, receiver.id)
  end

  def endorse_push_notification(sender, receiver)
    alert = "#{sender.fullname} endorsed you"
    send_push_notification(receiver, alert, user_id: sender.id)
  end

  def self.send_friend_request(current_user, user, status)
    Resque.enqueue(Jobs::PushFriendRequest, current_user.id, user.id, status)
  end

  def friend_push_notification(current_user,user,status)
    return if user.notification_setting.activebudz_request_freq.to_sym != :immediate

    if status.to_sym == :sent
      add_friend_push_notification(current_user,user)
    elsif status.to_sym == :accepted
      accepted_friend_push_notification(current_user,user)
    end
  end

  def add_friend_push_notification(sender,receiver)
    alert = "#{sender.fullname} sent you a friend request"
    send_push_notification(receiver,alert,{pending_friend_id: sender.id, pending_request: receiver.received_request_users.count})
  end

  def accepted_friend_push_notification(sender,receiver)
    alert = "#{sender.fullname} accepted your friend request"
    send_push_notification(receiver,alert,{accepted_friend_id: sender.id, pending_request: receiver.received_request_users.count})
  end

  def tribe_post_push_notification(post)

    alert = "#{post.user.fullname} posted on your tribe"
    tribe = post.tribe

    return if tribe.owner.notification_setting.tribe_post_freq.to_sym != :immediate
    send_push_notification(tribe.owner, alert, tribe_id: tribe.id) rescue return
  end

  def tribe_invitation_push_notification(sender, receiver, tribe)
    return if tribe.owner.notification_setting.tribe_invitation_freq.to_sym != :immediate

    alert = "#{sender.fullname} invited you to join #{tribe.name}"

    send_push_notification(receiver, alert, tribe_id: tribe.id)
  end

  def comment_push_notification(post_owner, comment)
    return if post_owner.notification_setting.comment_freq.to_sym != :immediate

    alert = "#{comment.user.fullname} commented on your post"
    send_push_notification(post_owner, alert, post_id: comment.commentable.id) rescue return
  end

  def message_push_notification(message)
    sender   = message.sender
    receiver = message.receiver
    content  = "#{sender.fullname}: #{message.body}"

    return if receiver.notification_setting.message_freq.to_sym != :immediate
    
    mapping  = if message.root_id
      MessageMapping.find_by(message_id: message.root_id)
    else
      MessageMapping.find_by(message_id: message.id)
    end

    begin
      user_count = Pusher.get("/channels/#{MessageService.message_channel(sender.id, receiver.id)}", {  info: 'user_count' })[:user_count]
      if user_count < 2
        send_push_notification(receiver, content, conversation_id: mapping.id)
      end
    rescue
      send_push_notification(receiver, content, conversation_id: mapping.id)
    end
  end

  private

  # Send push notification to the devices
  #
  # @param user the user sends push notification
  # @param message the message to be sent through push notification
  # @param options any additional payload to be included in push notification
  # @param badgeValue the badge value that is sent to user
	def send_push_notification(user, message, options={}, badgeValue="+1")
		return if user.nil?

		ios_tokens = []
		android_tokens = []

		user.devices.each do |device|
			ios_tokens << device.token if device.device_type == Device::IOS && device.token
			android_tokens << device.token if device.device_type == Device::ANDROID && device.token
		end

    puts "Send notification"
    
		if !ios_tokens.empty?
			ios_tokens.each do |token|
				Urbanairship.register_device(token)
			end

			notification = { device_tokens: ios_tokens, aps: { alert: message, badge: badgeValue }.merge(options) }
		elsif !android_tokens.empty?
			notification = { apids: android_tokens, android: { alert: message, extra: Hash[options.map{|x,y| [x, "#{y}"]}] }}
		end
    
		if notification
			response = Urbanairship.push(notification) 
			puts "[NotificationService] Send notification #{notification.inspect} with response #{response.inspect}"
		end
	end

  def send_notification_by_date(frequency)
    current_date = Date.today.strftime("%d")

    case frequency
    when :daily
      yield
    when :weekly
      yield if current_date.to_i % 7 == 1
    when :monthly
      yield if current_date.to_i == 1
    end
  end
end