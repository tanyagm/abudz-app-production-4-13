class SessionService
 
  def initialize(user = nil)
    @user = user
  end  

  def sign_in(username, password)
    @user = AuthenticationService.new(username).authenticate(password)

    if @user
      NotificationService.new.send_auto_badge_notification(@user)
      return @user, issue_token
    else
      fail Exceptions::AuthenticationFailed
    end
  end

  def sign_out
    return unless @user

    ActiveRecord::Base.transaction do
      Device.where(user_id: @user.id).delete_all
      Doorkeeper::AccessToken.where(resource_owner_id: @user.id)
        .delete_all
    end
  end

  def issue_token
    return nil unless @user

    token = Doorkeeper::AccessToken.where(resource_owner_id: @user.id).first_or_create do |token|
      token.application_id = Doorkeeper::Application.first.id
      token.expires_in = Doorkeeper.configuration.access_token_expires_in
      token.use_refresh_token = Doorkeeper.configuration.refresh_token_enabled?
    end

    token.update(created_at: Time.now) if token.expired?

    return token
  end

end