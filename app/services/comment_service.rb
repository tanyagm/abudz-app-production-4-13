class CommentService

  def initialize(commentable, user)
    @commentable = commentable
    @user = user
    @comments = @commentable.comments
  end 

  def create(params)
    @params = params
    @comment_params = prepare_params
    comment = @comments.create!(@comment_params)
    post_owner = @commentable.user
    if @user != post_owner
      NotificationService.new.comment_push_notification(post_owner, comment)
      NotificationMailer.comments(post_owner.id).deliver if @user.notification_setting.comment_freq.to_sym == :immediate
      comment.notify post_owner
    end
    comment
  end

  def delete_comment(comment_id)
    @comment = fetch_comment_for_deletion(comment_id)
    @comment.destroy
  end

  private 

  def fetch_comment_for_deletion(comment_id)
    if @commentable.owned_by?(@user)
      fetch_any_comment(comment_id)
    else
      fetch_self_owned_comment(comment_id)
    end
  end

  def fetch_self_owned_comment(comment_id)
    @comments.owned_by(@user).find(comment_id)
  end

  def fetch_any_comment(comment_id)
    @comments.find(comment_id)
  end

  def prepare_params
    @params.merge!(user_id: @user.id)
    @params.permit(:message, :user_id)
  end
end
