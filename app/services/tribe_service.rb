class TribeService

  def initialize(params, user)
    @params = params
    @user = user
  end

  def joined
    return @user.tribes.id_order.paginate(@params)
      .preload(:owner, :users)
  end

  def owned
    return @user.owned_tribes.id_order.paginate(@params)
      .preload(:owner, :users)
  end

  def create_post(tribe)
    raise Exceptions::NotTribeMemberError unless tribe.users.include? @user
    
    @params[:tribe_id] = tribe.id
    post = @user.posts.create!(@params)
    tribe_owner = tribe.owner
    if @user != tribe_owner
      tribe.create_activity(poster_id: @user.id, post_id: post.id)
      NotificationService.new.tribe_post_push_notification(post)
      NotificationMailer.tribe_posts(tribe_owner.id).deliver if tribe_owner.notification_setting.tribe_post_freq.to_sym == :immediate
      post.notify tribe_owner
    end
    return post
  end

  def create
    @params[:owner_id] = @user.id
    tribe = @user.tribes.create!(@params)
    SearchTribeService.new(tribe).create
    return tribe
  end

  def index
    tribes = Tribe.all.id_order.paginate(@params).preload(:owner, :users)
    tribes_id = tribes.map(&:id)
    list = @user.tribes.where(id: tribes_id).pluck(:id)
    
    return tribes, list
  end

  def join(tribe)
    tribe.users << @user unless tribe.is_tribe_member?(@user)
    return tribe
  end

  def leave(tribe)
    if tribe.owner_id != @user.id
      tribe.users.delete(@user)
    else
      raise Exceptions::LeaveTribeFailed
    end
  end

  def member_list(tribe)
    return tribe.members.paginate(@params)
  end

end
