class SocialCredentialService
  def initialize(user)
    @user = user
    @social_credentials = user.social_credentials
  end

  def add(params)
    @params = prepare_params(params)
    credential = create_or_update_social_credential
    Bookkeeping::BookkeepingAttachService.enqueue_job_for_credential(@user.id, credential)
    credential
  end

  def remove(social_type)
    type = encode_social_type(social_type)
    credential = @social_credentials.where_type(type).first!
    credential.destroy
  end

  private

  def encode_social_type(social_type)
    case social_type
    when 'twitter'
      SocialCredential::TWITTER
    when 'facebook'
      SocialCredential::FACEBOOK
    end
  end

  def create_or_update_social_credential
    credential = @social_credentials.where_type(@params[:social_type]).first_or_initialize
    credential.update!(@params)
    credential
  end

  def prepare_params(params)
    params[:social_type] = encode_social_type(params[:social_type])
    params[:uid] = get_uid(params)
    params[:secret] = AppConfig.facebook.secret if params[:social_type] == SocialCredential::FACEBOOK
    params.permit(:token, :secret, :social_type, :uid)
  end

  def get_uid(params)
    case params[:social_type]
    when SocialCredential::TWITTER
      get_twitter_uid(params[:token], params[:secret])
    when SocialCredential::FACEBOOK
      FacebookService.new(params[:token]).get_facebook_uid
    else
      raise Exceptions::NotImplemented
    end
  end

  def get_twitter_uid(token, secret)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = AppConfig.twitter.consumer_key
      config.consumer_secret     = AppConfig.twitter.consumer_secret
      config.access_token        = token
      config.access_token_secret = secret
    end
    client.user.id
  end
end
