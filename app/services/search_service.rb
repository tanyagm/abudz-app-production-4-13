class SearchService

  def initialize(user)
    @user = user
    @user_detail_service = UserDetailService.new(user)
    fitness_pursuit = @user_detail_service.get('fitness_pursuit')
    fitness_pursuit = fitness_pursuit.map { |item| item = item.downcase } if fitness_pursuit.present?
    @params = {
      fitness_pursuit: fitness_pursuit,
      fitness_level: @user_detail_service.get('fitness_level'),
      weekly_activities: @user_detail_service.get('weekly_activities'),
      lifestyle: @user_detail_service.get('lifestyle'),
      age_group: @user_detail_service.get('age_group'),
      living_place: @user_detail_service.get('living_place'),
      workout_time: @user_detail_service.get('workout_time')
    }
    @params.select! {|k,v| !v.nil?}
    @excluded_params = [ :terms => { _id: user.friends.pluck(:id).push(user.id) | user.pending_request_users.pluck(:id) | user.hidden_users.pluck(:id) } ]

    @lat = @user_detail_service.get('lat')
    @lon = @user_detail_service.get('lon')
  end

  def self.create(user_id)
    Resque.enqueue(Jobs::AddUserToIndex,user_id)
  end

  def self.update(user_id)
    Resque.enqueue(Jobs::UpdateIndexedUser,user_id)
  end

  def self.init_type(version=nil)
    current_index_name = version.present? ? "#{AppConfig.elasticsearch.index}_v#{version}" : AppConfig.elasticsearch.index
    Tire.index current_index_name do
      mapping AppConfig.elasticsearch.type_user, :properties => {
            :first_name => {
              :type => 'string',
              :analyzer => 'simple'
            },
            :last_name => {
              :type => 'string',
              :analyzer => 'simple'
            },
            :email => {
              :type => 'string',
              :analyzer => 'keyword_lowercase'
            },
            :fitness_pursuit => {
              :type => 'string',
              :analyzer => 'keyword_lowercase'
            },
            :living_place => {
              :type => 'string',
              :analyzer => 'keyword_lowercase'
            },
            :location => {
              :type => 'geo_point'
            }
          }
    end
  end

  def self.init_index(version=nil)
    current_index_name = version.present? ? "#{AppConfig.elasticsearch.index}_v#{version}" : AppConfig.elasticsearch.index
    Tire.index current_index_name do
      delete
      create :settings => {
        :analysis => {
          :analyzer => {
            :keyword_lowercase => {
              :tokenizer => 'keyword',
              :filter => ['lowercase','trim'],
              :type => 'custom'
            },
            :english_stemmed => {
              :tokenizer => 'standard',
              :filter => ['standard', 'lowercase', 'english_snowball']
            }
          },
          :filter => {
            :english_snowball => {
                :type => 'snowball',
                :language => 'English'
            }
          }
        }
      }
    end
  end

  def self.set_index_alias(version=nil)
    prev_index = Tire.index(AppConfig.elasticsearch.index)
    if prev_index.exists?
      if prev_index.aliases.include? AppConfig.elasticsearch.index
        Tire.index(prev_index_name).remove_alias AppConfig.elasticsearch.index
      end
    end

    current_index_name = version.present? ? "#{AppConfig.elasticsearch.index}_v#{version}" : AppConfig.elasticsearch.index
    current_index = Tire.index(current_index_name)

    prev_index.delete if current_index_name != AppConfig.elasticsearch.index

    puts "alias #{current_index_name} to #{AppConfig.elasticsearch.index}"
    current_index.add_alias AppConfig.elasticsearch.index

  end

  def create(version=nil)
    user = {
      type: 'user',
      id: @user.id,
      first_name: @user.first_name,
      last_name: @user.last_name,
      email: @user.email,
      avatar: @user.avatar.url,
      updated_at: @user.updated_at.to_i,
      gender: @user_detail_service.get('gender'),
      gender_preference: @user_detail_service.get('gender_preference'),
      role: @user_detail_service.get('role'),
      role_preference: @user_detail_service.get('role_preference'),
      location: {
        lat: @lat,
        lon: @lon
      }
    }.merge(@params)

    index_name = version.present? ? "#{AppConfig.elasticsearch.index}_v#{version}" : AppConfig.elasticsearch.index
    response = Tire.index index_name do
      import [user]
    end

    response = response.as_json.try(:[],"response").try(:as_json)

    if response.try(:[],"code") == 200
      return user
    else
      return nil
    end
  end

  def update
    @params.merge!({
      'first_name' => @user.first_name,
      'last_name' => @user.last_name,
      'gender' =>  @user_detail_service.get('gender'),
      'gender_preference' =>  @user_detail_service.get('gender_preference'),
      'role' =>  @user_detail_service.get('role'),
      'role_preference' =>  @user_detail_service.get('role_preference')
    })
    @params['avatar'] = @user.avatar.url if @user.avatar.present?

    user = @user
    params = @params
    Tire.index AppConfig.elasticsearch.index do
      update(AppConfig.elasticsearch.type_user,user.id,{
        :doc => params,
        :doc_as_upsert => true
        })
    end
  end

  def destroy
    Tire.index(AppConfig.elasticsearch.index).remove(AppConfig.elasticsearch.type_user,params[:id])
  end

  def search_suggested(options={})
    default_ignored_users = {}
    @user.hidden_users.each do |hidden_user|
      default_ignored_users[hidden_user.id.to_s] = hidden_user.updated_at.to_i
    end
    
    options = {
      from: 0,
      size: AppConfig.search.suggestion.default_pagination_size,
      ignored_user: default_ignored_users
    }.merge(options)

    if !is_int?(options[:from]) || !is_int?(options[:size]) || !options[:ignored_user].is_a?(Hash)
      raise Exceptions::InvalidParams
    end

    @params[:gender] = @user_detail_service.get('gender_preference')
    @params[:gender] = nil if @params[:gender].to_i == 2
    @params[:gender_preference] = @user_detail_service.get('gender')
    @params[:gender_preference] = nil if @params[:gender_preference].to_i == 2
    @params[:role] = @user_detail_service.get('role_preference')
    @params[:role_preference] = @user_detail_service.get('role')
    @params[:workout_time] = nil if @params[:workout_time].to_i == 2

    if @params[:fitness_level].present?
      if @params[:role].present?
        if @params[:role][0].to_i == 2
          if @params[:fitness_level].to_i < 3
            @excluded_params << { :range => { :fitness_level => { :lte => @params[:fitness_level].to_i } } }
          else
            @excluded_params << { :range => { :fitness_level => { :lt => @params[:fitness_level].to_i } } }
          end
        else
          @excluded_params << { :range => { :fitness_level => { :gt => @params[:fitness_level].to_i + 1 } } }
          @excluded_params << { :range => { :fitness_level => { :lt => @params[:fitness_level].to_i - 1 } } }
        end
      end
    end

    must_queries = construct_must_queries(@params)

    should_queries = construct_should_queries(@params)

    must_not_queries = construct_must_not_queries(@excluded_params)

    script = custom_score_algorithm

    queries = construct_suggestion_queries(must_queries, should_queries, must_not_queries, script, options)

    Rails.logger.info queries.to_json

    s = Tire.search "#{AppConfig.elasticsearch.index}/#{AppConfig.elasticsearch.type_user}", queries

    begin
      results = s.results 
      save_suggested_results(results) unless options[:prevent_save] == true

      results
    rescue
      raise Exceptions::SearchRequestFailed
    end
  end

  def compare_suggested_results
    current_suggested_result = search_suggested(prevent_save: true).as_json.map { |field| field['id'] }
    current_suggested_result - last_suggested_results
  end

  def save_suggested_results(results)
    result_ids = results.as_json.map { |field| field['id'] }
    @user.last_suggested_users = result_ids.to_s[1..-2].gsub " ",""
    @user.save
  end

  def last_suggested_results
    @user.last_suggested_users.present? ? @user.last_suggested_users.split(",").map { |s| s.to_i } : []
  end

  private

  def construct_must_queries(params)
    boost = AppConfig.search.suggestion.boost

    must_queries = []
    if params
      params.each do |k,v|
        next if v.nil? or boost[k].to_s != 'must'
        v = [v] unless v.is_a? Array
        must_queries << {
          :terms => {
            k => v
          }
        }
      end
    end
    return must_queries
  end

  def construct_should_queries(params)
    boost = AppConfig.search.suggestion.boost

    should_queries = []
    if params
      params.each do |k,v|
        k = k.to_s
        next if v.nil? or boost[k].is_a? String
        if v.is_a? Array
          v.each do |val|
            should_queries << {
              :term => {
                k => {
                  :value => val,
                  :boost => boost[k] / v.size
                }
              }
            }
          end
        else
          should_queries << {
            :term => {
              k => {
                :value => v,
                :boost => boost[k]
              }
            }
          }
        end
      end
    end
    should_queries << {
      :match_all => {}
    }
    return should_queries
  end

  def construct_must_not_queries(params)
    must_not_queries = []
    if params
      params.each do |v|
        must_not_queries << v
      end
    end
    return must_not_queries
  end

  def construct_suggestion_queries(must_queries, should_queries, must_not_queries, script, options)
    {
      :from => options[:from],
      :size => options[:size],
      :query => {
        :custom_score => {
          :query => {
            :bool => {
              :must => must_queries,
              :should => should_queries,
              :must_not => must_not_queries
            }
          },
          :params => {
            :lat => @lat,
            :lon => @lon,
            :current_time => Time.now.to_i,
            :ignored_user => options[:ignored_user]
          },
          :script => script
        }
      },
      :script_fields => {
        :first_name => {
          :script => "_source.first_name"
        },
        :last_name => {
          :script => "_source.last_name"
        },
        :avatar_url => {
          :script => "_source.avatar"
        }
        # ,
        # :distance => {
        #   :script => "doc['location'].distance(lat,lon)",
        #   :params => {
        #     :lat => @lat,
        #     :lon => @lon
        #   }
        # }
      }
    }
  end

  def custom_score_algorithm(use_geolocation=false)

    "
    score = _score;
    string_id = (String)doc['id'].value; 
    if(ignored_user.containsKey(string_id)) { 
      score - 100000.0 / abs(current_time - ignored_user[string_id]);
    } else { 
      score;
    }
    "
  end

  def is_int?(value)
    true if Integer(value) rescue false
  end
end