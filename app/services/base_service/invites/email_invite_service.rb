class BaseService::Invites::EmailInviteService 
  include BaseService::Invites::InviteService
  
  protected
  def get_target(params)
    params[:email].to_s
  end

  def existing_user
    @existing_user ||= User.where(email: @target).first
  end
end
