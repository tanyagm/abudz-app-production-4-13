module BaseService::Invites::InviteService
  extend ActiveSupport::Concern 
  
  def initialize(user)
    @user = user
  end

  def send_invite(params)
    @params = HashWithIndifferentAccess.new(params) 
    @target = get_target(@params)
    if existing_user_present? 
      existing_user_action(@params)
    else
      new_user_action(@params)
    end
  end

  protected

  def get_target(params)
    raise Exceptions::AbstractMethodCalled
  end

  def existing_user_action(params)
    raise Exceptions::AbstractMethodCalled
  end

  def new_user_action(params)
    raise Exceptions::AbstractMethodCalled
  end

  def existing_user
    raise Exceptions::AbstractMethodCalled
  end

  private 

  def existing_user_present?
    existing_user.present?
  end

end
