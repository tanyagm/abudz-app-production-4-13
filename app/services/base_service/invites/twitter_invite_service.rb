class BaseService::Invites::TwitterInviteService 
  include BaseService::Invites::InviteService
  
  def initialize(user)
    super(user)
    @credential = user.social_credentials.twitter.first
    raise Exceptions::NoTwitterCredential unless @credential
  end
  
  protected
  def get_target(params)
    params[:twitter_id].to_i
  end

  def existing_user
    @target_credential ||= SocialCredential.where(social_type: SocialCredential::TWITTER, uid: @target).includes(:user).first
    @existing_user ||= @target_credential.try(:user)
  end

  def new_user_action(params)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = AppConfig.twitter.consumer_key
      config.consumer_secret     = AppConfig.twitter.consumer_secret
      config.access_token        = @credential.token
      config.access_token_secret = @credential.secret
    end

    message = get_message(params)
    client.create_direct_message(@target, message)    
  end

  protected 

  def get_message(params)
    raise Exceptions::AbstractMethodCalled
  end
end
