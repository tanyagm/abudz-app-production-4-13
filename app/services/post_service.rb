class PostService

  def initialize(params, user)
    @params = params
    @user = user
  end 

  def create
    return @user.posts.create!(@params)
  end

  def delete(post)
    if post.user.id == @user.id
      post.destroy
    else
      raise Exceptions::TokenNotAuthorized, "Cannot delete others user's post"
    end
  end

  def index(tribe_id=nil)
    posts_arel = Post.arel_table

    if tribe_id
      where_clause = posts_arel[:tribe_id].eq(tribe_id)
    else
      relation_arel = FriendRelation.arel_table

      c_user = posts_arel[:user_id].eq(@user.id)

      ot_where = relation_arel[:from_user_id].eq(@user.id).and(relation_arel[:status].eq(FriendRelation::APPROVED))
      ot_query = relation_arel.where(ot_where).project(:to_user_id)
      others = posts_arel[:user_id].in(ot_query)

      # Exclude posts from tribes.
      exclude_tribe_posts = posts_arel[:tribe_id].eq(nil)

      where_clause = c_user.or(others).and(exclude_tribe_posts)
    end
    
    posts = Post.id_order.paginate(@params)
        .where(where_clause)
        .preload(:user)
        
    post_arr = posts.map(&:id)
    result = Hash.new
    result[:user_arr] = @user.votes.where(votable_type: Post.name, votable_id: post_arr).pluck(:votable_id)
    result[:posts] = posts

    return result
  end

  def like(post)
    if @user.is_friend?(post.user) || (post.tribe.present? && post.tribe.is_tribe_member?(@user))
      post.liked_by @user
      post.create_activity(user_id: @user.id)
    else
      raise Exceptions::NotAllowed
    end
  end

  def unlike(post)
    if @user.is_friend?(post.user) || (post.tribe.present? && post.tribe.is_tribe_member?(@user))
      post.unliked_by @user
    else
      raise Exceptions::NotAllowed
    end
  end

end