class NotifSettingService
  
  def initialize(user)
    @notif = NotificationSetting.where(user_id: user.id).first_or_create!
  end

  def save(params)
    params = prepare_params(params)
    @notif.update!(params)
    return @notif
  end

  private 

  def prepare_params(params)
    @params = params.permit( :activebudz_request_freq, :activebudz_suggested_freq, 
      :comment_freq, :message_freq, :tribe_post_freq, :tribe_invitation_freq, 
      activebudz_request_type: [], activebudz_suggested_type: [], comment_type: [], 
      message_type: [], tribe_post_type: [], tribe_invitation_type: [] )
    
    @params.each do |param| 
      param[1] = param[1].map!(&:to_sym) if param.to_s.include?'type' 
    end
    
  end

end