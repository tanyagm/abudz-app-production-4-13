class TribalRankService
  def initialize(user)
    @user = user
  end

  def endorse(target_user)
    return target_user.tribal_rank if target_user.id == @user.id
    if @user.is_friend?(target_user)
      target_user.upvote_by(@user)
      target_user.create_activity(user_id: @user.id)
      NotificationService.send_endorse_notification(@user, target_user)
      target_user.tribal_rank
    else
      raise Exceptions::NotAllowed
    end
  end
  
  def unendorse(target_user)
    return target_user.tribal_rank if target_user.id == @user.id
    if @user.is_friend?(target_user)
      target_user.unvote_up(@user)
      target_user.tribal_rank
    else
      raise Exceptions::NotAllowed
    end
  end
end