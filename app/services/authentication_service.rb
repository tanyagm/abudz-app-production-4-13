class AuthenticationService
  def initialize(username)
    @user = User.find_by(email: username)
  end

  def authenticate(password)
    if @user && @user.authenticate_with_password(password)
      return @user
    end
  end
end
