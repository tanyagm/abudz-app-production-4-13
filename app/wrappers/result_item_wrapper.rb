# Elastic Search result wrapper
# Process result given by elasticsearch before the results shown in view

class ResultItemWrapper

  # Removes result documents whose keys start with char '_'
  # Replace all occurence of char '_' with ' ' (space) in parameters object
  def initialize(attrs={})
    @attributes = attrs.select {|k, v| v && !k.start_with?("_") && k != "sort" && k != "highlight"}
    @attributes['id'] = attrs['id'].to_i
  end

  def as_json attrs={}
    @attributes 
  end

end