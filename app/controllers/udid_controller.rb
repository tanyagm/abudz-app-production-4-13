class UdidController < ActionController::Base
  before_filter :set_cache_buster

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  def start
    redirect_to :controller => 'udid', :action => 'install', email: params[:email], fullname: params[:fullname], status: 301
  end

  def step
    @fullname = params[:fullname]
    @email = params[:email]

    redirect_to :controller => 'udid', :action => 'initialize', error: true if @fullname.blank? || @email.blank?
  end

  def enroll
    @base_url = IOSCertEnrollment.base_url
    @fullname = params[:fullname]
    @email = params[:email]

    redirect_to :controller => 'udid', :action => 'initialize', error: true if @fullname.blank? || @email.blank?
  end

  def install
    profile = IOSCertEnrollment::Profile.new
    profile.payload = service(params[:email], params[:fullname])

    signed_certificate = sign(profile.payload)
   
    filename = "#{Time.now.to_i}.mobileconfig"
    file = Tempfile.new(filename) 
    file.binmode
    file.write(signed_certificate.certificate)
    file.close

    send_file file.path, x_sendfile: true, disposition: 'inline', :filename => filename, :type => signed_certificate.mime_type
  end

  def profile
    p7sign = IOSCertEnrollment::Sign.verify_response(request.body.read)
    p7sign.data.force_encoding Encoding.default_external

    @device_attributes = IOSCertEnrollment::Device.parse(p7sign)  
    puts @device_attributes.inspect

    if IOSCertEnrollment::Sign.verify_signer(p7sign)
      profile = IOSCertEnrollment::Profile.new
      profile.payload = webclip

      encrypted_profile = profile.encrypt(p7sign.certificates)
      signed_profile = profile.configuration(encrypted_profile.certificate).sign
      signed_profile = sign(profile.configuration(encrypted_profile.certificate).payload)

    else   
      DeviceRegistration.create(fullname: params[:fullname], email: params[:email],
       udid: @device_attributes["UDID"], device_name: @device_attributes["DEVICE_NAME"])

      profile = IOSCertEnrollment::Profile.new("/udid/scep")
      signed_profile = sign(profile.encrypted_service().payload)
    end

    filename = "#{Time.now.to_i}.mobileconfig"

    file = Tempfile.new(filename) 
    file.binmode
    file.write(signed_profile.certificate)
    file.close

    send_file file.path, x_sendfile: true, disposition: 'inline', :filename => filename, :type => "application/x-apple-aspen-config"
  end

  def scep
    if params['operation'] == "PKIOperation"
      signed_pki = IOSCertEnrollment::Sign.sign_PKI(request.body.read)
      send_data signed_pki.certificate, :type => signed_pki.mime_type, :disposition => 'inline'
    else
      raise "Invalid Action"
    end
  end   

  def show_scep
    case params['operation']
    when "GetCACert"
      send_data registration_authority.certificate, :type => registration_authority.mime_type, 
        :disposition => 'inline'
    when "GetCACaps" 
      capability = IOSCertEnrollment::Sign.certificate_authority_caps
      send_data capability, :type => "text/plain", :disposition => 'inline'
    else
      raise "Invalid Action"
    end
  end

  private

  def webclip
    content_payload = general_payload
    content_payload['PayloadIdentifier'] = IOSCertEnrollment.identifier + ".webclip"
    content_payload['PayloadType'] = "com.apple.webClip.managed" # do not modify

    # strings that show up in UI, customisable
    content_payload['PayloadDisplayName'] = IOSCertEnrollment.display_name
    content_payload['PayloadDescription'] = "Install this temporary profile to allow GOTRIbal to find your device's UDID for our alpha distribution. You may remove it via 'Settings'."

    # allow user to remove webclip
    content_payload['Label'] = "GOTRIBAL"
    content_payload['URL'] = "www.gotribalnow.com"
    content_payload['IsRemovable'] = true
   
    payload = Plist::Emit.dump([content_payload])

    puts payload.inspect
    payload
  end

  def sign(payload)
    ica_cert = OpenSSL::X509::Certificate.new(File.open(AppConfig.enrollment.inter_ca).read)
    root_cert = OpenSSL::X509::Certificate.new(File.open(AppConfig.enrollment.root_ca).read)

    signed_profile = OpenSSL::PKCS7.sign(IOSCertEnrollment::SSL.certificate, IOSCertEnrollment::SSL.key, payload, [ica_cert], OpenSSL::PKCS7::BINARY)
    IOSCertEnrollment::Certificate.new(signed_profile.to_der, "application/x-apple-aspen-config")     
  end

  def general_payload
    payload = Hash.new
    payload['PayloadVersion'] = 1 # do not modify
    payload['PayloadUUID'] = UUIDTools::UUID.random_create().to_s # should be unique
    payload['PayloadOrganization'] = IOSCertEnrollment.organization
    payload
  end

  def registration_authority    
    scep_certs = OpenSSL::PKCS7.new()

    ica_cert = OpenSSL::X509::Certificate.new(File.open(AppConfig.enrollment.inter_ca).read)
    root_cert = OpenSSL::X509::Certificate.new(File.open(AppConfig.enrollment.root_ca).read)

    scep_certs.type="signed"
    scep_certs.certificates=[IOSCertEnrollment::SSL.certificate, ica_cert] 
    return IOSCertEnrollment::Certificate.new(scep_certs.to_der, "application/x-x509-ca-ra-cert")
  end

  def service(email, fullname)
    payload = general_payload
    payload['PayloadType'] = "Profile Service" # do not modify
    payload['PayloadIdentifier'] = "com.gotribal.api.mobileconfig.profile"

    # strings that show up in UI, customizable
    payload['PayloadDisplayName'] = IOSCertEnrollment.display_name
    payload['PayloadDescription'] = "Install this temporary profile to allow GOTRIbal to find your device's UDID for our alpha distribution. You may remove it via 'Settings'."

    payload_content = Hash.new
    payload_content['URL'] = URI.encode(IOSCertEnrollment.base_url + "/udid/profile?email=#{email}&fullname=#{fullname}")
    payload_content['DeviceAttributes'] = [
        "UDID", 
        "VERSION",
        "PRODUCT",              # ie. iPhone1,1 or iPod2,1
        "DEVICE_NAME",          # given device name "My iPhone"
        "MAC_ADDRESS_EN0",
        "IMEI",
        "ICCID"
        ];
    
    payload['PayloadContent'] = payload_content
    payload = Plist::Emit.dump(payload)
    payload
   end
end