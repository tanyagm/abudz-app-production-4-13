class PasswordsController < ApplicationController
  before_filter :fetch_user_by_token
  rescue_from ActiveRecord::RecordNotFound, with: :render_token_not_found

  def edit
  end

  def update
    if @user.update(password_params) 
      @token.destroy
    else
      flash.now[:alert] = @user.errors.full_messages
      render :edit
    end
  end

  private  
  def fetch_user_by_token
    @token = UserToken.where_token(params[:token]).not_expired.first!
    @user = @token.user
  end

  def password_params
    @password_params ||= params[:user].permit(:password, :password_confirmation)
  end

  def render_token_not_found
    render :token_not_found
  end
end
