class HomeController < ApplicationController
  def raise_not_found!
    render page_path('404'), status: :not_found
  end
end
