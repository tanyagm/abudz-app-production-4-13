class Api::V1::DirectMessagesController < Api::V1::BaseController
  before_filter :fetch_mapping, only: [:show, :destroy, :conversation]
  before_filter :prepare_service, except: [:show, :auth]

  def index    
    @messages = @service.list_of_conversation(pagination_params)
    meta_data = @service.init_notification.merge(
      pagination_options(@messages.last.try(:sort_order), @messages.first.try(:sort_order))
    )

    render json: @messages, 
      meta:meta_data, 
      meta_key:'meta_data'
  end

  def show    
    raise Exceptions::MessageError unless @mapping.participant_ids.include? current_user.id
    render json: @mapping
  end

  def new
    render json: @service.init_conversation(params[:receiver_id])
  end

  def create
    receiver = User.find(params[:receiver_id])
    @service.send_message(receiver, message_params[:body])
    render_status :created
  end

  def destroy
    if @service.delete_conversation(@mapping)
      @messages = @service.list_of_conversation(pagination_params)
      meta_data = @service.init_notification.merge(
        pagination_options(@messages.last.try(:sort_order), @messages.first.try(:sort_order))
      )

      render json: { meta_data: meta_data }, status: :ok
    else
      head(:internal_server_error)
    end
  end

  def conversation
    @messages = @service.conversation(@mapping, pagination_params)
    meta_data = pagination_options(@messages.first.try(:id), @messages.last.try(:id))
      .merge!(message_channel)

    render json: @messages, meta:meta_data, meta_key:'meta_data'
  end

  def auth
    response = Pusher[params[:channel_name]].authenticate(params[:socket_id], {
      user_id: current_user.id, # => required
      user_info: {
        name: current_user.fullname
      }
    })
    render json: response
  end

  private

  def message_params
    params.permit(:body)
  end

  def fetch_mapping
    @mapping = MessageMapping.find(params[:id])
  end

  def prepare_service
    @service = MessageService.new(current_user, params[:socket_id])
  end

  def message_channel
    return { 
      channel_name: MessageService.message_channel(@mapping.first_user_id, @mapping.second_user_id), 
      event_name: MessageService.message_event
    }
  end

end
