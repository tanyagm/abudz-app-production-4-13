class Api::V1::SessionController <  Api::V1::BaseController
  skip_before_filter :authenticate_user!, only: [:sign_in, :sign_in_fb]

  def sign_in
    service = SessionService.new
    user, token = service.sign_in(params[:username], params[:password])
    render_user_with_token user, token
  end

  def sign_out
    SessionService.new(current_user).sign_out
    render_status :ok
  end

  def sign_in_fb
    fb_service = FacebookService.new(params[:token])
    user, status  = fb_service.sign_in
    token = SessionService.new(user).issue_token
    render_user_with_token user, token, status
  end
end
