class Api::V1::PostsController < Api::V1::BaseController

	before_filter :fetch_post, only: [:like, :unlike, :delete]

  def create
    post = PostService.new(post_params, current_user).create
    render json: post
  end

  def delete
    PostService.new(nil, current_user).delete(@post)
    render_status :ok
  end

  def index
    result = PostService.new(pagination_params, current_user).index
    pagination_response(result[:posts], PostListSerializer, liked_ids: result[:user_arr])
  end

  def like
    PostService.new(nil, current_user).like(@post)
    render_likes_count
  end

  def unlike
    PostService.new(nil, current_user).unlike(@post)
    render_likes_count
  end

  private 

  def render_likes_count
    render json: {likes_count: @post.cached_votes_total}
  end

  def fetch_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.permit(:message, :image_data, :content_type, :filename, :location_name, :lat, :lon)
  end

end
