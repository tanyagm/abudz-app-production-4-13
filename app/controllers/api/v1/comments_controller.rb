class Api::V1::CommentsController < Api::V1::BaseController
  before_filter :fetch_commentable
  before_filter :fetch_comments, only: [:index]

  def index
    pagination_response(@comments)
  end

  def create
    @comment = service.create(params)
    # TODO SA Find smarter way to update comments_count
    render json: @comment, status: :created, root: 'comment', meta: @commentable.comments_count+1,  meta_key: 'post_comments_count'
  end

  def destroy
    service.delete_comment(params[:id].to_i)
    render_status(:ok)
  end

  private

  def fetch_comments
    @comments ||= @commentable.comments.recent.includes(:user).paginate(pagination_params)
  end

  def service
    @service ||= CommentService.new(@commentable, current_user)
  end

  def fetch_commentable
    @commentable = Post.find(params[:post_id].to_i)
  end
end
