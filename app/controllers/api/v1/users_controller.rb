class Api::V1::UsersController <  Api::V1::BaseController
  include Api::V1::UsersRenderer
  user_pagination_cursor FriendRelation.arel_table[:id]
  
  skip_before_filter :authenticate_user!, only: [:create]
  before_filter :fetch_user, only: [:endorse, :unendorse, :connection]

  # Temporary disable the cache.
  # The cache not returning JSON.
  # caches_action :show, expires_in: AppConfig.expiry_in, cache_key: proc { |c|
  #   { id: params[:id] }
  # }

  def me
    render json: current_user, serializer: UserWithPrivateDetailsSerializer
  end

  def show
    service = UserProfileService.new(current_user, User.find(params[:id]))
    render json: service.serialized_user_profile_hash
  end

  def create
    @user = UserCreationService.new(params).create
    token = SessionService.new(@user).issue_token
    render_user_with_token @user, token
  end

  def suggestion
    from = params[:from] || 0
    size = params[:size] || 10
    @results = SearchService.new(current_user)
      .search_suggested(from: from, size: size)

    render json: @results.as_json, root: false and return
  end

  def update
    @user = UserUpdateService.new(current_user).update(params)
    render json: @user, serializer: UserWithPrivateDetailsSerializer
  end

  def stats
    render json: {
      pending_request_count: current_user.pending_request_users.count,
      new_messages_count: MessageMapping.unreads_count(current_user.id)
    }
  end

  def endorse
    render_tribal_rank(tribal_rank_service.endorse(@user))
  end

  def unendorse
    render_tribal_rank(tribal_rank_service.unendorse(@user))
  end

  def connection
    friends = @user.friends.paginate(pagination_params)
    render_users(friends)
  end

  private

  def render_tribal_rank(rank)
    render json: {
      tribal_rank: rank
    }
  end

  def tribal_rank_service 
    @tribal_rank_service ||= TribalRankService.new(current_user)
  end

  def fetch_user
    @user = User.find(params[:id].to_i)
  end
end
