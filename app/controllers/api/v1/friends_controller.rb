class Api::V1::FriendsController < Api::V1::BaseController
  include Api::V1::UsersRenderer
  user_pagination_cursor FriendRelation.arel_table[:id]

  def index
    render_users(current_user.friends.preload(:fitness_pursuit_details)
      .paginate(pagination_params), UserWithFitnessPursuitSerializer)
  end

  def pending_request_users
    render_users(current_user.pending_request_users.paginate(pagination_params))
  end

  def received_request_users
    render_users(current_user.received_request_users.paginate(pagination_params))
  end

  def add
    user = User.find(params[:id].to_i)
    status = service.add(user)
    render_status(status)
  end

  def remove
    status = service.remove(User.find(params[:id].to_i))
    render_status(status)
  end

  private

  def service
    @service ||= FriendshipService.new(current_user)
  end

  def render_status(status, code = 200)
    render json: { status: status }, status: code
  end
end
