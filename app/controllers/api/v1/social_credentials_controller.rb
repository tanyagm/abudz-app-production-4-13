class Api::V1::SocialCredentialsController < Api::V1::BaseController
  def create
    credential = service.add(params)
    render json: credential
  end

  def destroy
    service.remove(params[:social_type])
    render_status(:ok)
  end

  private

  def service
    @service ||= SocialCredentialService.new(current_user)
  end
end
