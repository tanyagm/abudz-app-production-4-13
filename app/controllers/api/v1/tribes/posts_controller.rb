class Api::V1::Tribes::PostsController < Api::V1::BaseController

  before_filter :define_tribe

  def create
    tribes_post = TribeService.new(post_params, current_user).create_post(@tribe)
    
    render json: tribes_post, serializer: PostSerializer, status: :created
  end

  def index
    raise Exceptions::NotTribeMemberError unless @tribe.users.include? current_user

    result = PostService.new(pagination_params, current_user).index(@tribe.id)
    
    pagination_response(result[:posts], PostListSerializer, liked_ids: result[:user_arr])
  end

  private
  
  def define_tribe
    @tribe = Tribe.find(params[:id])
  end

  def post_params
    params.permit(:message, :image_data, :content_type, :filename, :location_name, :lat, :lon)
  end

end
