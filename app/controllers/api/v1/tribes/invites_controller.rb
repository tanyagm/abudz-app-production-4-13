class Api::V1::Tribes::InvitesController < Api::V1::BaseController
  include Api::V1::UsersRenderer
  user_pagination_cursor FriendRelation.arel_table[:id]
  
  before_filter :fetch_tribe

  def send_invites
    case params[:platform_type]
    when 'twitter'
      send_twitter_invite
    when 'email'
      send_email_invite
    when 'gotribal'
      send_gotribal_invite
    else 
      raise Exceptions::NotImplemented
    end
    render_status(:ok)
  end

  def inviteable
    if @tribe.is_tribe_member?(current_user)
      paginated_friends = current_user.friends.paginate(pagination_params)
      paginated_friends_ids = paginated_friends.pluck(:id)
      friends_tribe_members_ids = @tribe.users.where(id: paginated_friends_ids).pluck(:id)
      render_users(paginated_friends, UserWithTribeMembershipSerializer, {friends_tribe_members_ids: friends_tribe_members_ids})
    else
      raise Exceptions::NotAllowed
    end
  end

  private

  def fetch_tribe
    @tribe = Tribe.find(params[:tribe_id].to_i)
    params.merge!({tribe: @tribe})
  end

  def send_twitter_invite
    TribeInvites::TwitterInviteService.new(current_user).send_invite(params)
  end

  def send_email_invite
    TribeInvites::EmailInviteService.new(current_user).send_invite(params)
  end

  def send_gotribal_invite
    TribeInvites::GotribalInviteService.new(current_user).send_invite(params[:user_id].to_i, @tribe)
  end
end
