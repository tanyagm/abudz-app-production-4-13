class Api::V1::Tribes::MembersController < Api::V1::BaseController
  include Api::V1::UsersRenderer
  user_pagination_cursor TribesUser.arel_table[:id]

  def index
    tribe = Tribe.find(params[:id])
    users = TribeService.new(pagination_params, nil).member_list(tribe)
    render_users(users)
  end

end
