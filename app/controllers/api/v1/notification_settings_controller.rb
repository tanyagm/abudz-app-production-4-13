class Api::V1::NotificationSettingsController < Api::V1::BaseController
  
  def set_notif
    notif = NotifSettingService.new(current_user).save(params)
    render json: notif, serializer: NotifSettingsSerializer, status: :ok
  end

  def show
  	render json: current_user.notification_setting, serializer: NotifSettingsSerializer, status: :ok
  end

end
