class Api::V1::TribesController < Api::V1::BaseController
  
  before_filter :define_tribe, only: [:join, :leave]
	
  def joined
    tribes = TribeService.new(pagination_params, current_user).joined
    respond_with tribes, each_serializer: TribeDetailSerializer, 
      member_limit: (params[:member_limit] || Tribe::MEMBER_LIMIT)
	end

  def owned
    tribes = TribeService.new(pagination_params, current_user).owned
    
    respond_with tribes, each_serializer: TribeDetailSerializer, 
      member_limit: (params[:member_limit] || Tribe::MEMBER_LIMIT)
  end

  def index
    tribes, joined_list = TribeService.new(pagination_params.except(:cursor), current_user).index
    respond_with tribes, each_serializer: TribeDetailSerializer, 
      joined_ids: joined_list, member_limit: (params[:member_limit] || Tribe::MEMBER_LIMIT)
  end

	def create
    tribe = TribeService.new(tribe_params, current_user).create
    
    render json: tribe, status: :created
	end

  def join
    tribe = TribeService.new(nil, current_user).join(@tribe)
    render json: tribe, status: :ok
	end

  def leave
    TribeService.new(nil, current_user).leave(@tribe)
    render_status :ok
  end

  def search
    results = SearchTribeService.search(current_user, keyword: params[:keyword], from: params[:from], size: params[:size])
    respond_with results
  end

	private

	def tribe_params
    params.permit(:name, :description, :activity, :image_data, :content_type, :filename, :location_name, :lat, :lon)
  end

  def define_tribe
    @tribe = Tribe.find(params[:id])
  end
end
