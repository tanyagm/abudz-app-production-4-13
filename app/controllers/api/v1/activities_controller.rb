class Api::V1::ActivitiesController < Api::V1::BaseController
  before_filter :prepare_service

  def index
    activities = @service.all_activities(params[:from].to_i, params[:size].to_i)
    render json: activities, root: false
  end

  private

  def prepare_service
    @service = ActivityService.new(current_user)
  end
end