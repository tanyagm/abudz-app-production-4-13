class Api::V1::PasswordRecoveriesController < Api::V1::BaseController
  skip_before_filter :authenticate_user!
  
  def request_instruction
    @user = User.where(email: params[:email]).first!
    PasswordRecoveryService.enqueue_job(@user.id)
    render_status(:ok)
  end
end
