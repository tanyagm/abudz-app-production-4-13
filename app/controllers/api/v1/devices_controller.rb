class Api::V1::DevicesController < Api::V1::BaseController
  before_filter :fetch_devices
  before_filter :fetch_device, only: :destroy

  def index
    render json: @devices
  end

  def create
    device = @devices.where(uid: params[:uid]).first_or_initialize
    device.update!(device_params)
    render json: device
  end

  def destroy
    @device.destroy
    head :no_content
  end

  private

  def fetch_device
    @device = @devices.find(params[:id].to_i)
  end

  def fetch_devices
    @devices = current_user.devices
  end

  def device_params
    params.permit(:name, :token, :device_type, :uid)
  end
end
