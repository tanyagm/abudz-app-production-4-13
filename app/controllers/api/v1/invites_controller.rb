class Api::V1::InvitesController < Api::V1::BaseController
  def send_invites
    case params[:platform_type]
    when 'twitter'
      send_twitter_invite
    when 'email'
      send_email_invite
    else 
      raise Exceptions::NotImplemented
    end
    render_status(:ok)
  end

  private

  def send_twitter_invite
    FriendshipInvites::TwitterInviteService.new(current_user).send_invite(params)
  end

  def send_email_invite
    FriendshipInvites::EmailInviteService.new(current_user).send_invite(params)
  end
end
