class Api::ApplicationController < ActionController::Base
  include ExceptionsHandler

  doorkeeper_for :all

  before_filter :throw_json_error, except: :raise_not_found!
  before_filter :authenticate_user!

  respond_to :json

  def raise_not_found!
    fail Exceptions::RoutingError, request
  end

  def check_request_exception
    super
    rescue MultiJson::LoadError, ActionDispatch::ParamsParser::ParseError
      @json_error = true
  end

  def throw_json_error
    fail Exceptions::JsonError, 'Request body is not valid JSON' if @json_error
  end

  protected

  def current_user
    if doorkeeper_token.resource_owner_id
      @current_user ||= User.find(doorkeeper_token.resource_owner_id)
    end
  end

  def current_user_id
    doorkeeper_token.resource_owner_id
  end

  def pagination_options(max_id, min_id)
    {
      max_id: max_id,
      min_id: min_id
    }
  end

  def pagination_params
    {
      per_page: params[:per_page],
      max_id: params[:max_id],
      min_id: params[:min_id]
    }
  end

  def render_status(status)
    render json: {}, status: status
  end

  def pagination_response(paginated_object_array, serializer = nil, options = {})
    pagination_data = pagination_options(
      paginated_object_array.last.try(:id),
      paginated_object_array.first.try(:id))

    options.reverse_merge!( { meta: pagination_data, meta_key: 'pagination' })
    options[:each_serializer] = serializer if serializer

    respond_with paginated_object_array, options
  end

  def render_user_with_token(user, token, status=nil)
    render(
      json: user,
      serializer: UserWithPrivateDetailsSerializer,
      root: 'user',
      meta: token.to_hash,
      meta_key: 'access_token',
      status: status || :ok
    )
  end

  private

  def authenticate_user!
    head :not_authorized unless doorkeeper_token.resource_owner_id
  end
end
