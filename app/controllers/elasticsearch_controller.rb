class ElasticsearchController < ApplicationController
  before_filter :authenticate

  def index
    @config = AppConfig.elasticsearch.to_json(except: :simple_browser)
    @indices = EsAdmin::Admin.all_indices
    @index = params[:index]
    @type = params[:type]
    @key = params[:key]
    @keyword = params[:keyword]

    if params[:index].present?
      @types = EsAdmin::Admin.all_types(params[:index])
      if params[:type].present?
        @keys = EsAdmin::Admin.all_keys(params[:index], params[:type])
        if params[:key].present? && params[:keyword].present?
          search_results = EsAdmin::Admin.simple_search(params[:index], params[:type], params[:key], params[:keyword])
          @results_total = search_results.total
          @results = JSON.pretty_generate(search_results.as_json)
        end
      end
    end
  end

  private

  def authenticate
    authenticate_or_request_with_http_basic('Administration') do |username, password|
      username == AppConfig.elasticsearch.simple_browser.username && password == AppConfig.elasticsearch.simple_browser.password
    end
  end
end