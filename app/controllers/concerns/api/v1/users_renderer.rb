module Api::V1::UsersRenderer
  extend ActiveSupport::Concern

  included do 
    class_attribute :cursor

    def self.user_pagination_cursor(cursor)
      self.cursor = cursor
    end
  end
    
  def pagination_params
    super.merge(cursor: self.cursor)
  end

  def render_users(users, serializer = nil, options = {})
    meta_data = pagination_options(users.last.try(:cursor_id), users.first.try(:cursor_id))
    options.reverse_merge!({ each_serializer: serializer || UserSerializer,
      meta: meta_data, 
      meta_key: 'pagination'})

    respond_with users, options
  end
end