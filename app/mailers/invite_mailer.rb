class InviteMailer < ActionMailer::Base
  default from: AppConfig.emailer.from

  def send_friendship_invite(sender_name, target)
    @sender_name = sender_name
    @hostname = AppConfig.hostname
    # TODO externalize this string
    mail(to: target, subject: "You are invited to try Activebudz")
  end

  def send_tribe_invite(sender_name, target, params)
    @sender_name = sender_name
    @hostname = AppConfig.hostname
    @tribe_name = params[:tribe].name
    # TODO externalize this string
    mail(to: target, subject: "You just received an invitation to a Tribe")
  end
end
