class NotificationMailer < ActionMailer::Base
  include Resque::Mailer
  @queue = :email

  default from: AppConfig.emailer.from

  def messages(user_id)
    user = User.find(user_id)
    @messages = user.received_notifications.messages.includes(:notifiable)
    mail(to: user.email, subject: "Your Activebud just sent you a message!")
  end

  def friend_requests(user_id)
    user = User.find(user_id)
    @friend_requests = user.received_notifications.friend_requests.includes(:notifiable)
    mail(to: user.email, subject: "You have an Activebud request")
  end

  def suggested_users(user_id, suggested_user_ids)
    user = User.find(user_id)
    @users = User.find(suggested_user_ids)
    mail(to: user.email, subject: "People you may know")
  end

  def comments(user_id)
    user = User.find(user_id)
    @comments = user.received_notifications.comments.includes(:notifiable)
    mail(to: user.email, subject: "You have a new comment")
  end

  def tribe_posts(user_id)
    user = User.find(user_id)
    @tribe_posts = user.received_notifications.tribe_posts.includes(:notifiable)
    mail(to: user.email, subject: "Chatter is happening in your Tribe")
  end

  def tribe_invitations(user_id)
    user = User.find(user_id)
    @tribe_invitations = user.received_notifications.tribe_invitations.includes(:notifiable)
    mail(to: user.email, subject: "You just received an invitation to a Tribe")
  end
end
