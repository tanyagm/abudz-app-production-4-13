class UserMailer < ActionMailer::Base
  default from: AppConfig.emailer.from

  def new_user(user_id)
    @user = User.find(user_id)
    @hostname = AppConfig.hostname
    mail(to: @user.email, subject: "New User Account")
  end
  
  def reset_password_instruction(user, user_token)
    @user = user
    @password_recovery_url = "%s/password_recovery/%s" % [AppConfig.hostname, user_token.token]
    mail(to: @user.email, subject: "Activebudz Password Recovery")
  end
end
