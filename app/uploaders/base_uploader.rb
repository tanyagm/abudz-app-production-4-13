class BaseUploader < CarrierWave::Uploader::Base
  storage :file

  def filename
    if original_filename
      if model && model.read_attribute(mounted_as).present?
        model.read_attribute(mounted_as)
      else
        "#{secure_token}.#{file.extension}"
      end
    end
  end

  def store_dir
    "#{Rails.root}/public/uploads/#{model.id}"
  end

  def cache_dir
    "#{Rails.root}/tmp/uploads/cache/"
  end

  def extension_white_list
    %w(jpg jpeg png)
  end

  protected
  
  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
  end

end