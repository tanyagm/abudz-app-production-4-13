# encoding: utf-8

class CloudinaryUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  # Include the Sprockets helpers for Rails 3.1+ asset pipeline compatibility:
  # include Sprockets::Helpers::RailsHelper
  # include Sprockets::Helpers::IsolatedHelper

  # Choose what kind of storage to use for this uploader:
  # if Rails.env == "developlment"
  #   storage :file
  # end

  # def filename
  #   "#{secure_token(10)}.#{file.extension}" if original_filename.present?
  #   # if original_filename.present?
  #   #   if version_name == :retina
  #   #     "#{secure_token(10)}@2x.#{file.extension}"
  #   #   else
  #   #     "#{secure_token(10)}.#{file.extension}"
  #   #   end
  #   # end
  # end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  # def store_dir
  #   "images/#{model.class.to_s.underscore}/#{model.id}"
  # end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Generate a 90x90 face-detection based thumbnail
  # version :face do
  #   cloudinary_transformation :width => 90, :height => 90,
  #     :crop => :thumb, :gravity => :face
  # end

  # Generate a 50x50 face-detection based thumbnail
  version :thumb do
    cloudinary_transformation :width => 50, :height => 50,
      :crop => :thumb, :gravity => :face
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
   %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

  def as_json(options=nil)
    url
  end

protected

  def secure_token(length=16)
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.hex(length/2))
  end

end
