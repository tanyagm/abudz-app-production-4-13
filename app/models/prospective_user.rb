# == Schema Information
#
# Table name: prospective_users
#
#  id           :integer          not null, primary key
#  source_type  :integer          default(0), not null
#  source_id    :string(255)      not null
#  inviter_id   :integer
#  inviter_type :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

class ProspectiveUser < ActiveRecord::Base
  SOURCE_EMAIL = 0
  SOURCE_TWITTER = 1
  SOURCE_FACEBOOK = 2

  belongs_to :inviter, polymorphic: true

  validates_presence_of :source_type, :source_id, :inviter_id, :inviter_type

  scope :source, -> (source_type, source_id) { where(source_type: source_type, source_id: source_id) }

  def self.find_or_create(source_type, source_id)
    self.source(source_type, source_id).first_or_create
  end
end
