# == Schema Information
#
# Table name: user_details
#
#  id         :integer          not null, primary key
#  key        :integer
#  value      :text
#  private    :integer          default(0)
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class UserDetail < ActiveRecord::Base
  attr_accessor  :fieldname

  serialize :value

  belongs_to :user, touch: true

  validates :key, uniqueness: { scope: :user_id , message: 'duplicate user detail' }
  validates :user_id,  presence: true
    
  validate :existence_of_fieldname
  validate :validity_of_answer

  after_initialize :initialize_fieldname
  before_validation :encode_key

  scope :public_infos, -> { where(private: false) }
  scope :fitness_pursuit, -> { public_infos.where(key: QuestionSet.instance.get_key('fitness_pursuit')) }

  def question_type
    QuestionSet.instance.get_question_type self.key
   end

  def is_private?
    private != 0
  end

  private

  def initialize_fieldname
    @fieldname = QuestionSet.instance.get_fieldname(self.key)
  end

  def validity_of_answer
    case question_type
      when QuestionSet::SINGLE_CHOICE
        validate_numeric_answer(value)
      when QuestionSet::MULTIPLE_CHOICE
        validate_answer_array
      when QuestionSet::DATE
        validate_answer_date
      when QuestionSet::MULTIPLE_STRING
        validate_array
    end
  end

  def validate_array
    errors.add(:value, "#{value.to_s} it not an array") unless value.kind_of? Array
  end

  def validate_answer_array
    errors.add(:value, "#{value.to_s} it not an array") and return unless value.kind_of? Array
    value.each { |ans| validate_numeric_answer(ans) }
  end

  def validate_numeric_answer(answer) 
    answer_integer = answer.to_s.to_i
    if answer_integer < 0 || answer_integer >= QuestionSet.instance.get_possible_answer(key) 
      errors.add(:value, "answer ##{answer_integer} for fieldname #{fieldname} does not exist") 
    end
  end

  def validate_answer_date
    begin
      value.to_time
    rescue ArgumentError
      errors.add(:value, "#{value} is not a valid date")
    end
  end

  def existence_of_fieldname
    errors.add(:fieldname, "field #{@fieldname} does not exist") unless QuestionSet.instance.exist? @fieldname
  end

  def encode_key
    self.key = QuestionSet.instance.get_key @fieldname
  end

end
