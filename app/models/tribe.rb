# == Schema Information
#
# Table name: tribes
#
#  id            :integer          not null, primary key
#  name          :string(50)       not null
#  description   :string(255)
#  location_name :string(50)
#  activity      :string(50)       not null
#  lat           :float
#  lon           :float
#  image         :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  owner_id      :integer
#

class Tribe < ActiveRecord::Base
  include Base64Uploader
  include Pagination
  include Notifiable
  include Trackable

  notification_target_counter_cache Counter::TRIBE_INVITATION_NOTIFICATIONS

  MEMBER_LIMIT = 6

  validates :name, :uniqueness => true
  validates :name, presence: true, length: { maximum: 50 }
  validates :activity, presence: true, length: { maximum: 50 }
  validates :location_name, length: { maximum: 50 }
  validates :description, length: { maximum: 255 }
  
  mount_base64_uploader :image, CloudinaryUploader

  has_many :tribes_users
  has_many :users, -> { order(TribesUser.arel_table[:id].desc) }, through: :tribes_users

  has_many :members, -> { 
    order(TribesUser.arel_table[:id].desc) 
    .select([User.arel_table['*'], TribesUser.arel_table[:id].as('cursor_id')])
  }, through: :tribes_users, source: :user

  has_many :posts
  belongs_to :owner, class_name: "User", foreign_key: "owner_id"
  has_many :invited_users, class_name: "ProspectiveUser", as: :inviter, dependent: :destroy

  scope :id_order, -> { order(arel_table[:id].desc) }

  before_validation :strip_whitespace

  track time_to_live: 86400, value: :trackable_values
  
  def is_tribe_member?(user)
    self.tribes_users.pluck(:user_id).include?(user.id)
  end

  def trackable_values
    {
      type: 0,
      post_id: nil,
      poster_id: nil,
      tribe_id: id,
      timestamp: Time.now.to_i
    }
  end

  private
  def strip_whitespace
    self.name = self.name.squish if self.name
    self.activity = self.activity.squish if self.activity
    self.description = self.description.strip if self.description
  end

end
