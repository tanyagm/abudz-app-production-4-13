# User Detail Constant

class QuestionSet
  include Singleton

  DEFAULT_QUESTION_SET = Rails.root.join('config', "question_set.yml")
  QUESTION_SET = Rails.root.join('config', "question_set_#{Rails.env}.yml")

  # QUESTION_TYPE
  SINGLE_CHOICE = 1
  MULTIPLE_CHOICE = 2
  DATE = 3
  STRING = 4
  MULTIPLE_STRING = 5

  def exist?(key)
    !!get(key)
  end

  def get_possible_answer(key)
    get(key)['possible_answers'].to_i if exist?(key)    
  end

  def get_question_type(key)
    get(key)['question_type'] if exist?(key)
  end

  def get_fieldname(key)
    get(key)['fieldname'] if exist?(key)
  end  

  def get_key(fieldname)
    get(fieldname)['key'] if exist?(fieldname)
  end  

  def fieldnames
    @config.keys
  end

  private 
  
  def initialize
    read_config
    create_mapping
  end

  # Fetch user detail 
  def read_config
    @config = if File.exist?(QUESTION_SET)
      YAML.load(File.read(QUESTION_SET))
    else
      YAML.load(File.read(DEFAULT_QUESTION_SET))
    end
  end

  # Create key & field name mapping
  def create_mapping 
    @mapping = {}
    @config.each do |field_name, value|
      value["fieldname"] = field_name
      @mapping[value["key"]] = field_name
    end
  end

  # Get detail constant by fieldname or its key
  def get(key)
    if key.kind_of? Fixnum
      @config[@mapping[key]]
    else
      @config[key]
    end
  end

end
