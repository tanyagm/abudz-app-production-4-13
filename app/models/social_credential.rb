# == Schema Information
#
# Table name: social_credentials
#
#  id          :integer          not null, primary key
#  user_id     :integer          not null
#  uid         :integer          not null
#  token       :string(255)      not null
#  social_type :integer          default(0), not null
#  created_at  :datetime
#  updated_at  :datetime
#  secret      :string(255)
#

class SocialCredential < ActiveRecord::Base

  FACEBOOK = 1
  TWITTER = 2

  validates :user_id, :presence => true
  validates :uid, :presence => true
  validates :uid, uniqueness: { scope: :social_type }
  validates :token, :presence => true
  validates :social_type, :presence => true
  
  scope :twitter, -> { where(social_type: TWITTER) }
  scope :where_type, -> (type) { where(social_type: type)}

  belongs_to :user
    
end
