# == Schema Information
#
# Table name: tribes_users
#
#  id       :integer          not null, primary key
#  user_id  :integer
#  tribe_id :integer
#

class TribesUser < ActiveRecord::Base
  
  belongs_to :user
  belongs_to :tribe

end
