# == Schema Information
#
# Table name: users
#
#  id                           :integer          not null, primary key
#  email                        :string(255)
#  password_hash                :string(255)
#  password_salt                :string(255)
#  created_at                   :datetime
#  updated_at                   :datetime
#  first_name                   :string(255)
#  last_name                    :string(255)
#  avatar                       :string(255)
#  cached_votes_total           :integer          default(0)
#  password_recovery_token      :string(255)
#  password_recovery_expired_at :datetime
#

class User < ActiveRecord::Base
  include Messageable
  include Base64Uploader
  include Friendable
  include Trackable
  include NotificationTarget
  include Notifiable
  include PasswordRecoverable

  attr_accessor :password, :password_confirmation

  notification_target_counter_cache Counter::FRIEND_NOTIFICATIONS
  acts_as_voter
  acts_as_votable
  
  validates_presence_of :email, :first_name, :last_name
  validates_uniqueness_of :email
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  validates_presence_of :password, :on => :create
  validates_length_of :password, :minimum => 8, :allow_blank => true
  validates_confirmation_of :password
  validates_presence_of :password_confirmation, :if => :password_changed?

  before_save :prepare_password

  has_many :devices, dependent: :destroy
  has_many :posts, dependent: :destroy
  
  has_many :details, class_name: 'UserDetail', dependent: :destroy
  has_many :public_details, -> { merge(UserDetail.public_infos) }, class_name: 'UserDetail'
  has_many :fitness_pursuit_details, -> { merge(UserDetail.fitness_pursuit) }, class_name: 'UserDetail'

  has_many :deleted_conversations, class_name: 'DeletedMessageMapping', dependent: :destroy
  
  has_and_belongs_to_many :tribes
  has_many :owned_tribes, class_name: "Tribe", foreign_key: "owner_id"
  has_many :social_credentials, dependent: :destroy
  has_many :invited_users, class_name: "ProspectiveUser", as: :inviter, dependent: :destroy

  has_one :notification_setting, dependent: :destroy
  has_many :counters, dependent: :destroy

  # Add default accessors for image upload (:image_data, :filename, :content_type) to the class. 
  # You can overwrite the accessors by adding a 3rd accessor params as hash.
  # The 3 accessors are required to upload an image.
  mount_base64_uploader :avatar, CloudinaryUploader, 
  image_data: :avatar_content, filename: :avatar_filename

  scope :has_notifications, -> { joins(:counters).where('count > 0').includes(:notification_setting) }
  
  def comment_notifications_count
    counters.find_or_create_by(counter_type: Counter::COMMENT_NOTIFICATIONS).count
  end  

  def friend_notifications_count
    counters.find_or_create_by(counter_type: Counter::FRIEND_NOTIFICATIONS).count
  end

  def message_notifications_count
    counters.find_or_create_by(counter_type: Counter::MESSAGE_NOTIFICATIONS).count
  end

  def tribe_post_notifications_count
    counters.find_or_create_by(counter_type: Counter::TRIBE_POST_NOTIFICATIONS).count
  end

  def tribe_invitation_notifications_count
    counters.find_or_create_by(counter_type: Counter::TRIBE_INVITATION_NOTIFICATIONS).count
  end

  def authenticate_with_password(password)
    encrypt_password(password) == password_hash
  end

  def fullname
    return "#{self.first_name} #{self.last_name}".strip
  end

  def tribal_rank
    cached_votes_total<3? cached_votes_total : 3
  end

  def conversation(target_id)
    MessageMapping.find_by(first_user_id: [id, target_id].min, second_user_id: [id, target_id].max)
  end

  track time_to_live: 86400, value: :trackable_values

  def trackable_values
    {
      user_id: nil,
      endorsed_user_id: id,
      timestamp: Time.now.to_i
    }
  end

  private

  def password_changed?
    !!self.password
  end

  def encrypt_password(pass)
    BCrypt::Engine.hash_secret(pass, password_salt)
  end

  def prepare_password 
    unless password.blank?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = encrypt_password(password)
    end
  end

end
