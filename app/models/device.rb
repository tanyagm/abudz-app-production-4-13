# == Schema Information
#
# Table name: devices
#
#  id          :integer          not null, primary key
#  name        :string(50)       not null
#  token       :string(255)
#  uid         :string(255)
#  last_sync   :datetime
#  user_id     :integer
#  device_type :integer          default(0)
#

class Device < ActiveRecord::Base
  IOS = 0
  ANDROID = 1

  belongs_to :user

  validates_presence_of :name, :token, :uid, :device_type, :user_id

  before_save :update_last_sync

  private

  def update_last_sync
    self.last_sync = Time.now
  end
end
