# == Schema Information
#
# Table name: friend_relations
#
#  id           :integer          not null, primary key
#  from_user_id :integer
#  to_user_id   :integer
#  status       :integer          default(0)
#  created_at   :datetime
#  updated_at   :datetime
#

class FriendRelation < ActiveRecord::Base
  include Trackable

  PENDING = 0
  APPROVED = 1
  REJECTED = 2

  belongs_to :from_user, class_name: 'User'
  belongs_to :to_user, class_name: 'User'

  validates :from_user_id, :to_user_id, presence: true
  validates :from_user_id, uniqueness: { scope: :to_user_id  }
  validate :add_self_as_friend

  after_destroy :destroy_inverse

  scope :rejected_or_pending, -> { where('status = ? OR status = ?', PENDING, REJECTED) }
  scope :pending, -> { where(status: PENDING) }
  scope :rejected, -> { where(status: REJECTED) }
  scope :approved, -> { where(status: APPROVED) }

  track time_to_live: 86400, value: :trackable_values, filter: :trackable_filter, as: :user

  def trackable_values
    {
      from_user_id: from_user_id,
      to_user_id: to_user_id,
      timestamp: Time.now.to_i
    }
  end

  def trackable_filter
    status_changed? and !id_was
  end

  def self.from_user(user_id)
    where(from_user_id: user_id)
  end

  def self.to_user(user_id)
    where(to_user_id: user_id)
  end

  def accept
    unless is_approved? 
      create_inverse
      update_status(APPROVED)
    end
    self
  end

  def reject
    update_status(REJECTED) if is_pending?
    self
  end

  def is_pending?
    status == PENDING
  end

  def is_approved?
    status == APPROVED
  end

  def update_status(status)
    update!(status: status)
  end

  private

  def create_inverse
    relation = inverse_relation.first_or_initialize
    relation.update_status(APPROVED)
  end

  def destroy_inverse
    get_inverse.destroy if get_inverse
  end

  def get_inverse
    inverse_relation.first
  end

  def inverse_relation
    @inverse_relation ||= FriendRelation.where(
                                    from_user_id: self.to_user_id,
                                    to_user_id: self.from_user_id)
  end

  private

  def add_self_as_friend
    errors.add(:to_user_id, 'cannot add yourself as a friend') if self.from_user_id == self.to_user_id
  end

end

