# == Schema Information
#
# Table name: message_mappings
#
#  id                 :integer          not null, primary key
#  first_user_id      :integer          not null
#  second_user_id     :integer          not null
#  message_id         :integer          not null
#  last_message       :string(255)      not null
#  first_user_opened  :boolean          default(FALSE)
#  second_user_opened :boolean          default(FALSE)
#  created_at         :datetime
#  updated_at         :datetime
#  sort_order         :integer
#

class MessageMapping < ActiveRecord::Base
	include ContentSanitizer
  include Pusherable
  include Pagination

	sanitize_attributes :last_message

	belongs_to :message
  belongs_to :first_user, class_name: 'User', foreign_key: 'first_user_id'
  belongs_to :second_user, class_name: 'User', foreign_key: 'second_user_id'
  has_many :excluded_conversations, class_name: 'DeletedMessageMapping' 

	validates :first_user_id, :second_user_id, :message_id, :last_message, 
		presence: true
	validate :same_users

  before_save :set_sort_order_timestamp

  scope :list_of_conversation, ->(user_id) do
    # Setup join conditions with Arel.
    # It generates, "LEFT OUTER JOIN `deleted_message_mappings` ON `message_mappings`.`id` = `deleted_message_mappings`.`message_mapping_id` 
    # AND `deleted_message_mappings`.`user_id` = @user_id"
    on = Arel::Nodes::On.new(
      Arel::Nodes::Equality.new(MessageMapping.arel_table[:id], DeletedMessageMapping.arel_table[:message_mapping_id])
        .and(Arel::Nodes::Equality.new(DeletedMessageMapping.arel_table[:user_id], user_id))
    )
    join = Arel::Nodes::OuterJoin.new(DeletedMessageMapping.arel_table, on)

    # Construct OR conditions.
    first_user_id  = self.arel_table[:first_user_id].eq user_id
    second_user_id = self.arel_table[:second_user_id].eq user_id

    where(first_user_id.or(second_user_id)).joins(join)
      .merge(DeletedMessageMapping.where(id: nil))
      .order('sort_order DESC')
  end

  def self.unreads_count(user_id)
    count = 0
    list_of_conversation(user_id).each do |mapping|      
      if (mapping.first_user_id == user_id && mapping.first_user_opened == false) || (mapping.second_user_id == user_id && mapping.second_user_opened == false)
        count+=1      
      end
    end

    return count
  end

  def participant_ids
    return [self.first_user_id, self.second_user_id]
  end

	private

	def same_users
		errors.add(:second_user_id, "cannot mapped message to self") if self.first_user_id == self.second_user_id
	end

  def set_sort_order_timestamp
    self.sort_order = Time.now.to_i
  end

end
