# == Schema Information
#
# Table name: deleted_message_mappings
#
#  id                 :integer          not null, primary key
#  user_id            :integer          not null
#  message_mapping_id :integer          not null
#  created_at         :datetime
#

class DeletedMessageMapping < ActiveRecord::Base

  validates :user_id, presence: true
  validates :message_mapping_id,
    uniqueness: { scope: :user_id },
    presence: true

  belongs_to :user
  belongs_to :message_mapping
end
