class NotificationSetting < ActiveRecord::Base
  extend Enumerize
  
  bitmask :activebudz_request_type, as: [ :phone, :email, :none ]
  bitmask :activebudz_suggested_type, as: [ :phone, :email, :none ]
  bitmask :comment_type, as: [ :phone, :email, :none ]
  bitmask :message_type, as: [ :phone, :email, :none ]
  bitmask :tribe_post_type, as: [ :phone, :email, :none ]
  bitmask :tribe_invitation_type, as: [ :phone, :email, :none ]

  enumerize :activebudz_request_freq, in: { never:1, immediate:2, weekly:3 }
  enumerize :activebudz_suggested_freq, in: { never:1, weekly:3 }
  enumerize :comment_freq, in: { never:1, immediate:2, weekly:3 }
  enumerize :message_freq, in: { never:1, immediate:2, weekly:3 }
  enumerize :tribe_post_freq, in: { never:1, immediate:2, weekly:3 }
  enumerize :tribe_invitation_freq, in: { never:1, immediate:2, weekly:3 }

  belongs_to :user, touch: true

  validates :user_id, presence: true

  validates :user_id, uniqueness: true

end
