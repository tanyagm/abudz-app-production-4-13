# == Schema Information
#
# Table name: user_hidden_users
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  hidden_user_id :integer
#  hidden_until   :datetime
#  created_at     :datetime
#  updated_at     :datetime
#

class UserHiddenUsers < ActiveRecord::Base

  belongs_to :user
  belongs_to :hidden_user, class_name: "User"

  validates :user_id, :hidden_user_id, presence: true
  validates :user_id, uniqueness: { scope: :hidden_user_id  }

  scope :not_expired, -> { where(['hidden_until > ?', Time.now]) }

  def update_expiry(datetime = nil)
    datetime ||= default_expiry
    self.update!(hidden_until:datetime)
  end

  private

  def default_expiry
    Time.now + AppConfig.friendship.hidden_period.days
  end

end
