# == Schema Information
#
# Table name: user_tokens
#
#  id         :integer          not null, primary key
#  token      :string(255)      not null
#  expired_at :datetime
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class UserToken < ActiveRecord::Base
  validates_presence_of :token, :expired_at, :user_id
  validates_uniqueness_of :token, :user_id
  belongs_to :user

  scope :expired, -> { where("expired_at < ?", DateTime.now) }
  scope :not_expired, -> { where("expired_at > ?", DateTime.now) }
  scope :where_user, -> (user) { where(user_id: user.id) }
  scope :where_token, -> (token) { where(token: token) }

  def self.issue_token(user)
    self.where_user(user).first_or_initialize.tap do |user_token|
      while(!user_token.update(
          user_id: user.id,
          token: SecureRandom.hex(5),
          expired_at: AppConfig.password_recovery_expire_period.hours.from_now
        ))
      end
    end
  end 
end
