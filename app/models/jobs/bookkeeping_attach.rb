# Email new user
class Jobs::BookkeepingAttach
  extend Resque::Plugins::Retry
  @queue = :bookkeeping
  @retry_limit = AppConfig.retry_limit
  @retry_delay = AppConfig.retry_delay

  def self.perform(user_id, source_type, source_id)
    Bookkeeping::BookkeepingAttachService.new(User.find(user_id)).attach(source_type, source_id)
  end
end
