# Email new user
class Jobs::EmailNewUser
  extend Resque::Plugins::Retry

  @queue = :email

  @retry_limit = AppConfig.retry_limit
  @retry_delay = AppConfig.retry_delay

  def self.perform(user_id)
    UserMailer.new_user(user_id).deliver!
  end

end
