class Jobs::UpdateIndexedTribe
  extend Resque::Plugins::Retry

  @queue = :elasticsearch

  @retry_limit = AppConfig.retry_limit
  @retry_delay = AppConfig.retry_delay

  def self.perform(tribe_id)
    SearchService.new(Tribe.find(tribe_id)).update
  end
end