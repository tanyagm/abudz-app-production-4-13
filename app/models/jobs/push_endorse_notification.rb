class Jobs::PushEndorseNotification
  extend Resque::Plugins::Retry

  @queue = :push_message

  @retry_limit = AppConfig.retry_limit
  @retry_delay = AppConfig.retry_delay

  def self.perform(sender_id, receiver_id)
    sender = User.find(sender_id)
    receiver = User.find(receiver_id)
    NotificationService.new.endorse_push_notification(sender, receiver)
  end

end