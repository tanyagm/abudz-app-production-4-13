class Jobs::AddUserToIndex
  extend Resque::Plugins::Retry

  @queue = :elasticsearch

  @retry_limit = AppConfig.retry_limit
  @retry_delay = AppConfig.retry_delay

  def self.perform(user_id)
    SearchService.new(User.find(user_id)).create
  end
end