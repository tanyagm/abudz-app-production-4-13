class Jobs::AddTribeToIndex
  extend Resque::Plugins::Retry

  @queue = :elasticsearch

  @retry_limit = AppConfig.retry_limit
  @retry_delay = AppConfig.retry_delay

  def self.perform(tribe_id)
    SearchTribeService.new(Tribe.find(tribe_id)).create
  end
end