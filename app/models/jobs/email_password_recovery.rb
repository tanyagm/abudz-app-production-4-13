class Jobs::EmailPasswordRecovery
  extend Resque::Plugins::Retry

  @queue = :email

  @retry_limit = AppConfig.retry_limit
  @retry_delay = AppConfig.retry_delay

  def self.perform(user_id)
    PasswordRecoveryService.new(User.find(user_id)).send_reset_password_instruction
  end

end
