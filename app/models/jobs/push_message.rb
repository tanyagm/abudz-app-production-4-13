class Jobs::PushMessage
  extend Resque::Plugins::Retry

  @queue = :push_message

  @retry_limit = AppConfig.retry_limit
  @retry_delay = AppConfig.retry_delay

  def self.perform(message_id, socket_id)
  	message = Message.find(message_id)
    
    if message
  	 push_to_pusher(message, socket_id)
     push_to_urbanairship(message)
    end
  end

  def self.push_to_pusher(message, socket_id)
    content = JSON.parse(MessageSerializer.new(message).to_json)
    Pusher.trigger(MessageService.message_channel(message.sender_id, message.receiver_id), MessageService.message_event, content, {socket_id: socket_id.to_s})

    user_ids = [message.sender_id, message.receiver_id]
    mapping = MessageMapping.find_by(first_user_id: user_ids.min, second_user_id: user_ids.max)
    sender = User.find(message.sender_id)
    notification_content = {
      id: mapping.id,
      message: message.body,
      updated_at: message.updated_at.to_i,
      user: {
        id: sender.id,
        avatar_url: sender.avatar.url,
        first_name: sender.first_name,
        last_name: sender.last_name
      }
    }
    Pusher.trigger(MessageService.notification_channel(message.receiver_id), MessageService.notification_event, notification_content)
  end

  def self.push_to_urbanairship(message)
    service = NotificationService.new
    service.message_push_notification(message)
  end

end