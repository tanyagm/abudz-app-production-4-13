class Jobs::RedmonWorker
  @queue = :redmon

  def self.perform
    Redmon::Worker.new.record_stats
  ensure
    Resque.enqueue_in(1.minute, Jobs::RedmonWorker)
  end
end