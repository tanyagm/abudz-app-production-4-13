module Trackable
  extend ActiveSupport::Concern  

  included do
    class_attribute :tracked_ttl, :tracked_id, :tracked_value_method, :tracked_filter_method, :tracker_sets
    after_save :record

    def record(options={})
      begin
        expected_result = self.send(self.class.tracked_value_method).merge(options.except(:skip_filter) || {})
        filtered = self.class.tracked_filter_method ? self.send(self.class.tracked_filter_method) : false
        if filtered || options[:skip_filter].present?
          redis_key = self.class.redis_key
          expected_result.each do |k,v|
            $redis.hset(redis_key,k,v)
            self.class.tracker_sets.each do |tracker_name, tracker_id|
              $redis.zadd(self.class.redis_tracker_key(self,tracker_name),Time.now.to_i,redis_key) if tracker_id.present?
            end
          end
          $redis.expire(redis_key,self.class.tracked_ttl)
        end
      rescue => error
        error.backtrace
      end
    end

    def create_activity(options={})
      record(options.merge({ skip_filter: true }))
    end

    
    def self.fetch_activities(key, id, options={}, &block)
      preload_class = options[:preload_class]
      preload_attribute = options[:preload_attribute]
      pagination_offset = options[:from] || 0
      pagination_size = options[:size] || 10
      
      results = fetch_from_tracker(key, id, pagination_offset, pagination_size)
      preloaded = preload_activity_items(results, preload_class, preload_attribute, id)
      filter_activities(results, preload_attribute, preloaded, &block)
    end

    def self.track(options={})
      self.tracked_ttl = options[:time_to_live] || 1.day
      self.tracked_value_method = options[:value]
      self.tracked_filter_method = options[:filter]
    end

    def self.tracker(name, tracker_id_lambda)
      self.tracker_sets ||= {}
      self.tracker_sets[name] = tracker_id_lambda
    end

    private

    def self.redis_key
      "act:#{self.name}:#{Time.now.to_i}"
    end

    def self.redis_tracker_key(me,tracker_set_name)
      "act:#{tracker_set_name}:#{self.tracker_sets[tracker_set_name].call(me)}"
    end

    def self.fetch_from_tracker(tracker_name, tracker_id, pagination_offset, pagination_size)
      results = []
      keys = $redis.zrevrange("act:#{tracker_name.to_s}:#{tracker_id}",pagination_offset,pagination_offset + pagination_size - 1)
      keys.each do |notification_key|
        if !$redis.exists(notification_key)
          $redis.zrem("act:#{tracker_name.to_s}:#{tracker_id}", notification_key)
          next
        end
        if notification_key.include?(self.name)
          expected_result = self.new.send(self.tracked_value_method)
          result = {}
          expected_result.each do |k,v|
            value = $redis.hget(notification_key,k)
            result[k] = value if value.present?
          end

          if result.present?
            results << result
          end
        end
      end
      results
    end

    def self.preload_activity_items(tracks, preload_class, preload_attribute, id)
      if preload_attribute.present?
        preload_ids = tracks.map { |res| res[preload_attribute] }
        preload_class.where(id: preload_ids)
      elsif preload_class.present?
        preload_class.where(id: id) 
      end
    end

    def self.filter_activities(tracks, preload_attribute, preloaded_items)
      final_results = []
      tracks.each do |result|
        pre_id = result[preload_attribute] || id
        result = yield(result, preloaded_items.try(:find) { |item| item.id == pre_id.to_i }) if block_given?
        final_results << result if result.present?
      end
      final_results
    end
  end
end