module Notifiable
  extend ActiveSupport::Concern

  included do
    class_attribute :counter_cache_column
    has_many :notifications, :as => :notifiable, dependent: :destroy

    def notify(notification_target)
      notifications.create(notification_target:notification_target)
      counter = notification_target.counters.find_or_create_by(counter_type: self.class.counter_cache_column)
      Counter.update_counters counter.id, :count => 1
    end

    def self.notification_target_counter_cache(counter_cache_column)
      self.counter_cache_column = counter_cache_column
    end
  end
end
