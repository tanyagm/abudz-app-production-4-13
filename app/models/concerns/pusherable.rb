module Pusherable
	extend ActiveSupport::Concern    

  PUSHER_MESSAGE = 'received_message'

  # Not very smart, need refactor.
  def channel_name(private_prefix = true)
    if self.respond_to? :sender_id
      user_ids = [self.sender_id, self.receiver_id]         
    elsif self.respond_to? :first_user_id
      user_ids = [self.first_user_id, self.second_user_id]
    else
      return nil
    end

    prefix = private_prefix ? "private-" : ""

    return "#{prefix}#{AppConfig.env}-#{user_ids.min}-#{user_ids.max}-channel".strip
  end

  def event_name
    return PUSHER_MESSAGE
  end
		
end