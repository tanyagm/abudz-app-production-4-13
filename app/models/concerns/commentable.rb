module Commentable
  extend ActiveSupport::Concern

  included do
    has_many :comments, as: :commentable, dependent: :destroy
    
    def owned_by?(user)
      self.user_id == user.id
    end
  end
end
