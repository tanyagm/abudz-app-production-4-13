module NotificationTarget
  extend ActiveSupport::Concern

  included do
    has_many :received_notifications, :as => :notification_target, :class_name => 'Notification'
  end
end
