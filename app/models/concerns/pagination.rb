module Pagination
	extend ActiveSupport::Concern

  included do

    def self.pagination_default
      {
        max_id: nil,
        min_id: nil,
        cursor: arel_table[:id]
      }
    end

    scope :paginate, -> (pagination_params) do
      pagination_params.reverse_merge!(pagination_default)
      per_page = pagination_params[:per_page] || AppConfig.per_page
      
      scoped = if pagination_params[:max_id]
        where(pagination_params[:cursor].lt(pagination_params[:max_id]))
      elsif pagination_params[:min_id]
        where(pagination_params[:cursor].gt(pagination_params[:min_id]))
      else
        all
      end
    
      per_page = (per_page.to_i >= AppConfig.max_per_page) ? AppConfig.max_per_page : per_page
      scoped.limit(per_page)
    end
  end

end