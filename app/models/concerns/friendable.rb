module Friendable
  extend ActiveSupport::Concern

  included do
    include Pagination
    has_many :friend_relations, foreign_key: 'from_user_id', dependent: :destroy
    has_many :friend_relations_to, class_name: 'FriendRelation',
      foreign_key: 'to_user_id', dependent: :destroy

    has_many :friends, -> { merge(FriendRelation.approved).order_by_latest_relation },
      through: :friend_relations, source: :to_user
    has_many :pending_request_users, -> { merge(FriendRelation.rejected_or_pending).order_by_latest_relation },
      through: :friend_relations, source: :to_user
    has_many :received_request_users, -> { merge(FriendRelation.pending).order_by_latest_relation },
      through: :friend_relations_to, source: :from_user

    has_many :user_hidden_users, class_name: 'UserHiddenUsers', dependent: :destroy
    has_many :hidden_users, -> { merge(UserHiddenUsers.not_expired) }, through: :user_hidden_users

    scope :exclude, -> (ids) { where('to_user_id not in (?)', ids) }

    scope :order_by_latest_relation, -> do
     order(FriendRelation.arel_table[:id].desc)
      .select([User.arel_table['*'], FriendRelation.arel_table[:id].as('cursor_id')])
   end
  end

  def is_friend?(user)
    !!self.friends.exists?(user.id)
  end

  def friends_count
    self.friends.count
  end
end
