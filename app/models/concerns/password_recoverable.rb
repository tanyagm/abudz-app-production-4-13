module PasswordRecoverable
  extend ActiveSupport::Concern

  included do
    def issue_password_recovery_token
      UserToken.issue_token(self)
    end
  end
end
