# == Schema Information
#
# Table name: posts
#
#  id                 :integer          not null, primary key
#  user_id            :integer          not null
#  message            :string(255)      not null
#  image              :string(255)
#  location_name      :string(255)
#  lat                :float
#  lon                :float
#  created_at         :datetime
#  updated_at         :datetime
#  cached_votes_total :integer          default(0)
#  cached_votes_up    :integer          default(0)
#  tribe_id           :integer
#  comments_count     :integer          default(0)
#

class Post < ActiveRecord::Base
  include Base64Uploader
  include Pagination
  include Commentable
  include Trackable
  include Notifiable

  notification_target_counter_cache Counter::TRIBE_POST_NOTIFICATIONS
  
  acts_as_votable

  scope :id_order, -> { order(arel_table[:id].desc) }

  validates :message, :presence => true

  belongs_to :user
  belongs_to :tribe


  mount_base64_uploader :image, CloudinaryUploader

  track time_to_live: 86400, value: :trackable_values

  def trackable_values
    {
      user_id: nil,
      post_id: id,
      timestamp: Time.now.to_i
    }
  end

end
