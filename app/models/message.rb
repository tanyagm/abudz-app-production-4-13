# == Schema Information
#
# Table name: messages
#
#  id              :integer          not null, primary key
#  sender_id       :integer          not null
#  receiver_id     :integer          not null
#  body            :string(255)      not null
#  sender_delete   :boolean          default(FALSE)
#  receiver_delete :boolean          default(FALSE)
#  root_id         :integer
#  opened          :boolean          default(FALSE)
#  created_at      :datetime
#  updated_at      :datetime
#

class Message < ActiveRecord::Base
	include ContentSanitizer
  include Pagination
  include Notifiable

  notification_target_counter_cache Counter::MESSAGE_NOTIFICATIONS

	sanitize_attributes :body

	belongs_to :sender, class_name: "User", foreign_key: "sender_id"
	belongs_to :receiver, class_name: "User", foreign_key: "receiver_id"
	
	validates :sender_id, :receiver_id, :body, presence: true
	validate  :sent_to_self

  scope :conversations, -> (root_id) do
    where(["id = ? OR root_id = ?", root_id, root_id])
      .order("id DESC")
  end

	def participant_ids
    return [self.sender_id, self.receiver_id]
	end

	private

	def sent_to_self
		errors.add(:receiver_id, "cannot sent message to self") if self.sender_id == self.receiver_id
	end

end
