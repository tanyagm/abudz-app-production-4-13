# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  message          :text
#  commentable_id   :integer
#  commentable_type :string(255)
#  user_id          :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class Comment < ActiveRecord::Base
  include Pagination
  include Trackable
  include Notifiable

  notification_target_counter_cache Counter::COMMENT_NOTIFICATIONS

  validates :user_id,  presence: true
  validates :message,  presence: true
  validates :commentable_id,  presence: true
  validates :commentable_type,  presence: true

  scope :recent, -> { order(arel_table[:id].desc) }
  scope :owned_by, -> (user) do
    where(user_id: user.id)
  end

  belongs_to :commentable, polymorphic:true, counter_cache:true
  belongs_to :user

  track time_to_live: 86400, value: :trackable_values, filter: :trackable_filter

  def trackable_values
    {
      user_id: user_id,
      post_id: commentable_id,
      timestamp: Time.now.to_i
    }
  end

  def trackable_filter
    !id_was
  end

end
