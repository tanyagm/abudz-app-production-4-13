class Notification < ActiveRecord::Base
  belongs_to :notifiable, :polymorphic => true
  belongs_to :notification_target, :polymorphic => true

  scope :messages, -> { where(notifiable_type: 'Message') }
  scope :friend_requests, -> { where(notifiable_type: 'User') }
  scope :comments, -> { where(notifiable_type: 'Comment') }
  scope :tribe_posts, -> { where(notifiable_type: 'Post') }
  scope :tribe_invitations, -> { where(notifiable_type: 'Tribe') }
end