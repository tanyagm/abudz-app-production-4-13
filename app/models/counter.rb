class Counter < ActiveRecord::Base
  COMMENT_NOTIFICATIONS = 0
  FRIEND_NOTIFICATIONS = 1
  MESSAGE_NOTIFICATIONS = 2
  TRIBE_POST_NOTIFICATIONS = 3
  TRIBE_INVITATION_NOTIFICATIONS = 4

  belongs_to :user

  validates :counter_type, presence: true
  validates :user_id, presence: true
end