# == Schema Information
#
# Table name: device_registrations
#
#  id          :integer          not null, primary key
#  fullname    :string(50)
#  email       :string(255)      not null
#  udid        :string(255)      not null
#  device_name :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class DeviceRegistration < ActiveRecord::Base
end
