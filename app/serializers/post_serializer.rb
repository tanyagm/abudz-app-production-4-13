class PostSerializer < BaseSerializer
  attributes :id, :message, :image, :location_name, :lat, :lon
  has_one :user

  def image
    pict = object.image
    pict.url if pict.present?
  end

end
