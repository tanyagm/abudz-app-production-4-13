class CommentSerializer < BaseSerializer
  attributes :id, :message, :created_at
  has_one :user

  def created_at
    return object.created_at.to_i
  end
end
