class TribeDetailSerializer < TribeSerializer
  has_many :users
  
  def users
    limit = @options[:member_limit].to_i
    if limit
      object.users[0..limit-1]
    else
      object.users
    end
  end

end