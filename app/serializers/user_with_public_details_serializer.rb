class UserWithPublicDetailsSerializer < UserSerializer
  attributes :conversation_id, :endorsed, :is_friend, :friends_count
  has_many :details

  def is_friend
    scope.is_friend?(object)
  end
  
  def conversation_id
    object.conversation(scope.id).try(:id)
  end

  def details
    object.public_details
  end

  def endorsed
    scope.voted_for?(object)
  end
end