class SocialCredentialSerializer < BaseSerializer
  attributes :uid, :social_type

  def social_type
    case object.social_type
    when SocialCredential::FACEBOOK
      'facebook'
    when SocialCredential::TWITTER
      'twitter'
    end
  end
end
