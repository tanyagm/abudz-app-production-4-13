class NotifSettingsSerializer < BaseSerializer
  attributes :activebudz_request_type, :activebudz_request_freq, :activebudz_suggested_type, 
    :activebudz_suggested_freq, :comment_type, :comment_freq, :message_type, :message_freq, 
    :tribe_post_type, :tribe_post_freq, :tribe_invitation_type, :tribe_invitation_freq

end