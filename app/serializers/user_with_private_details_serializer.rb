class UserWithPrivateDetailsSerializer < UserSerializer
  attributes :email, :friends_count
  has_many :details
  has_many :social_credentials
end
