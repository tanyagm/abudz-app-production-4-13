class UserWithTribeMembershipSerializer < UserSerializer
  def attributes
    hash = super
    hash[:tribe_member] = tribe_member
    hash
  end

  def tribe_member
    @options[:friends_tribe_members_ids].include?(object.id)
  end
end
