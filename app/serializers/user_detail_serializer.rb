class UserDetailSerializer < BaseSerializer
  attributes :key, :value, :private

  def key
    object.fieldname
  end
end
