class UserSerializer < BaseSerializer
  attributes :id, :first_name, :last_name, :avatar_url, :tribal_rank

  def avatar_url
    avatar = object.avatar
    avatar.url if avatar.present?
  end
end
