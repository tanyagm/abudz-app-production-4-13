class TribeSerializer < BaseSerializer
  attributes :id, :name, :description, :activity, :image, :location_name, :lat, :lon, :joined
  has_one :owner

  def joined
    joined_tribes = @options[:joined_ids]
    return true unless joined_tribes
    return joined_tribes.include? object.id
  end
end
