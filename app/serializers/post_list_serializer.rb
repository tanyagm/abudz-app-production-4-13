class PostListSerializer < PostSerializer
  attributes :liked, :likes_count, :comments_count, :created_at
  
  def liked
    return @options[:liked_ids].include? object.id
  end

  def likes_count
    return object.cached_votes_up
  end

  def created_at
  	return object.created_at.to_i
  end
end