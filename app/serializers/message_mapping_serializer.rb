class MessageMappingSerializer < BaseSerializer
  attributes :id, :message, :user, :status, :updated_at

  private

  def message
  	object.last_message
  end

  def user
    if user_context.id == object.first_user_id
      JSON.parse(UserSerializer.new(object.second_user).to_json)
    else
      JSON.parse(UserSerializer.new(object.first_user).to_json)
    end
  end

  def status
    if user_context.id == object.first_user_id
      object.first_user_opened
    else
      object.second_user_opened
    end
  end

  def updated_at
    object.sort_order
  end

  private

  def user_context
    if defined? current_user
      current_user
    else
      scope
    end    
  end

end
