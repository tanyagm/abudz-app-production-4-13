class MessageSerializer < BaseSerializer
  attributes :id, :body, :sender_id, :receiver_id, :created_at
  has_one :sender

  private

  def created_at
    object.created_at.to_i
  end
end
