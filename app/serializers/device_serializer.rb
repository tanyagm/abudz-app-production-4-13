class DeviceSerializer < BaseSerializer
  attributes :id, :name, :token, :uid, :last_sync, :device_type
end
