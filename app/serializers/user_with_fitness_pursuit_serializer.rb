class UserWithFitnessPursuitSerializer < UserSerializer
  has_many :details

  def details
    object.fitness_pursuit_details
  end
end