source 'http://rubygems.org'

ruby '2.0.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.0'

# Use MySql as the database for Active Record
gem 'mysql2', '~> 0.3.14'

# Use SCSS for stylesheets
gem 'sass-rails', '>= 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '>= 4.0.0'

gem 'jquery-rails', '3.0.4'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Delayed jobs
gem 'resque', '~> 1.25.1', require: 'resque/server'
gem 'resque-web', '~> 0.0.4', require: 'resque_web'
gem 'resque-retry', '~> 1.0.0'
gem 'resque-scheduler', '~> 2.3.0', require: 'resque_scheduler'
gem 'resque_mailer', '~> 2.2.6'
gem 'resque-pool', '~> 0.3.0'

# Attachment
gem 'carrierwave', '0.8.0'
gem 'cloudinary', '1.0.66'

# Application Config
gem 'rails_config', '~> 0.3.3'

# Serializer
gem 'active_model_serializers', '~> 0.8.1'

# Access control
gem 'cancan', '>= 1.6.10'

gem "twitter", "~> 5.3.0"

# Pagination
gem 'kaminari', '~> 0.14.1'

# Oauth provider
gem 'doorkeeper', '~> 0.7.0'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

gem 'tire', '~> 0.6.1'
gem 'faker', '~> 1.2.0'

# Use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.0.0', :require => 'bcrypt'

# Ruby library for interacting with the Urban Airship API
gem 'urbanairship', '~> 2.3.3'

# to make model likeable
gem 'acts_as_votable', github: 'sbycrosz/acts_as_votable'

# Handle request exception
gem 'request_exception_handler', '~> 0.4'

# Library for interacting with Pusher service
gem 'pusher', '~> 0.12.0'

# Whitelist-based HTML sanitizer
gem 'sanitize', '~> 2.0.6'

# Using redis as Rails cache store and session store.
gem 'redis-rails', '~> 4.0.0'

# Rails engine for static pages.
gem 'high_voltage', '~> 2.0.0'

gem 'actionpack-action_caching', github: 'rails/actionpack-action_caching'

# Use unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
gem 'capistrano', '~> 2.13.4'
gem 'rvm-capistrano'
gem 'capistrano-ext'
gem 'sshkit'

# Koala is a Facebook library for Ruby
gem "koala", "~> 1.8.0rc1"

# Redis web interface
gem 'redmon', '~> 0.0.8'

# Over the air enrollment
gem 'ios-cert-enrollment'

# Bootstrap
gem "twitter-bootstrap-rails", "~> 2.2.8"

# Simple bitmask attribute support for ActiveRecord
gem "bitmask_attributes", "~> 1.0.0"

# Enumerated attributes with I18n and ActiveRecord/Mongoid/MongoMapper support
gem "enumerize", "~> 0.7.0"

# Use debugger
# gem 'debugger', group: [:development, :test]

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', '~> 0.3.20', require: false
end

group :development, :test, :sandbox do
  gem 'jazz_hands', '~> 0.5.2'
  gem 'better_errors', '~> 1.0.1'
end

group :development do
  gem 'annotate', '>= 2.5.0'
  gem 'foreman', '~> 0.63.0'
  gem 'rubocop'
end

group :test do
  gem "simplecov", "~> 0.8.2", :require => false
  gem 'capybara-webkit', '~> 1.0.0'
  gem 'machinist', '>= 2.0.0.beta2'
  gem 'database_cleaner', '>= 1.2.0'
  gem 'email_spec', '~> 1.5.0'
  gem 'resque_spec', '~> 0.14.2'
  gem 'shoulda-matchers', '~> 2.4.0'
  gem 'rspec_candy', '~> 0.3.0'
  gem "rspec-rails", ">= 2.0.1"
  gem "cucumber-rails", '~> 1.4.0', require: false
  gem 'spork', '~> 1.0rc'
  gem 'fakeweb'
  gem 'fakeredis'
end
