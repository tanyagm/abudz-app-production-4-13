require 'spec_helper'

describe PasswordsController do
  let(:token){"muchtoken"}
  let(:params){{token: token}}
  let(:user_token){ stub_model UserToken, id: 302, user: user}
  let(:user) { stub_model User, id: 106}

  describe 'GET #edit' do
    context 'token exist' do
      before do
        UserToken.stub_chain(:where_token,:not_expired, :first!).and_return(user_token)
      end

      it 'render :edit' do
        get :edit,params
        response.should render_template(:edit)
      end
    end

    context 'token do not exist / has expired' do
      it 'render :token_not_found' do
        get :edit, params
        response.should render_template(:token_not_found)
      end
    end
  end

  describe 'PUT #update' do
    let(:update_params) { params.merge({user: {password: 123, password_confirmation: 132}})}
    before do
      UserToken.stub_chain(:where_token,:not_expired, :first!).and_return(user_token)
    end

    context 'update succeed' do
      before do
        user.stub(:update).and_return(true) 
      end

      it 'render :update' do
        put :update, update_params
        response.should render_template(:update)
      end

      it 'destroy user_token' do
        user_token.should_receive(:destroy)
        put :update, update_params
      end
    end

    context 'update failed' do
      before do
        user.stub(:update).and_return(false) 
      end

      it 'render :edit' do
        put :update, update_params
        response.should render_template(:edit)
      end
    end
  end
end
