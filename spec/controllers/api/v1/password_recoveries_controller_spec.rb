require 'spec_helper'

describe Api::V1::PasswordRecoveriesController do
  include_context 'api_v1_controller_context'

  describe 'request_instruction' do 
    let(:user) {stub_model User, email: 'sam@mail.com'}
    let(:params) {{email: user.email}}
    before do 
      User.stub_chain(:where, :first!).and_return(user)
      PasswordRecoveryService.stub(:enqueue_job)
    end

    it 'enqueue Password Recovery job' do
      PasswordRecoveryService.should_receive(:enqueue_job).with(user.id)
      post 'request_instruction', params
    end

    it 'render ok' do
      post 'request_instruction', params
      expect(response.status).to eql(200)      
    end
  end
end
