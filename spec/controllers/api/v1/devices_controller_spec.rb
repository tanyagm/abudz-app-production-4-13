require 'spec_helper'

describe Api::V1::DevicesController do
  include_context 'api_v1_controller_context'

  let(:devices) { [device1, device2] }
  let(:device) { device1 }
  let(:device1) { stub_model Device, id: 101, name: "device#1"}
  let(:device2) { stub_model Device, id: 102, name: "device#2"}
  

  describe 'GET #index' do
    before do
      current_user.stub(:devices).and_return(devices)
    end
  
    it 'respond_with 200' do
      get :index
      expect(response.status).to eql(200)
    end

    it 'render all of user devices' do
      get :index
      expect(response.body).to include(DeviceSerializer.new(device1).to_json)
      expect(response.body).to include(DeviceSerializer.new(device2).to_json)
    end
  end  

  describe 'POST #create' do
    let(:params) { stub_model Hash }
    context 'when succeed' do
      before do
        current_user.stub(:devices).and_return(devices)
        devices.stub(:where).and_return(devices)
        devices.stub(:first_or_initialize).and_return(device)
        device.stub(:update!).and_return(device)
      end

      it 'respond_with 200' do
        post :create, params
        expect(response.status).to eql(200)
      end

      it 'render newly created user devices' do
        post :create, params
        expect(response.body).to include(DeviceSerializer.new(device).to_json)
      end
    end

    context 'when failed' do
       it 'respond_with 422' do
        post :create, params
        expect(response.status).to eql(422)
      end
    end
  end 


  describe 'DELETE #destroy' do
    context 'when succeed' do
      before do
        current_user.stub(:devices).and_return(devices)
        devices.stub(:find).with(101).and_return(device)
        device.stub(:destroy).and_return(true)
      end

      it 'destroy specified device' do
        device.should_receive(:destroy)
        delete :destroy, id: 101
      end

      it 'respond_with 204' do
        delete :destroy, id: 101
        expect(response.status).to eql(204)
      end
    end
    context 'when failed' do
      it 'respond_with 404' do
        delete :destroy, id: 100003
        expect(response.status).to eql(404)
      end
    end
  end 
end
