require 'spec_helper'

describe Api::V1::PostsController do
  include_context 'api_v1_controller_context'

  before do
    User.any_instance.stub(:is_friend?).and_return(true)
  end

  describe '.create' do
    let(:params){{
      message: 'A new test posting',
      lat: 109.9876,
      lon: 12.6543,
      location_name: 'Manhattan'
    }}

    context 'when succeed' do
      it 'respond_with 200' do
        post :create, params
        expect(response.status).to eql(200)
      end
    end

    context 'when failed' do
      it 'respond_with 422' do
        post :create, params.except(:message)
        expect(response.status).to eql(422)
      end
    end
  end

  describe '.delete' do
    let(:post1) { stub_model Post, id: 1, message: "my first message", user: current_user }
    
    let(:user2) { stub_model User, id: 2, first_name: "Acong", last_name: "Ganteng" }
    let(:post2) { stub_model Post, id: 1, name: "my second message", user: user2 }

    context 'when succeed' do
      it 'respond_with 200' do
        Post.stub(:find).with('1').and_return(post1)
        delete :delete, id:1
        expect(response.status).to eql(200)
      end
    end

    context 'when failed' do
      it 'respond_with 401' do
        Post.stub(:find).with('2').and_return(post2)
        delete :delete, id:2
        expect(response.status).to eql(401)
      end
    end
  end

  describe '.index' do
    let(:p){{
      per_page: 2,
      max_id: 3
    }}
    
    let(:user1) { stub_model User, id: 1, first_name: "Abdul Haris", last_name: "Ilmawan" }
    let(:user2) { stub_model User, id: 2, first_name: "Acong", last_name: "Ganteng" }
    let(:liked) { [] }
    let(:post1) { stub_model Post, id: 1, message: "my first message", user: user1, created_at: 2.hour.ago }
    let(:post2) { stub_model Post, id: 2, message: "my second message", user: user2, created_at: 1.hour.ago }
    let(:posts_arr) { [post1, post2] }

    let(:result) { {:posts => posts_arr, :user_arr => liked} }
    before do
      PostService.stub_chain(:new, :index).and_return(result)
    end
    
    it 'respond_with 200' do
      get :index, p, format: :json 
      expect(response.status).to eql(200)
    end

    it 'render latest post stream as requested params' do
      get :index, p, format: :json
      expected_response =
        {
          data: [
              {
                id: 1,
                message: "my first message",
                image: nil,
                location_name: nil,
                lat: nil,
                lon: nil,
                liked: false,
                likes_count: 0,
                comments_count: 0,
                created_at: 2.hour.ago.to_i,
                user: {
                  id:1,
                  first_name: "Abdul Haris",
                  last_name: "Ilmawan",
                  avatar_url: nil,
                  tribal_rank: 0
                }
              },
                          
              {
                id: 2,
                message: "my second message",
                image: nil,
                location_name: nil,
                lat: nil,
                lon: nil,
                liked: false,
                likes_count: 0,
                comments_count: 0,
                created_at: 1.hour.ago.to_i,
                user: {
                  id:2,
                  first_name: "Acong",
                  last_name: "Ganteng",
                  avatar_url: nil,
                  tribal_rank: 0
                }
              },
          ],
          pagination: {
            max_id: 2,
            min_id: 1
          }
        }

      expect(response.body).to eql(expected_response.to_json)
    end
    
  end

  describe 'Post vote' do
    let(:ar_post){ stub_model Post, id:1 }
    
    describe '.like' do
      context 'when succeed' do
        before do
          Post.stub(:find).with('1').and_return(ar_post)
        end

        it 'respond_with 200' do
          post :like, id:1
          expect(response.status).to eql(200)
        end

        it 'respond_with likes count' do
          post :like, id:1
          expect(response.body).to include('likes_count')
        end
      end

      context 'when fail' do
        it 'respond_with 404' do
          expect{Post.find}.to raise_error(ActiveRecord::RecordNotFound)
          post :like, id:2
          expect(response.status).to eql(404)
        end
      end

    end

    describe '.unlike' do
      context 'when succeed' do
        before do
          Post.stub(:find).with('1').and_return(ar_post)
        end

        it 'respond_with 200' do
          delete :unlike, id:1
          expect(response.status).to eql(200)
        end

        it 'respond_with likes count' do
          delete :unlike, id:1
          expect(response.body).to include('likes_count')
        end
      end

      context 'when fail' do
        it 'respond_with 404' do
          expect{Post.find}.to raise_error(ActiveRecord::RecordNotFound)
          delete :unlike, id:2
          expect(response.status).to eql(404)
        end
      end
    end
  end

end