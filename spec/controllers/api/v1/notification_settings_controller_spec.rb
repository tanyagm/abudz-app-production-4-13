require 'spec_helper'

describe Api::V1::NotificationSettingsController do
  include_context 'api_v1_controller_context'

  describe '.set_notif' do
    let(:params){{
      tribe_post_type: ['phone'], 
      tribe_post_freq: 'immediate', 
      comment_type: ['phone'], 
      comment_freq: 'daily'
    }}

    let(:notif){ stub_model NotificationSetting, id: 1, tribe_post_type: ['phone'], tribe_post_freq: 'immediate', comment_type: ['phone'], comment_freq: 'daily', user_id: 1 }

    context 'when succeed' do
      before do
        NotifSettingService.any_instance.stub(:save).and_return(notif)
      end

      it 'respond_with 200' do
        post :set_notif, params
        expect(response.status).to eql(200)
      end

      it 'render user notifications setting' do
        post :set_notif, params
        expect(response.body).to eql(NotifSettingsSerializer.new(notif).to_json)
  		end
    end
  
  end

end
