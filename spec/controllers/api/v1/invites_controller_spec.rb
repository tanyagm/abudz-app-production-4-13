require 'spec_helper'

describe Api::V1::InvitesController do
  include_context 'api_v1_controller_context'

  describe 'POST #send_invite' do
    context 'twitter' do
      let(:params){{platform_type: 'twitter'}}
      let(:credential){ stub_model SocialCredential, token: 'sotoken', secret: 'sosecret' }
      
      before do
        FriendshipInvites::TwitterInviteService.any_instance.stub(:send_invite)
        current_user.stub_chain(:social_credentials, :twitter, :first).and_return(credential)
      end

      it 'call send_invite on TwitterInviteService' do
        FriendshipInvites::TwitterInviteService.any_instance.should_receive(:send_invite)
        post :send_invites, params 
      end

      it 'respond_with 200' do
        post :send_invites, params 
        expect(response.status).to eql(200)
      end
    end

    context 'email' do
      let(:params){{platform_type: 'email'}}

      before do
        FriendshipInvites::EmailInviteService.any_instance.stub(:send_invite)
      end

      it 'call send_invite on EmailInviteService' do
        FriendshipInvites::EmailInviteService.any_instance.should_receive(:send_invite)
        post :send_invites, params 
      end

      it 'respond_with 200' do
        post :send_invites, params 
        expect(response.status).to eql(200)
      end
    end
  end

end
