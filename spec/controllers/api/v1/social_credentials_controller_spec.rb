require 'spec_helper'

describe Api::V1::SocialCredentialsController do
  include_context 'api_v1_controller_context'

  let(:service) {SocialCredentialService.new(current_user)}

  before do
    controller.stub(:service).and_return(service)
  end

  describe 'POST #create' do
    let (:params) { {social_type: 'twitter', token: 'muchtoken'} }
    let (:credential) { stub_model SocialCredential, uid: 1337, social_type: 2 }
    before do
      service.stub(:add).and_return(credential)
    end

    it 'respond_with 200' do
      post :create, params
      expect(response.status).to eql(200)
    end

    it 'send :add on service' do
      service.should_receive(:add)
      post :create, params
    end

    it 'render newly created credential' do
      post :create, params
      expect(response.body).to include(SocialCredentialSerializer.new(credential).to_json)
    end
  end

  describe 'DELETE #destroy' do
    let (:params) { {social_type: 'twitter'} }
    before do
      service.stub(:remove).and_return(true)
    end

    it 'respond_with 200' do
      delete :destroy, params
      expect(response.status).to eql(200)
    end

    it 'send :add on service' do
      service.should_receive(:remove).with(params[:social_type])
      delete :destroy, params
    end
  end
end
