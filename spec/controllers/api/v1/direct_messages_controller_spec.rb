require 'spec_helper'

describe Api::V1::DirectMessagesController do
  include_context 'api_v1_controller_context'

  let(:message_mapping) do
    stub_model MessageMapping, id: 1, 
      last_message: 'message', first_user_id: 1, second_user_id: 2,
      first_user: current_user, second_user: current_user
  end
  let(:mappings) { [message_mapping] }
  let(:current_user) { stub_model User, id: 1 }
  let(:message_service) { MessageService.new current_user }  

  before do
    allow_message_expectations_on_nil
  end

  describe 'GET #index' do
    before do
      MessageService.stub(:new).and_return(message_service)
      message_service.should_receive(:list_of_conversation).and_return(mappings)
      message_service.should_receive(:init_notification).and_return({})
    end

    it 'respond_with 200' do
      get :index
      expect(response.status).to eql(200)
    end

    it 'render the correct response' do      
      get :index
      expect(response.body).to eql(ActiveModel::ArraySerializer.new(mappings, each_serializer: MessageMappingSerializer, 
          scope: current_user, 
          root: 'data', 
          meta: { max_id: nil, min_id: nil }, 
          meta_key: 'meta_data').to_json)
    end
  end

  describe 'GET #show' do
    before do
      MessageService.stub(:new).and_return(message_service)
      MessageMapping.should_receive(:find).and_return(message_mapping)
    end

    context 'when viewing owned direct message' do
      it 'respond_with 200' do      
        get :show, id: 1
        expect(response.status).to eql(200)
      end

      it 'render the correct response' do
        get :show, id: 1
        expect(response.body).to eql(MessageMappingSerializer.new(message_mapping, scope: current_user).to_json)
      end
    end

    context 'when viewing another user direct message' do
      before do
        message_mapping.first_user_id = 10
        message_mapping.second_user   = nil
      end

      it 'respond_with 422' do
        get :show, id: 1
        expect(response.status).to eql(422)
      end
    end
  end

  describe 'POST #new' do
    let(:params) { { receiver_id: 2 } }

    before do
      MessageService.stub(:new).and_return(message_service)
    end

    it 'respond_with 200' do
      message_service.should_receive(:init_conversation)

      post :new, params
      expect(response.status).to eql(200)
    end
  end

  describe 'POST #create' do
    let(:params) { { receiver_id: 2, body: 'message', socket_id: '123' } }

    context 'when receiver is not found' do
      it 'respond_with 404' do
        User.should_receive(:find).and_raise ActiveRecord::RecordNotFound
        post :create, params
        expect(response.status).to eql(404)
      end
    end

    context 'when receiver is found' do
      let(:receiver) { stub_model User, id: 2 }

      before do
        User.should_receive(:find).with('2').and_return(receiver)
        MessageService.stub(:new).and_return(message_service)
      end

      it 'respond_with 201' do
        message_service.should_receive(:send_message)
          .with(receiver, params[:body])

        post :create, params
        expect(response.status).to eql(201)
      end
    end
  end

  describe 'DELETE #destroy' do
    before do
      MessageService.stub(:new).and_return(message_service)
      MessageMapping.should_receive(:find).and_return(message_mapping)
    end

    it 'call delete_conversation method with correct parameter' do
      message_service.should_receive(:delete_conversation)
        .with(message_mapping).and_return(true)
      delete :destroy, id: 1
    end

    context 'when delete_conversation returns true' do
      before do
        message_service.should_receive(:list_of_conversation).and_return(mappings)
        message_service.should_receive(:init_notification).and_return({})
      end

      it 'respond_with 200' do
        message_service.stub(:delete_conversation).with(message_mapping)
          .and_return(true)
        delete :destroy, id: 1 
        expect(response.status).to eql(200)
      end 
    end

    context 'when delete_conversation returns false' do
      it 'respond_with 500' do
        message_service.stub(:delete_conversation).with(message_mapping)
          .and_return(false)
        delete :destroy, id: 1 
        expect(response.status).to eql(500)
      end
    end
  end

  describe 'GET #conversation' do
    let(:messages) { [stub_model(Message, id: 1)] }

    before do
      MessageService.stub(:new).and_return(message_service)
    end

    context 'when message mapping is found' do
      before do
        MessageMapping.should_receive(:find)
          .with('1').and_return(message_mapping)
        message_service.should_receive(:conversation)
          .with(message_mapping,per_page: nil, max_id: nil, min_id: nil)
          .and_return(messages)
      end

      it 'respond_with 200' do
        get :conversation, id: 1

        expect(response.status).to eql(200)
      end

      it 'render the correct response' do
        get :conversation, id: 1

        expect(response.body).to eql(ActiveModel::ArraySerializer.new(messages, each_serializer: MessageSerializer, 
          scope: current_user, 
          root: 'data', 
          meta: { max_id: 1, min_id: 1 }.merge!(controller.send(:message_channel)), 
          meta_key: 'meta_data').to_json)
      end
    end

    context 'when message mapping is not found' do
      it 'respond_with 404' do
        MessageMapping.should_receive(:find)
          .with('1').and_raise ActiveRecord::RecordNotFound
        get :conversation, id: 1
        expect(response.status).to eql(404)
      end
    end    
  end

  describe 'POST #auth' do
    let(:pusher_response) { { auth: '278d425bdf160c' } }
    
    it 'respond_with 200' do
      Pusher::Channel.any_instance.should_receive(:authenticate)
        .with('1234', { user_id: current_user.id, user_info: { name: current_user.fullname } })
        .and_return(pusher_response)
      post :auth, socket_id: 1234, channel_name: 'channel_name'
      expect(response.status).to eql(200)
    end
  end

end
