require 'spec_helper'

describe Api::V1::CommentsController do
  include_context 'api_v1_controller_context'

  let(:commentable){ stub_model Post, id: 5001 }
  let(:user){ current_user }
  let(:service){ CommentService.new(commentable, user) }
  
  let(:comment){ comment1 }
  let(:comment1){ stub_model Comment, id: 113921 }
  let(:comment2){ stub_model Comment, id: 140012 }
  let(:comments){ [comment1, comment2] }

  before do
    Post.stub(:find).with(5001).and_return(commentable)
    controller.stub(:service).and_return(service)
  end

  describe 'GET #index' do
    before do
      commentable.stub_chain(:comments, :recent, :includes, :paginate).and_return(comments)
    end

    it 'respond_with 200' do
      get :index, post_id: 5001
      expect(response.status).to eql(200)
    end

    it 'render comments' do
      get :index, post_id: 5001
      expect(response.body).to include(ActiveModel::ArraySerializer.new(comments).to_json)
    end

    it 'render pagination parameter' do
      get :index, post_id: 5001
      expect(response.body).to include("pagination\":{\"max_id\":140012,\"min_id\":113921}}")
    end
  end

  describe 'POST #create' do
    let(:params){ {post_id: 5001, message: 'comment'} }

    before do
      service.stub(:create).and_return(comment)
    end

    it 'respond_with 201' do
      post :create, params
      expect(response.status).to eql(201)
    end

    it 'create a new comments' do
      service.should_receive(:create)
      post :create, params
    end

    it 'render newly created comments' do
      post :create, params
      expect(response.body).to include(CommentSerializer.new(comment).to_json)
    end

    it 'render post comment count' do
      post :create, params
      expect(response.body).to include("post_comments_count")
    end
  end

  describe 'DELETE #destroy' do
    let(:params){ {post_id: 5001, id: 123321} }

    before do
      service.stub(:delete_comment).with(123321).and_return(true)
    end

    it 'respond_with 200' do
      delete :destroy, params
      expect(response.status).to eql(200)
    end

    it 'create a new comments' do
      service.should_receive(:delete_comment).with(123321)
      delete :destroy, params
    end
  end
end
