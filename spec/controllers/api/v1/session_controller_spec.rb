require 'spec_helper'

describe Api::V1::SessionController do
  include_context 'api_v1_controller_context'

  let(:session_service) { SessionService.new current_user }
  let(:access_token) { stub_model(Doorkeeper::AccessToken, token: 'token') }

  before do
    allow_message_expectations_on_nil
    SessionService.stub(:new).and_return(session_service)
  end

  describe 'POST #sign_in' do
    before do
      session_service.should_receive(:sign_in)
        .with('username', 'password')
        .and_return([current_user, access_token])
    end

    it 'respond_with 200' do
      post :sign_in, username: 'username', password: 'password'
      expect(response.status).to eql(200)
    end

    it 'render user json reponse' do
      post :sign_in, username: 'username', password: 'password'
      expect(response.body).to include(UserWithPrivateDetailsSerializer.new(current_user).to_json)
    end

    it 'render a valid access_token' do
      post :sign_in, username: 'username', password: 'password'
      expect(response.body).to include(access_token.token)
    end
  end

  describe 'DELETE #sign_out' do
    it 'respond_with 200' do
      session_service.should_receive(:sign_out).and_return(nil)
      delete :sign_out
      expect(response.status).to eql(200)
    end
  end

  describe 'POST #sign_in_fb' do
    let(:params){{
      token: 'token-abcd-efgh'
    }}

    before do
      FacebookService.stub_chain(:new, :sign_in).and_return(current_user)
      session_service.should_receive(:issue_token).and_return(access_token)
    end

    it 'respond_with 200' do
      post :sign_in_fb, params
      expect(response.status).to eql(200)
    end

    it 'render user json reponse' do
      post :sign_in_fb, params
      expect(response.body).to include(UserWithPrivateDetailsSerializer.new(current_user).to_json)
    end

    it 'render a valid access_token' do
      post :sign_in_fb, params
      expect(response.body).to include(access_token.token)
    end
  end
end
