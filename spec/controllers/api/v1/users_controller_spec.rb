require 'spec_helper'

describe Api::V1::UsersController do
  include_context 'api_v1_controller_context'

  describe 'GET #me' do
    it 'respond_with 200' do
      get :me
      expect(response.status).to eql(200)
    end

    it 'render current user detail' do
      get :me
      expect(response.body).to eql(UserWithPrivateDetailsSerializer.new(current_user, scope: current_user).to_json)
    end
  end

  describe 'GET #show' do
    context 'when user is found' do
      let(:user) { stub_model User, id: 1 }

      before do
        User.should_receive(:find).with('1').and_return(user) 
        UserProfileService.any_instance
          .stub(:serialized_user_profile_hash)
          .and_return({})
      end
      
      it 'respond_with 200' do
        get :show, id: 1
        expect(response.status).to eql(200)
      end  
    end

    context 'when user is not found' do
      before do
        User.should_receive(:find)
          .with('1').and_raise ActiveRecord::RecordNotFound
      end

      it 'respond_with 404' do
        get :show, id: 1
        expect(response.status).to eql(404)
      end
    end
  end

  describe 'POST #create' do
    let(:params){{
      :email => 'aaa@bbb.com',
      :password => 'aaa@bbb.com' 
    }}

    context 'when succeed' do
      let(:access_token) { stub_model Doorkeeper::AccessToken, token: 'this_is_an_access_token' }
      before do
        UserCreationService.any_instance.stub(:create).and_return(current_user)
        SessionService.any_instance.stub(:issue_token).and_return(access_token)
      end

      it 'respond_with 200' do
        post :create, params
        expect(response.status).to eql(200)
      end

      it 'render newly created user detail' do
        post :create, params
        expect(response.body).to include(UserWithPrivateDetailsSerializer.new(current_user, scope: current_user).to_json)
      end

      it 'render a valid access_token' do
        post :create, params
        expect(response.body).to include(access_token.token)
      end
    end

    context 'when failed' do
      it 'respond_with 422' do
        post :create, params
        expect(response.status).to eql(422)
      end
    end
  end

  describe 'PUT #update' do
    let(:params) { stub_model Hash }

    context 'when succeed' do
      let(:updated_user) { stub_model User, last_name: 'Bond' }
      before do
        UserUpdateService.any_instance.stub(:update).and_return(updated_user)
      end

      it 'respond_with 200' do
        put :update, params
        expect(response.status).to eql(200)
      end

      it 'render updated user detail' do
        put :update, params
        expect(response.body).to include(UserWithPrivateDetailsSerializer.new(updated_user).to_json)
      end
    end

    context 'when failed' do
      it 'respond_with 422' do
        put :update, params
        expect(response.status).to eql(422)
      end
    end
  end

  describe 'GET #stats' do
    before do
      MessageMapping.should_receive(:unreads_count).with(current_user.id)
        .and_return(1)
      current_user.should_receive(:pending_request_users).and_return([double, double])
    end

    it 'respond_with 200' do
      get :stats
      expect(response.status).to eql(200)
    end

    it 'render correct response' do
      get :stats
      response.body.should be_eql({ pending_request_count: 2, new_messages_count: 1 }.to_json)
    end
  end


  context 'tribal_rank endpoints' do
    let(:target_user){stub_model User, id: 30001}
    let(:service){ TribalRankService.new(current_user) }

    before do
      User.stub(:find).with(30001).and_return(target_user)
      controller.stub(:tribal_rank_service).and_return(service)
      service.stub(:endorse).and_return(1337)
      service.stub(:unendorse).and_return(1337)
    end

    describe 'POST #endorse' do
      it 'call endorse' do
        service.should_receive(:endorse).with(target_user)
        post :endorse, id: 30001
      end
      
      it 'respond_with 200' do
        post :endorse, id: 30001
        expect(response.status).to eql(200)
      end

      it 'render target_user tribal_rank' do
        post :endorse, id: 30001
        response.body.should be_eql({ tribal_rank: 1337 }.to_json)
      end
    end

    describe 'DELETE #endorse' do
      it 'call unendorse' do
        service.should_receive(:unendorse).with(target_user)
        delete :unendorse, id: 30001
      end
      
      it 'respond_with 200' do
        delete :unendorse, id: 30001
        expect(response.status).to eql(200)
      end

      it 'render target_user tribal_rank' do
        delete :unendorse, id: 30001
        response.body.should be_eql({ tribal_rank: 1337 }.to_json)
      end
    end
  end

  describe 'GET #connection' do
    context 'when user is found' do
      let(:user) { stub_model User, id: 1 }
      let(:user_a) { stub_model User, id: 2 }
      let(:user_b) { stub_model User, id: 3 }
      let(:users) {[user_a, user_b]}

      before do
        User.should_receive(:find).with(1).and_return(user) 
        user.stub_chain(:friends, :paginate).and_return(users)
      end
      
      it 'respond_with 200' do
        get :connection, id: 1
        expect(response.status).to eql(200)
      end

      it 'render list of user connection' do
        get :connection, id: 1
        expected_response =
        {
          data: [
              {
                id: 2,
                first_name: nil,
                last_name: nil,
                avatar_url: nil,
                tribal_rank: 0
              },
              {
                id: 3,
                first_name: nil,
                last_name: nil,
                avatar_url: nil,
                tribal_rank: 0
              },
          ],
          pagination: {
            max_id: nil,
            min_id: nil
          }
        }

        expect(response.body).to eql(expected_response.to_json)
      end
    end

    context 'when user is not found' do
      before do
        User.should_receive(:find)
          .with(1).and_raise ActiveRecord::RecordNotFound
      end

      it 'respond_with 404' do
        get :connection, id: 1
        expect(response.status).to eql(404)
      end
    end
  end
end
