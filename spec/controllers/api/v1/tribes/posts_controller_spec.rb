require 'spec_helper'

describe Api::V1::Tribes::PostsController do
  include_context 'api_v1_controller_context'

  let(:tribe_a){ stub_model Tribe, id: 1, name: "Running tribe", activity: 'Running', owner_id: 1, users: [current_user] }
  let(:tribe_b){ stub_model Tribe, id: 2, name: "Swimming tribe", activity: 'Swimming', owner_id: 2, users: [current_user] }
  let(:tribe_c){ stub_model Tribe, id: 3, name: "Swimming tribe", activity: 'Swimming', owner_id: 2 }

  describe 'Post' do
    let(:params_a){{
      message: 'My first post to this awesome tribe!',
      lat: 109.9876,
      lon: 12.6543,
      location_name: 'Manhattan',
      id:1
    }}

    let(:params_b){{
      message: 'My post to another tribe!',
      id: 3
    }}

    describe 'create tribes post' do
      let (:user) { stub_model User, id: 1, first_name: 'first', last_name: 'last' }
      before do
        Tribe.stub(:find).with('1').and_return(tribe_a)
        Tribe.stub(:find).with('3').and_return(tribe_c)
        NotificationService.any_instance.stub(:tribe_post_push_notification).and_return(nil)
        Post.any_instance.stub(:notify).and_return(nil)
        User.any_instance.stub_chain(:notification_setting, :tribe_post_freq).and_return('immediate')
        NotificationMailer.stub_chain(:tribe_posts, :deliver).and_return(true)
        NotificationMailer.stub_chain(:tribe_posts, :deliver!).and_return(true)
        Tribe.any_instance.stub(:owner).and_return(user)
      end

      context 'when succeed' do
        it 'respond_with 201' do
          post :create, params_a

          expect(response.status).to eql(201)
        end
      end

      context 'when failed' do
        it 'respond_with 422' do
          post :create, params_a.except(:message)
          expect(response.status).to eql(422)
        end

        it 'respond_with 401 if post to not joined tribe' do
          post :create, params_b
          expect(response.status).to eql(403)
        end
      end
    end

    describe 'index tribes post' do
      let(:p){{
        per_page: 2,
        max_id: 3,
        id: 1
      }}
      
      let(:user1) { stub_model User, id: 1, first_name: "Abdul Haris", last_name: "Ilmawan" }
      let(:user2) { stub_model User, id: 2, first_name: "Acong", last_name: "Ganteng" }
      let(:liked) { [] }
      let(:post1) { stub_model Post, id: 1, message: "my first message", tribe_id: 1, user: user1, created_at: 2.hour.ago }
      let(:post2) { stub_model Post, id: 2, message: "my second message", tribe_id: 1, user: user2, created_at: 1.hour.ago }
      let(:posts_arr) { [post1, post2] }

      let(:result) { {:posts => posts_arr, :user_arr => liked} }
      before do
        Tribe.stub(:find).with('1').and_return(tribe_a)
        PostService.stub_chain(:new, :index).and_return(result)
      end
      
      it 'respond_with 200' do
        get :index, p
        expect(response.status).to eql(200)
      end

      it 'render latest post stream as requested params' do
        get :index, p
        expected_response =
        {
          data: [
              {
                id: 1,
                message: "my first message",
                image: nil,
                location_name: nil,
                lat: nil,
                lon: nil,
                liked: false,
                likes_count: 0,
                comments_count: 0,
                created_at: 2.hour.ago.to_i,
                user: {
                  id:1,
                  first_name: "Abdul Haris",
                  last_name: "Ilmawan",
                  avatar_url: nil,
                  tribal_rank: 0
                }
              },
                          
              {
                id: 2,
                message: "my second message",
                image: nil,
                location_name: nil,
                lat: nil,
                lon: nil,
                liked: false,
                likes_count: 0,
                comments_count: 0,
                created_at: 1.hour.ago.to_i,
                user: {
                  id:2,
                  first_name: "Acong",
                  last_name: "Ganteng",
                  avatar_url: nil,
                  tribal_rank: 0
                }
              },
          ],
          pagination: {
            max_id: 2,
            min_id: 1
          }
        }

      expect(response.body).to eql(expected_response.to_json)
      end
    end

  end
end
