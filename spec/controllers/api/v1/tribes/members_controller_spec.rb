require 'spec_helper'

describe Api::V1::Tribes::MembersController do
  include_context 'api_v1_controller_context'

  let(:tribe_b){ stub_model Tribe, id: 2, name: "Swimming tribe", activity: 'Swimming', owner_id: 2, users: [current_user] }

  describe '.members' do
    context 'when succeed' do
      it 'respond_with 200' do
        Tribe.stub(:find).with('2').and_return(tribe_b)

        get :index, id:2
        expect(response.status).to eql(200)
      end

      it 'render members of the tribes' do
        Tribe.stub(:find).with('2').and_return(tribe_b)
        TribeService.stub_chain(:new, :member_list).and_return(tribe_b.users)
        get :index, id:2
        expect(response.body).to eql(
          ActiveModel::ArraySerializer.new(
            tribe_b.users, 
            root: "data", 
            meta_key: 'pagination',
            meta: { max_id: nil, min_id: nil }
            ).to_json)
      end
    end

    context 'when fail' do
      it 'respond_with 404' do
        expect{Tribe.find}.to raise_error(ActiveRecord::RecordNotFound)
        get :index, id:5
        expect(response.status).to eql(404)
      end
    end

  end

end
