require 'spec_helper'

describe Api::V1::Tribes::InvitesController do
  include_context 'api_v1_controller_context'

  let(:tribe){stub_model Tribe, id: 42, name: "tribename"}

  before do
    Tribe.stub(:find).with(tribe.id).and_return(tribe)    
  end

  describe 'POST #send_invite' do
    context 'twitter' do
      let(:params){{tribe_id: 42, platform_type: 'twitter'}}
      let(:credential){ stub_model SocialCredential, token: 'sotoken', secret: 'sosecret' }
      
      before do
        TribeInvites::TwitterInviteService.any_instance.stub(:send_invite)
        current_user.stub_chain(:social_credentials, :twitter, :first).and_return(credential)
      end

      it 'call send_invites on TwitterInviteService' do
        TribeInvites::TwitterInviteService.any_instance.should_receive(:send_invite)
        post :send_invites, params 
      end

      it 'respond_with 200' do
        post :send_invites, params 
        expect(response.status).to eql(200)
      end
    end

    context 'email' do
      let(:params){{tribe_id: 42, platform_type: 'email'}}

      before do
        TribeInvites::EmailInviteService.any_instance.stub(:send_invite)
      end

      it 'call send_invites on EmailInviteService' do
        TribeInvites::EmailInviteService.any_instance.should_receive(:send_invite)
        post :send_invites, params 
      end

      it 'respond_with 200' do
        post :send_invites, params 
        expect(response.status).to eql(200)
      end
    end

    context 'gotribal' do
      let(:invited_user) {stub_model User, id: 4331}
      let(:params){{tribe_id: 42, platform_type: 'gotribal', user_id: invited_user.id}}

      before do 
        User.stub(:find).with(invited_user.id).and_return(invited_user)
        TribeService.any_instance.stub(:join)
        tribe.stub(:notify)
      end

      it 'make invited_user join the tribe' do
        TribeInvites::GotribalInviteService.any_instance.should_receive(:send_invite)
        post :send_invites,params
      end
    end
  end

  describe 'GET #inviteable' do 
    let(:params){{tribe_id: tribe.id}}
    let(:tribe){stub_model Tribe, id: 42, name: "tribename"}
    let(:friend1){ stub_model User, id: 1032}
    let(:friend2){ stub_model User, id: 21111}
    let(:tribe_members){ [friend2] }
    let(:friends){[friend1, friend2]}
    let(:friends_ids){friends.map(&:id)}
    let(:tribe_members_ids){[friend2.id]}

    before do
      current_user.stub_chain(:friends, :paginate).and_return(friends)
      friend1.stub(:cursor_id).and_return(friend1.id)
      friend2.stub(:cursor_id).and_return(friend2.id)
      friends.stub(:pluck).with(:id).and_return(friends_ids)
      tribe.stub_chain(:users, :where, :pluck).and_return(tribe_members_ids)
    end

    context 'when user belong to tribe' do
      before do
        tribe.stub(:is_tribe_member?).with(current_user).and_return(true)
      end

      it 'respond_with 200' do
        get :inviteable, params
        expect(response.status).to eql(200)
      end

      it 'render expected response' do
        get :inviteable, params

        expected_response = 
          {
            data: [ 
              { 
                id: 1032, 
                first_name: nil,
                last_name: nil,
                avatar_url: nil,
                tribal_rank: 0,
                tribe_member: false
              },
              { 
                id: 21111, 
                first_name: nil,
                last_name: nil,
                avatar_url: nil,
                tribal_rank: 0,
                tribe_member: true
              }
            ],
            pagination: {
              max_id: 21111,
              min_id: 1032
            }
          }

        expect(response.body).to eql(expected_response.to_json)
      end
    end

    context 'when user did not belong to tribe' do
      before do
        tribe.stub(:is_tribe_member?).with(current_user).and_return(false)
      end

      it 'respond_with 403' do
        get :inviteable, params
        expect(response.status).to eql(403)
      end
    end
  end
end
