require 'spec_helper'

describe Api::V1::TribesController do
  include_context 'api_v1_controller_context'

  let(:tribe_a){ stub_model Tribe, id: 1, name: "Running tribe", activity: 'Running', owner_id: 1, users: [current_user] }
  let(:tribe_b){ stub_model Tribe, id: 2, name: "Swimming tribe", activity: 'Swimming', owner_id: 2, users: [current_user] }
  let(:tribe_c){ stub_model Tribe, id: 3, name: "Swimming tribe", activity: 'Swimming', owner_id: 2 }

  describe '.create' do
    let(:params){{
      name: 'My running tribe',
      activity: 'Running',
      location_name: 'Manhattan'
    }}

    context 'when succeed' do
      before do
        TribeService.any_instance.stub(:create).and_return(tribe_a)
      end
      it 'respond_with 201' do
        post :create, params
        expect(response.status).to eql(201)
      end
    end

    context 'when name not set' do
      it 'respond_with 422' do
        post :create, params.except(:name)
        expect(response.status).to eql(422)
      end
    end

    context 'when activity not set' do
      it 'respond_with 422' do
        post :create, params.except(:activity)
        expect(response.status).to eql(422)
      end
    end
  end

  describe 'Indexing' do
    let(:p){{
      per_page: 3,
      max_id: 3
    }}

    context 'index' do
      let(:tribes_arr) { [tribe_a, tribe_b, tribe_c] }
      let(:user_tribes) { [1] }

      before do
        TribeService.stub_chain(:new, :index).and_return([tribes_arr, user_tribes])
      end

      it 'respond_with 200' do
        get :index, p, format: :json 
        expect(response.status).to eql(200)
      end

      it 'render tribe list as requested params' do
        get :index, p, format: :json
        expect(response.body).to eql(ActiveModel::ArraySerializer.new(tribes_arr, joined_ids: user_tribes, 
          root: "data", each_serializer: TribeDetailSerializer).to_json)
      end
      
    end

    context 'owned tribes' do
      let(:result) { [tribe_a] }
        
      before do
        TribeService.stub_chain(:new, :owned).and_return(result)
      end

      it 'respond_with 200' do
        get :owned, p, format: :json 
        expect(response.status).to eql(200)
      end

      it 'render owned tribes requested params' do
        get :owned, p, format: :json
        expect(response.body).to eql(ActiveModel::ArraySerializer.new(result, root: "data", 
          each_serializer: TribeDetailSerializer).to_json)
      end

    end

    context 'joined tribes' do
      let(:result) { [tribe_a, tribe_b] }
      
      before do
        TribeService.stub_chain(:new, :joined).and_return(result)
      end
      
      it 'respond_with 200' do
        get :joined, p, format: :json 
        expect(response.status).to eql(200)
      end

      it 'render joined tribes as requested params' do
        get :joined, p, format: :json
        expect(response.body).to eql(ActiveModel::ArraySerializer.new(result, root: "data", each_serializer: TribeDetailSerializer).to_json)
      end
    end

  end

  describe 'Membership' do

    describe '.join' do
      context 'when succeed' do
        it 'respond_with 200' do
          Tribe.stub(:find).with('3').and_return(tribe_c)
          post :join, id:3
          expect(response.status).to eql(200)
        end
      end

      context 'when fail' do
        it 'respond_with 404' do
          expect{Tribe.find}.to raise_error(ActiveRecord::RecordNotFound)
          post :join, id:2
          expect(response.status).to eql(404)
        end
      end
    end

    describe '.leave' do
      context 'when succeed' do
        it 'respond_with 200' do
          Tribe.stub(:find).with('2').and_return(tribe_b)
          delete :leave, id:2
          expect(response.status).to eql(200)
        end
      end

      context 'when fail' do
        it 'respond_with 404 if try to leave not existing group' do
          expect{Tribe.find}.to raise_error(ActiveRecord::RecordNotFound)
          delete :leave, id:4
          expect(response.status).to eql(404)
        end

        it 'respond_with 422 if try to leave owned group' do
          Tribe.stub(:find).with('1').and_return(tribe_a)
          delete :leave, id:1
          expect(response.status).to eql(422)
        end
      end
    end
    
  end

end
