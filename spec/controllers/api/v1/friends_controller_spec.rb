require 'spec_helper'

describe Api::V1::FriendsController do
  include_context 'api_v1_controller_context'

  let(:user1) { stub_model User, id: 212 }
  let(:user2) { stub_model User, id: 121 }
  let(:friends_array) {[user1, user2]}
  let!(:service) { FriendshipService.new(current_user) }

  before do
    FriendshipService.stub(:new).and_return(service)
    User.stub(:find).with(212).and_return(user1)
  end

  describe 'GET index' do
    before do
      current_user.stub_chain(:friends, :preload, :paginate).and_return(friends_array)
    end

    it 'respond_with 200' do
      get :index
      expect(response.status).to eql(200)
    end

    it 'render current_user.friends' do
      get :index
      expect(response.body).to eql(
        ActiveModel::ArraySerializer.new(
          friends_array, 
          root: "data", 
          each_serializer: UserWithFitnessPursuitSerializer,
          meta_key: 'pagination',
          meta: { max_id: nil, min_id: nil }
        ).to_json)
    end
  end

  describe 'GET pending_request_users' do
    before do
      current_user.stub_chain(:pending_request_users, :paginate).and_return(friends_array)
    end

    it 'respond_with 200' do
      get :pending_request_users
      expect(response.status).to eql(200)
    end

    it 'render current_user.pending_request_users' do
      get :pending_request_users
      expect(response.body).to eql(
        ActiveModel::ArraySerializer.new(
          friends_array, 
          root: "data", 
          meta_key: 'pagination',
          meta: { max_id: nil, min_id: nil }
        ).to_json)
    end
  end

  describe 'GET received_request_users' do
    before do
      current_user.stub_chain(:received_request_users, :paginate).and_return(friends_array)
    end

    it 'respond_with 200' do
      get :received_request_users
      expect(response.status).to eql(200)
    end

    it 'render current_user.received_request_users' do
      get :received_request_users
      expect(response.body).to eql(
        ActiveModel::ArraySerializer.new(
          friends_array, 
          root: "data", 
          meta_key: 'pagination',
          meta: { max_id: nil, min_id: nil }
        ).to_json)
    end
  end

  describe 'POST add' do
    before do
      service.stub(:add).and_return("some_status")
    end
    
    it 'respond_with 200' do
      post :add, id: 212
      expect(response.status).to eql(200)
    end

    it 'render the status' do
      post :add, id: 212
      expect(response.body).to include("some_status")
    end
  end

  describe 'DELETE remove' do
    before do
      service.stub(:remove).and_return("deleted_status")
    end
    
    it 'respond_with 200' do
      delete :remove, id: 212
      expect(response.status).to eql(200)
    end

    it 'render the status' do
      delete :remove, id: 212
      expect(response.body).to include("deleted_status")
    end
  end

end
