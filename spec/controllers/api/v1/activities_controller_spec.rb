require 'spec_helper'

describe Api::V1::ActivitiesController do
  include_context 'api_v1_controller_context'

  let(:user){ current_user }

  describe 'GET #index' do

    it 'respond_with 200' do
      get :index
      expect(response.status).to eql(200)
    end
  end
end