require 'spec_helper'

describe UserUpdateService do
  let(:user) { stub_model User, id: 1001 }
  let(:user_update_service) { UserUpdateService.new(user) }

  describe 'update' do
    let(:params){ stub_model Hash }
    let(:user_params){ stub_model Hash }

    before do
      user_update_service.stub(:prepare_params).and_return(user_params)
      user.stub(:update!).and_return(true)
      User.any_instance.stub(:save!).and_return(true)
      UserDetailService.any_instance.stub(:set_mass).and_return(true)
    end

    it 'update the user' do
      user.should_receive(:update!).with(user_params)
      user_update_service.update(params)
    end

    it 'set the user details' do
      UserDetailService.any_instance.should_receive(:set_mass).with(params)
      user_update_service.update(params)
    end
  end
end
