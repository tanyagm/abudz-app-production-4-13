require 'spec_helper'

describe UserProfileService do
  let(:current_user) { stub_model User, id: 2 }
  let(:friends) { [current_user] }

  describe '#serialized_user_profile_hash' do
    context 'When current user viewing his own profile' do
      it 'returns a correct serialized hash of user with all details and friends object' do
        hash = { 
          user: UserWithPrivateDetailsSerializer.new(current_user)
        }
        current_user.stub(:friends).and_return(friends)
        current_user.stub_chain(:friends, :preload, :limit).and_return(friends)

        UserProfileService.new(current_user, current_user).serialized_user_profile_hash.to_json.should be_eql hash.to_json
      end
    end

    context 'When viewing other user profile' do
      let(:other_user) { stub_model User, id: 3 }
      let(:expected_hash) { { 
            user: UserWithPublicDetailsSerializer.new(other_user, scope: current_user, is_friend: is_friend)
          } }
      
      before do
        current_user.stub(:is_friend?).with(other_user).and_return(is_friend)
        other_user.stub(:friends).and_return(friends)
        other_user.stub_chain(:friends, :preload, :limit).and_return(friends)
      end

      context 'specified user is a friend' do
        let(:is_friend) { true }  
        it 'returns a correct serialized hash of user with public details and friends object' do        
          UserProfileService.new(current_user, other_user).serialized_user_profile_hash.to_json.should be_eql expected_hash.to_json
        end
      end

      context 'specified user is not a friend' do
        let(:is_friend) { false }
        it 'returns a correct serialized hash of user with public details and friends object' do        
          UserProfileService.new(current_user, other_user).serialized_user_profile_hash.to_json.should be_eql expected_hash.to_json
        end
      end
    end
  end
end