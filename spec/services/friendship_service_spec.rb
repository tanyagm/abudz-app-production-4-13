require 'spec_helper'

describe FriendshipService do
  let(:user) { stub_model User, id:2001, email: 'a@b.com', password: 'correct_password'}
  let(:user2) { stub_model User, id:3030, email: 'a@b.com', password: 'correct_password'}
  let(:service) {FriendshipService.new(user)}
  let(:request) { nil }
  let(:sent_request) { nil }

  before do
    User.stub(:find_by).with(id: user.id).and_return(user)
    User.stub(:find).with(user.id).and_return(user)
    User.stub(:find_by).with(id: user2.id).and_return(user2)
    User.stub(:find).with(user2.id).and_return(user2)
    User.any_instance.stub_chain(:notification_setting, :activebudz_request_freq).and_return('immediate')
    NotificationMailer.stub_chain(:friend_requests, :deliver).and_return(true)
    NotificationMailer.stub_chain(:friend_requests, :deliver!).and_return(true)
  end

  describe 'public method' do
    before do
      service.stub(:request).and_return(request)
      service.stub(:sent_request).and_return(sent_request)
      service.stub(:resend_request).and_return(true)
      service.stub(:create_new_request).and_return(true)
      service.stub(:add_to_hidden_user_list).and_return(true)
      FriendRelation.any_instance.stub(:resend)
      FriendRelation.any_instance.stub(:accept)
      FriendRelation.any_instance.stub(:reject)  
    end

    describe 'add' do
      context 'request exist' do

        context 'PENDING request' do
          let(:request) { stub_relation FriendRelation::PENDING }
          it 'accept the request' do
            request.should_receive(:accept)
            service.add(user2)
          end
          it 'return :accepted' do
            expect(service.add(user2)).to eql(:accepted)
          end
        end

        context 'REJECTED request' do
          let(:request) { stub_relation FriendRelation::REJECTED }
          it 'accept the request' do
            request.should_receive(:accept)
            service.add(user2)
          end
          it 'return :accepted' do
            expect(service.add(user2)).to eql(:accepted)
          end
        end

        context 'APPROVED request' do
          let(:request) { stub_relation FriendRelation::APPROVED }
          it 'do not re-accept the request' do
            request.should_not_receive(:accept)
            service.add(user2)
          end
          it 'return :friend' do
            expect(service.add(user2)).to eql(:friend)
          end
        end

      end

      context 'request not exist' do

        context 'sent_request not exist' do
          it 'create a new request' do
            service.should_receive(:create_new_request)
            service.add(user2)
          end
          it 'return :sent' do
            expect(service.add(user2)).to eql(:sent)
          end
        end

        context 'sent_request exist' do
          let(:sent_request) { stub_relation FriendRelation::PENDING }
          it 'resent request' do
            service.should_receive(:resend_request)
            service.add(user2)
          end
          it 'return :resent' do
            expect(service.add(user2)).to eql(:resent)
          end
        end

      end
    end

    describe 'remove' do

      context 'request not exist' do
        context 'sent_request exist' do
          let(:sent_request) { stub_relation FriendRelation::PENDING }
          it 'delete the request' do
            sent_request.should_receive(:destroy)
            service.remove(user2)
          end
          it 'return :removed' do
            expect(service.remove(user2)).to eql(:removed)
          end
        end
        context 'sent_request not exist' do
          it 'create_hidden_relation' do
            service.should_receive(:add_to_hidden_user_list)
            service.remove(user2)
          end
          it 'return :no_relation' do
            expect(service.remove(user2)).to eql(:hidden)
          end
        end
      end

      context 'request exist' do

        context 'NOT APPROVED request' do
          let(:request) { stub_relation FriendRelation::PENDING }
          it 'reject the request' do
            request.should_receive(:reject)
            service.remove(user2)
          end
          it 'return :rejected' do
            expect(service.remove(user2)).to eql(:rejected)
          end
        end  

        context 'APPROVED request' do
          let(:request) { stub_relation FriendRelation::APPROVED }
          it 'destroy the relation' do
            request.should_receive(:destroy)
            service.remove(user2)
          end
          it 'return :unfriended' do
            expect(service.remove(user2)).to eql(:unfriended)
          end
        end
      end
    end
  end

  describe 'private method' do
    describe 'resend_request' do
      let(:sent_request) { stub_relation FriendRelation::REJECTED }
      before do
        service.stub(:sent_request).and_return(sent_request)
        service.stub(:create_new_request).and_return(sent_request)
        sent_request.stub(:destroy).and_return(true)
      end
      it 'destroy sent_request' do
        sent_request.should_receive(:destroy)
        service.send(:resend_request)
      end
      it 'create a new request' do
        service.should_receive(:create_new_request)
        service.send(:resend_request)
      end
    end
  end

  private
  
  def stub_relation(status)
    stub_model FriendRelation, id: 2000, status: status  
  end

end
