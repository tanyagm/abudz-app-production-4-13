require 'spec_helper'

describe SearchTribeService do
  let(:tribe_owner) { stub_model User, id: 5, email: 'test@gm.com' }
  let(:tribe) { stub_model Tribe, id: 1, owner: tribe_owner, name: 'Lorem', description: 'Ipsum dolor sit amet' }
  let(:search_tribe_service) { SearchTribeService.new(tribe) }

  describe 'self.create' do

    it 'should add tribe to index using resque' do
      Resque.should_receive(:enqueue).with(Jobs::AddTribeToIndex, tribe.id)
      SearchTribeService.send(:create, tribe.id)
    end
  end

  describe 'self.update' do

    it 'should update tribe in index using resque' do
      Resque.should_receive(:enqueue).with(Jobs::UpdateIndexedTribe, tribe.id)
      SearchTribeService.send(:update, tribe.id)
    end
  end

  describe 'init_type' do
    it 'should initialize index type mapping' do
      Tire.should_receive(:index)
      SearchTribeService.send(:init_type)
    end
  end

  describe 'create' do
    before do
      FakeWeb.allow_net_connect = false
    end

    context 'when success' do
      before do
        FakeWeb.register_uri :any, %r(#{Tire::Configuration.url}), body: '{}'
        tribe.stub_chain(:image, :url).and_return("image.jpg")
      end

      it 'should return tribe detail' do
        output = search_tribe_service.send(:create)
        search_tribe_service.send(:create).should include(
          type: AppConfig.elasticsearch.type_tribe,
          id: tribe.id,
          owner_id: tribe_owner.id,
          name: tribe.name,
          activity: tribe.activity,
          description: tribe.description,
          location_name: tribe.location_name,
          image_url: tribe.image.url,
          lat: tribe.lat,
          lon: tribe.lon
          )
      end
    end

    context 'when failed' do
      before do
        FakeWeb.register_uri :any, %r(#{Tire::Configuration.url}), body: '{}', status: ["400","Bad Request"]
      end

      it 'should return nil' do
        search_tribe_service.send(:create).should be_nil
      end
    end
  end

  describe 'destroy' do
    before do
      FakeWeb.register_uri :any, %r(#{Tire::Configuration.url}), body: '{}'
    end

    it 'should remove tribe from index' do
      index = double('index')
      index.should_receive(:remove).with(AppConfig.elasticsearch.type_tribe,tribe.id)
      Tire.should_receive(:index).with(AppConfig.elasticsearch.index).and_return(index)

      search_tribe_service.send(:destroy)
    end
  end

  describe 'search' do
    before do
      FakeWeb.allow_net_connect = false
      FakeWeb.register_uri :any, %r(#{Tire::Configuration.url}), body: '{}'
    end

    context 'when failed' do
      it 'should return empty array when no keyword provided' do
        SearchTribeService.send(:search, tribe_owner).should eq([])
      end 
    end

    context 'when succeed' do
      it 'should return serialized output' do
        SearchTribeService.should_receive(:serialize).with(tribe_owner, an_instance_of(Tire::Results::Collection))

        SearchTribeService.send(:search, tribe_owner, keyword: 'test')
      end
    end

  end

end