require 'spec_helper'

describe TribeService do
  let(:user){ stub_model User, id: 13921 }
  let(:params){nil}
  let(:service){TribeService.new(params, user)}

  describe 'join' do
    let(:tribe){stub_model Tribe, id: 2031}
    let(:tribe_users){[]}

    before do
      tribe.stub(:users).and_return(tribe_users)
      tribe_users.stub(:<<)
    end
    
    context 'user is a member of tribe' do
      before do
        tribe.stub(:is_tribe_member?).with(user).and_return(true)
      end

      it 'do not add a new tribe member' do
        tribe_users.should_not_receive(:<<)
        service.join(tribe)
      end
    end
    
    context 'user is not a member of tribe' do
      before do
        tribe.stub(:is_tribe_member?).with(user).and_return(false)
      end

      it 'add a new tribe member' do
        tribe_users.should_receive(:<<)
        service.join(tribe)
      end
    end
  end
end
