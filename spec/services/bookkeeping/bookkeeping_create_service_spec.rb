require 'spec_helper'

describe Bookkeeping::BookkeepingCreateService do
  let (:inviter) {stub_model User, id: 42}
  let (:service) {Bookkeeping::BookkeepingCreateService.new(inviter)}

  describe 'create' do
    let (:source_type) {ProspectiveUser::SOURCE_EMAIL}
    let (:source_id) {"johndoe@mail.com"}
    let (:invited_users) {[invited_user]} 
    let (:invited_user) {stub_model ProspectiveUser}

    before do
      inviter.stub(:invited_users).and_return(invited_users)
      invited_users.stub(:find_or_create)
    end

    it 'create a new invited users' do
      invited_users.should_receive(:find_or_create).with(source_type, source_id)
      service.create(source_type, source_id)
    end
  end
end
