require 'spec_helper'

describe Bookkeeping::BookkeepingAttachService do
  let (:user) {stub_model User, id: 42}
  let (:service) {Bookkeeping::BookkeepingAttachService.new(user)}
  let (:source_type) {ProspectiveUser::SOURCE_EMAIL}
  let (:source_id) {"johndoe@mail.com"}

  describe 'attach' do
    let (:prospective_users) {[prospective_tribe_member, prospective_friend]} 
    let (:prospective_tribe_member) {stub_model ProspectiveUser, inviter: inviter_user}
    let (:prospective_friend) {stub_model ProspectiveUser, inviter: inviter_tribe}
    let (:inviter_user) {stub_model User, id: 20043}
    let (:inviter_tribe) {stub_model Tribe, id: 1042}

    before do
      ProspectiveUser.stub_chain(:preload, :source).and_return(prospective_users)
      FriendshipService.any_instance.stub(:add)
      TribeService.any_instance.stub(:join)
    end

    it 'create a pending request from inviter_user to user' do
      FriendshipService.any_instance.should_receive(:add).with(user)
      service.attach(source_type, source_id)
    end

    it 'make user join inviter_tribe' do
      TribeService.any_instance.should_receive(:join).with(inviter_tribe)
      service.attach(source_type, source_id)
    end

    it 'destroy all prospective_users' do
      prospective_tribe_member.should_receive(:destroy)
      prospective_friend.should_receive(:destroy)
      service.attach(source_type, source_id)
    end
  end

  describe 'enqueue_job' do
    before do
      Resque.stub(:enqueue_in)
    end

    it 'enqueue Jobs::BookkeepingAttach' do
      Resque.should_receive(:enqueue_in).with(
          an_instance_of(Fixnum), 
          Jobs::BookkeepingAttach,
          user.id, 
          source_type,
          source_id)
      Bookkeeping::BookkeepingAttachService.enqueue_job(user.id, source_type, source_id)
    end
  end


  describe 'enqueue_job_for_credential' do
    before do
      Bookkeeping::BookkeepingAttachService.stub(:enqueue_job)
    end

    context 'twitter credential' do
      let (:twitter_credential) { stub_model SocialCredential, social_type: SocialCredential::TWITTER}
      it 'enqueue proper Jobs::BookkeepingAttach' do
        Bookkeeping::BookkeepingAttachService.should_receive(:enqueue_job).with(
            user.id, 
            ProspectiveUser::SOURCE_TWITTER,
            twitter_credential.uid)
        Bookkeeping::BookkeepingAttachService.enqueue_job_for_credential(user.id, twitter_credential)
      end
    end
  end
end
