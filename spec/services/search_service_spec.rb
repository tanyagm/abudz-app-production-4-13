require 'spec_helper'

describe SearchService do
  let(:user) { stub_model User, id: 1 }
  let(:search_service) { SearchService.new(user) }
  let(:tire_index) { Tire.index(AppConfig.elasticsearch.index) }

  before do
    search_service.stub(:save_suggested_results).and_return(nil)
  end

  describe 'initialize' do
    let(:user_detail_service) { UserDetailService.new(user) }

    before do
      user_detail_service.stub(:get).and_return("")
    end

    it 'should store user details' do
      UserDetailService.should_receive(:new).with(user).and_return(user_detail_service)
      user_detail_service.should_receive(:get).with('fitness_pursuit')
      user_detail_service.should_receive(:get).with('fitness_level')
      user_detail_service.should_receive(:get).with('weekly_activities')
      user_detail_service.should_receive(:get).with('lifestyle')
      user_detail_service.should_receive(:get).with('age_group')
      user_detail_service.should_receive(:get).with('living_place')
      user_detail_service.should_receive(:get).with('workout_time')
      user_detail_service.should_receive(:get).with('lat')
      user_detail_service.should_receive(:get).with('lon')

      SearchService.send(:new, user)
    end
  end

  describe 'self.create' do
    before do
      FakeWeb.allow_net_connect = false
    end

    it 'should add user via resque' do
      Resque.should_receive(:enqueue).with(Jobs::AddUserToIndex, user.id)

      SearchService.send(:create, user.id)
    end

    context 'when success' do
      before do
        FakeWeb.register_uri :any, %r(#{Tire::Configuration.url}), body: '{}'
      end

      it 'should return user details' do
        search_service.send(:create).should include(:id, :first_name, :last_name, :email, :avatar, :updated_at, :gender, :gender_preference, :role, :role_preference, :location)
      end
    end

    context 'when failed' do
      before do
        FakeWeb.register_uri :any, %r(#{Tire::Configuration.url}), body: '{}', status: ["400","Bad Request"]
      end

      it 'should return nil' do
        search_service.send(:create).should be_nil
      end
    end
  end

  describe 'self.update' do
    it 'should update user via resque' do
      Resque.should_receive(:enqueue).with(Jobs::UpdateIndexedUser, user.id)

      SearchService.send(:update, user.id)
    end
  end

  describe 'init_index' do

    it 'should recreate index' do
      Tire.should_receive(:index).with(an_instance_of(String))

      SearchService.send(:init_index)
    end
  end

  describe 'create' do
    it 'should add user data to index' do
      Tire.should_receive(:index).with(an_instance_of(String))
      search_service.send(:create)
    end
  end

  describe 'update' do
    it 'should update user in index' do
      Tire.should_receive(:index).with(an_instance_of(String))
      search_service.send(:update)
    end
  end

  describe 'search_suggested' do

    before do
      FakeWeb.allow_net_connect = false
      search_service.stub(:custom_score_algorithm).and_return("script")
    end

    it 'should construct query' do
      FakeWeb.register_uri :any, %r(#{Tire::Configuration.url}), body: '{}'

      search_service.should_receive(:construct_suggestion_queries).with(an_instance_of(Array), an_instance_of(Array), an_instance_of(Array), an_instance_of(String), an_instance_of(Hash))
      .and_return({})

      search_service.send(:search_suggested, {})
    end

    context 'when succees' do
      before do
        FakeWeb.register_uri :any, %r(#{Tire::Configuration.url}), body: '{}'
      end

      it 'should return elasticsearch results' do

      end
    end

    context 'when failed' do
      before do
        FakeWeb.register_uri :any, %r(#{Tire::Configuration.url}), body: '{}', status: ["400","Bad Request"]
      end

      it 'should raise exception' do
        expect { search_service.send(:search_suggested) }.to raise_error(Exceptions::SearchRequestFailed)
      end
    end
  end
end