require 'spec_helper'

describe UserCreationService do

  describe 'create' do
    let(:params){ stub_model Hash }
    let(:user_params){ stub_model Hash }
    let(:user) { stub_model User, email: 'sambya@icehousecorp.com', id: 1 }
    let(:user_creation_service){ UserCreationService.new(params) }

    before do
      UserCreationService.any_instance.stub(:prepare_params).and_return(user_params)
      User.stub(:create!).and_return(user)
      User.any_instance.stub(:save!).and_return(true)
      Resque.stub(:enqueue_in).and_return(true)
      Bookkeeping::BookkeepingAttachService.stub(:enqueue_job)
    end
  
    it 'return created user' do
      expect(user_creation_service.create).to eql(user)
    end

    it 'create a new user' do
      User.should_receive(:create!).with(user_params)
      user_creation_service.create
    end

    it 'set the user details' do
      UserDetailService.any_instance.should_receive(:set_mass).with(params)
      user_creation_service.create
    end

    it 'enqueue email new user job' do
      Resque.should_receive(:enqueue_in).with(an_instance_of(Fixnum), Jobs::EmailNewUser, an_instance_of(Fixnum))
      user_creation_service.create
    end
    
    it 'enqueue bookkeeping attach job' do
      Bookkeeping::BookkeepingAttachService.should_receive(:enqueue_job).with(user.id, ProspectiveUser::SOURCE_EMAIL, user.email)
      user_creation_service.create
    end
  end
end
