require 'spec_helper'

describe NotificationService do
  let(:sender) { stub_model User, id: 1, email: 'sender@ipsum.com' }
  let(:receiver) { stub_model User, id: 2, email: 'receiver@ipsum.com' }
  let(:current_user) { stub_model User, id: 3, email: 'user@lorem.com' }
  let(:alert) { "alert" }
  let(:notification_service) { NotificationService.new }

  before do
    notification_service.stub(:send_push_notification)
    Resque.stub(:enqueue).and_return(true)
  end

  describe '#send bulk comment notification' do
    before do
      receiver.stub(:comment_notifications_count).and_return(0)
    end

    it 'should check notification count' do
      receiver.should_receive(:comment_notifications_count)

      notification_service.send(:send_bulk_comment_notification, receiver)
    end

    context 'when notification is 0' do
      it 'should not send push notification' do
        receiver.should_not_receive(:send_push_notification)

        notification_service.send(:send_bulk_comment_notification, receiver)
      end
    end

    context 'when notification > 0' do
      before do
        receiver.stub(:comment_notifications_count).and_return(1)
      end

      it 'should send push notification' do
        notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String))

        notification_service.send(:send_bulk_comment_notification, receiver)
      end
    end
  end

  describe '#send bulk friend notification' do
    before do
      receiver.stub(:friend_notifications_count).and_return(0)
    end

    it 'should check notification count' do
      receiver.should_receive(:friend_notifications_count)

      notification_service.send(:send_bulk_friend_notification, receiver)
    end

    context 'when notification is 0' do
      it 'should not send push notification' do
        receiver.should_not_receive(:send_push_notification)

        notification_service.send(:send_bulk_friend_notification, receiver)
      end
    end

    context 'when notification > 0' do
      before do
        receiver.stub(:friend_notifications_count).and_return(1)
      end

      it 'should send push notification' do
        notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String))

        notification_service.send(:send_bulk_friend_notification, receiver)
      end
    end
  end

  describe '#send bulk tribe post notification' do
    before do
      receiver.stub(:tribe_post_notifications_count).and_return(0)
    end

    it 'should check notification count' do
      receiver.should_receive(:tribe_post_notifications_count)

      notification_service.send(:send_bulk_tribe_post_notification, receiver)
    end

    context 'when notification is 0' do
      it 'should not send push notification' do
        receiver.should_not_receive(:send_push_notification)

        notification_service.send(:send_bulk_tribe_post_notification, receiver)
      end
    end

    context 'when notification > 0' do
      before do
        receiver.stub(:tribe_post_notifications_count).and_return(1)
      end

      it 'should send push notification' do
        notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String))

        notification_service.send(:send_bulk_tribe_post_notification, receiver)
      end
    end
  end

  describe '#send bulk tribe invitation notification' do
    before do
      receiver.stub(:tribe_invitation_notifications_count).and_return(0)
    end

    it 'should check notification count' do
      receiver.should_receive(:tribe_invitation_notifications_count)

      notification_service.send(:send_bulk_tribe_invitation_notification, receiver)
    end

    context 'when notification is 0' do
      it 'should not send push notification' do
        receiver.should_not_receive(:send_push_notification)

        notification_service.send(:send_bulk_tribe_invitation_notification, receiver)
      end
    end

    context 'when notification > 0' do
      before do
        receiver.stub(:tribe_invitation_notifications_count).and_return(1)
      end

      it 'should send push notification' do
        notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String))

        notification_service.send(:send_bulk_tribe_invitation_notification, receiver)
      end
    end
  end

  describe '#send bulk message notification' do
    before do
      receiver.stub(:message_notifications_count).and_return(0)
    end

    it 'should check notification count' do
      receiver.should_receive(:message_notifications_count)

      notification_service.send(:send_bulk_message_notification, receiver)
    end

    context 'when notification is 0' do
      it 'should not send push notification' do
        receiver.should_not_receive(:send_push_notification)

        notification_service.send(:send_bulk_message_notification, receiver)
      end
    end

    context 'when notification > 0' do
      before do
        receiver.stub(:message_notifications_count).and_return(1)
      end

      it 'should send push notification' do
        notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String))

        notification_service.send(:send_bulk_message_notification, receiver)
      end
    end
  end

  describe '#send endorse notification' do
    it 'should enqueue endorse notification' do
      Resque.should_receive(:enqueue).with(Jobs::PushEndorseNotification, sender.id, receiver.id)

      NotificationService.send(:send_endorse_notification, sender, receiver)
    end
  end

  describe '#endorse push notification' do
    it 'should enqueue endorse notification' do
      notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String), user_id: sender.id)

      notification_service.send(:endorse_push_notification, sender, receiver)
    end
  end

  describe '#send friend request' do
    it 'should enqueue endorse notification' do
      Resque.should_receive(:enqueue).with(Jobs::PushFriendRequest, current_user.id, receiver.id, :sent)

      NotificationService.send(:send_friend_request, current_user, receiver, :sent)
    end
  end

  describe '#friend push notification' do
    before do
      receiver.stub_chain(:notification_setting, :activebudz_request_freq).and_return('immediate')
    end

    context 'when user send new friend request' do
      it 'send add friend push notification' do
        notification_service.should_receive(:add_friend_push_notification).with(sender,receiver)

        notification_service.send(:friend_push_notification,sender, receiver, :sent)
      end
    end

    context 'when user accept friend request' do
      it 'send accept friend push notification' do
        notification_service.should_receive(:accepted_friend_push_notification).with(sender,receiver)

        notification_service.send(:friend_push_notification,sender, receiver, :accepted)
      end
    end

  end

  describe '#add friend push notification' do
    before do
      receiver.stub_chain(:received_request_users,:count).and_return(1)
    end

    it 'should send add friend push notification' do
      notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String), pending_friend_id: sender.id, pending_request: receiver.received_request_users.count)

      notification_service.send(:add_friend_push_notification, sender, receiver)
    end
  end

  describe '#accepted friend push notification' do
    before do
      receiver.stub_chain(:received_request_users,:count).and_return(1)
    end

    it 'should send add friend push notification' do
      notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String), accepted_friend_id: sender.id, pending_request: receiver.received_request_users.count)

      notification_service.send(:accepted_friend_push_notification, sender, receiver)
    end
  end

  describe '#tribe post push notification' do
    let (:tribe) { stub_model Tribe, id: 1, name: 'my tribe' }
    let (:post) { stub_model Post, id: 1 }
    let (:tribe_owner) { stub_model User, id: 345 }

    before do
      post.stub_chain(:user,:fullname) { 'user' }
      post.stub(:tribe).and_return(tribe)
      tribe.stub(:owner).and_return(tribe_owner)
      tribe_owner.stub_chain(:notification_setting, :tribe_post_freq).and_return('weekly')
    end

    it 'get post tribe' do
      post.should_receive(:tribe).and_return(tribe)

      notification_service.send(:tribe_post_push_notification, post)
    end

    context 'when notification frequency is immediate' do
      before do
        tribe_owner.stub_chain(:notification_setting, :tribe_post_freq).and_return('immediate')
      end

      it 'send push notification' do
        notification_service.should_receive(:send_push_notification).with(tribe_owner, an_instance_of(String), tribe_id: tribe.id)

        notification_service.send(:tribe_post_push_notification, post)
      end
    end

    context 'when notification frequency is not immediate' do
      before do
        tribe_owner.stub_chain(:notification_setting, :tribe_post_freq).and_return('weekly')
      end

      it 'did not send push notification' do
        notification_service.should_not_receive(:send_push_notification)

        notification_service.send(:tribe_post_push_notification, post)
      end
    end
  end

  describe '#tribe invitation push notification' do
    let (:tribe) { stub_model Tribe, id: 1, name: 'my tribe' }
    let (:tribe_owner) { stub_model User, id: 345 }

    context 'when notification frequency is immediate' do
      before do
        tribe.stub(:owner).and_return(tribe_owner)
        tribe_owner.stub_chain(:notification_setting, :tribe_invitation_freq).and_return('immediate')
      end

      it 'send push notification' do
        notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String), tribe_id: tribe.id)

        notification_service.send(:tribe_invitation_push_notification, sender, receiver, tribe)
      end
    end

    context 'when notification frequency is not immediate' do
      before do
        tribe.stub(:owner).and_return(tribe_owner)
        tribe_owner.stub_chain(:notification_setting, :tribe_invitation_freq).and_return('weekly')
      end

      it 'did not send push notification' do
        notification_service.should_not_receive(:send_push_notification)

        notification_service.send(:tribe_invitation_push_notification, sender, receiver, tribe)
      end
    end
  end

  describe '#comment push notification' do
    let (:comment) { stub_model Comment, id: 1, user: sender }
    let (:commentable) { stub_model Post, id: 1 }

    before do
      comment.stub(:commentable).and_return(commentable)
    end

    context 'when notification frequency is immediate' do
      before do
        receiver.stub_chain(:notification_setting, :comment_freq).and_return('immediate')
      end

      it 'send push notification' do
        notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String), post_id: commentable.id)

        notification_service.send(:comment_push_notification, receiver, comment)
      end
    end

    context 'when notification frequency is not immediate' do
      before do
        receiver.stub_chain(:notification_setting, :comment_freq).and_return('weekly')
      end

      it 'did not send push notification' do
        notification_service.should_not_receive(:send_push_notification)

        notification_service.send(:comment_push_notification, receiver, comment)
      end
    end
  end

  describe '#message push notification' do
    describe 'when notification frequency is immediate' do
      before do
        receiver.stub_chain(:notification_setting, :message_freq).and_return('immediate')
      end

      context 'when message root exists' do
        let (:message) { stub_model Message, id: 1, root_id: 2, body: 'msg body', sender: sender, receiver: receiver }
        let (:message_mapping) { stub_model MessageMapping, id: 12 }

        before do
          Pusher.stub(:get).and_return( { :user_count => 0 } )
        end

        it 'should find message mapping by message root' do
          MessageMapping.should_receive(:find_by).with(message_id: message.root_id).and_return(message_mapping)

          notification_service.send(:message_push_notification, message)
        end
      end

      context 'when message root did not exists' do
        let (:message) { stub_model Message, id: 1, body: 'msg body', sender: sender, receiver: receiver }
        let (:message_mapping) { stub_model MessageMapping, id: 12 }

        before do
          Pusher.stub(:get).and_return( { :user_count => 0 } )
        end

        it 'should find message mapping by message root' do
          MessageMapping.should_receive(:find_by).with(message_id: message.id).and_return(message_mapping)

          notification_service.send(:message_push_notification, message)
        end
      end

      context 'when user count < 2' do
        let (:message) { stub_model Message, id: 1, body: 'msg body', sender: sender, receiver: receiver }
        let (:message_mapping) { stub_model MessageMapping, id: 12 }

        before do
          Pusher.stub(:get).and_return( { :user_count => 0 } )
          MessageMapping.stub(:find_by).and_return(message_mapping)
        end

        it 'should send push notification' do
          notification_service.should_receive(:send_push_notification).with(receiver, an_instance_of(String), conversation_id: message_mapping.id)

          notification_service.send(:message_push_notification, message)
        end
      end

      context 'when user count >= 2' do
        let (:message) { stub_model Message, id: 1, body: 'msg body', sender: sender, receiver: receiver }
        let (:message_mapping) { stub_model MessageMapping, id: 12 }

        before do
          Pusher.stub(:get).and_return( { :user_count => 2 } )
          MessageMapping.stub(:find_by).and_return(message_mapping)
        end

        it 'should send push notification' do
          notification_service.should_not_receive(:send_push_notification)

          notification_service.send(:message_push_notification, message)
        end
      end
    end

    describe 'when notification frequency is not immediate' do

    end
  end

  describe '#send push notification' do
    let(:token) { '123' }

    before do
      notification_service.unstub(:send_push_notification)
    end

    context 'ios notification' do
      let(:device) { stub_model Device, id: 1, name: 'device', token: token, uid: '213', device_type: Device::IOS }
      let(:user) { stub_model User, id: 1, email: 'lorem@ipsum.com', devices: [device] }

      it 'should sent notification' do
        Urbanairship.should_receive(:register_device).with(token)
        Urbanairship.should_receive(:push)
        notification_service.send(:send_push_notification, user, alert, {})
      end
    end

    context 'android notification' do
      let(:device) { stub_model Device, id: 1, name: 'device', token: token, uid: '213', device_type: Device::ANDROID }
      let(:user) { stub_model User, id: 1, email: 'lorem@ipsum.com', devices: [device] }

      it 'should sent notification' do
        Urbanairship.should_not_receive(:register_device)
        Urbanairship.should_receive(:push)
        notification_service.send(:send_push_notification, user, alert, {})
      end
    end
  end

end