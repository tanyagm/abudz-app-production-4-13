require 'spec_helper'

describe MessageService do  
  let(:sender)          { stub_model User, id: 1, email: 'a@b.com' }
  let(:receiver)        { stub_model User, id: 2, email: 'a1@b.com' }
  let(:message_service) { MessageService.new(sender) }
  let(:mappings)        { [message_mapping] }
  let(:message)         { stub_model Message, id: 1, sender_id: 1, receiver_id: 2, root_id: 20 }
  let(:messages)        { [message] }
  let(:body)            { 'hello world' }
  let(:socket_id)       { '32582.2418640' }
  
  let(:message_mapping) do
    stub_model MessageMapping, id: 1, 
      first_user_id: 1, second_user_id: 2, 
      message_id: 10, message: message
  end

  let(:create_or_update_mapping_params) do
    { 
      message_id: message.id,
      user_ids: [sender.id, receiver.id],
      last_message: body 
    }
  end

  before do
    User.stub(:find_by).with(id: sender.id).and_return(sender)
    User.stub(:find).with(sender.id).and_return(sender)
    User.stub(:find_by).with(id: receiver.id).and_return(receiver)
    User.stub(:find).with(receiver.id).and_return(receiver)
    User.any_instance.stub_chain(:notification_setting, :message_freq).and_return('immediate')
    NotificationMailer.stub_chain(:messages, :deliver).and_return(true)
    NotificationMailer.stub_chain(:messages, :deliver!).and_return(true)
  end

  describe '#list_of_conversations' do
    before do
      MessageMapping.stub(:list_of_conversation).and_return(mappings)
      mappings.stub(:preload)
    end

    it 'set the default pagination params' do
      mappings.should_receive(:paginate)
        .with({ per_page: AppConfig.per_page, max_id: nil, min_id: nil, cursor: MessageMapping.arel_table[:sort_order]})
        .and_return(mappings)

      message_service.list_of_conversation
    end

    it 'paginate with the passed in pagination params' do
      mappings.should_receive(:paginate)
        .with({ per_page: 10, max_id: nil, min_id: 12, cursor: MessageMapping.arel_table[:sort_order] })
        .and_return(mappings)
      message_service.list_of_conversation({ per_page: 10, min_id: 12 }) 
    end

    it 'preloads first_user and second_user associations' do
      mappings.stub(:paginate).with({ per_page: 10, max_id: nil, min_id: 12, cursor: MessageMapping.arel_table[:sort_order] })
        .and_return(mappings)
      mappings.should_receive(:preload).with(:first_user, :second_user)
      message_service.list_of_conversation({ per_page: 10, min_id: 12 }) 
    end
  end

  describe '#conversation' do
    before do
      Message.stub(:conversations).and_return(messages)
      message_service.stub(:update_read_status)
      messages.stub(:preload).and_return(messages)
    end

    it "returns nil when not passed message mapping object" do
      message_service.conversation(nil).should be_nil
    end

    it 'set the default pagination params' do
      messages.should_receive(:paginate)
        .with({ per_page: AppConfig.per_page, max_id: nil, min_id: nil })
        .and_return(messages)

      message_service.conversation(message_mapping)
    end

    it 'paginate with the passed in pagination params' do
      messages.should_receive(:paginate)
        .with({ per_page: 10, max_id: 1, min_id: 12 })
        .and_return(messages)

      message_service.conversation(message_mapping, { per_page: 10, max_id: 1, min_id: 12 }) 
    end

    it 'preload sender' do
      messages.stub(:paginate).and_return(messages)
      messages.should_receive(:preload)
        .with(:sender)
        .and_return(messages)

      message_service.conversation(message_mapping, { per_page: 10, max_id: 1, min_id: 12 }) 
    end

    it 'raise MessageError exception when called by a non message participants' do
      mapping = stub_model MessageMapping, first_user_id: 10, second_user_id: 10

      expect { message_service.conversation(mapping) }
        .to raise_error(Exceptions::MessageError, 'Cannot view this messages')

    end

    it 'call update_read_status method' do
      messages.stub(:paginate)
        .with({ per_page: AppConfig.per_page, max_id: nil, min_id: nil })
        .and_return(messages)

      message_service.should_receive(:update_read_status)

      message_service.conversation(message_mapping)
    end

    it 'call conversations scope' do
      Message.should_receive(:conversations)
        .with(message_mapping.message_id).and_return(messages)
      messages.stub(:paginate).and_return(messages)

      message_service.conversation(message_mapping)
    end
  end

  describe '#send_message' do
    it "returns nil when not passed receiver object" do
      message_service.send_message(nil, nil).should be_nil
    end

    context 'when message mapping is found' do
      before do
        message_service.should_receive(:find_mapping)
          .with(sender.id, receiver.id)
          .and_return(message_mapping)

        message_service.stub(:remove_excluded_conversations)
        Message.any_instance.stub(:notify).and_return(nil)
      end

      it 'call reply_to method with correct parameters' do
        message_service.should_receive(:reply_to)
          .with(message_mapping.message, body, receiver)

        message_service.send_message(receiver, body)
      end

      it 'call remove_excluded_conversations method' do
        message_service.should_receive(:remove_excluded_conversations)
        message_service.send_message(receiver, body)
      end
    end

    context 'when message mapping is not found' do
      before do
        message_service.should_receive(:find_mapping)
          .with(sender.id, receiver.id)
          .and_return(nil)
      end

      it 'call create_message method with correct parameters' do
        message_service.should_receive(:create_message)
          .with(receiver_id: receiver.id, body: body)

        message_service.send_message(receiver, body)
      end
    end
  end

  describe '#init_conversation' do
    it 'returns InvalidParams when receiver_id not passed' do
      expect{ message_service.send(:init_conversation, nil) }.to raise_error(Exceptions::InvalidParams)
    end

    it 'returns channel info' do
      message_service.send(:init_conversation, 7).should have_key(:channel_name)
      message_service.send(:init_conversation, 7).should have_key(:event_name)
    end
  end

  describe '#init_notification' do
    it 'returns channel info' do
      message_service.send(:init_notification).should have_key(:channel_name)
      message_service.send(:init_notification).should have_key(:event_name)
    end
  end

  describe '#reply_to' do
    it "returns nil when not passed message object" do
      message_service.send(:reply_to, nil, nil, nil).should be_nil
    end

    it 'raise MessageError exception when called by a non message participants' do
      message = stub_model Message, sender_id: 10, receiver_id: 20

      expect { message_service.send(:reply_to, message, body, nil) }
        .to raise_error(Exceptions::MessageError, 'Cannot reply to this message')
    end

    context 'when the message root_id is not nil' do
      it 'set the root_id based on message object root_id' do
        message = stub_model Message, sender_id: 2, receiver_id: 1, root_id: 100
        message_service.should_receive(:create_message)
          .with(receiver_id: receiver.id, body: body, root_id: message.root_id)

        message_service.send(:reply_to, message, body, receiver)
      end
    end

    context 'when the message root_id is nil' do
      it 'set the root_id based on message object id' do
        message = stub_model Message, sender_id: 2, receiver_id: 1, id: 10
        message_service.should_receive(:create_message)
          .with(receiver_id: receiver.id, body: body, root_id: message.id)

        message_service.send(:reply_to, message, body, receiver)
      end
    end

    context 'when passed receiver parameter' do
      it 'set the receiver_id based on receiver object id' do
        message_service.should_receive(:create_message)
          .with(receiver_id: receiver.id, body: body, root_id: message.root_id)

        message_service.send(:reply_to, message, body, receiver)
      end
    end

    context 'when not passed receiver parameter' do
      context 'given sender id is the same as message sender_id' do
        it 'set the receiver_id to message object receiver_id' do
          message_service.should_receive(:create_message)
            .with(receiver_id: message.receiver_id, 
                  body: body, root_id: message.root_id)

          message_service.send(:reply_to, message, body, nil)
        end
      end
      
      context 'given sender id is not the same as message sender_id' do
        it 'set the receiver_id to message object sender_id' do
          message = stub_model Message, sender_id: 2, receiver_id: 1, root_id: 100
          message_service.should_receive(:create_message)
            .with(receiver_id: message.sender_id, 
                  body: body, root_id: message.root_id)

          message_service.send(:reply_to, message, body, nil)
        end
      end

    end    
  end

  describe '#find_mapping' do
    it 'find message mapping with the smallest user id first' do
      MessageMapping.should_receive(:find_by)
        .with(first_user_id: 10, second_user_id: 100)
        .and_return(message_mapping)

      message_service.send(:find_mapping, 100, 10)
    end
  end

  describe '#create_message' do
    let(:message_params) { { body: body, receiver_id: receiver.id, sender_socket_id: socket_id } }

    before do
      sender.stub_chain(:sent_messages, :build).and_return(message)
      message.stub(:save!)
      message_service.stub(:create_or_update_mapping!)
      message_service.stub(:push_message)
      Message.any_instance.stub(:notify).and_return(nil)
    end

    it 'build message object with correct parameters' do
      sender.should_receive(:sent_messages).and_return(message)

      message.should_receive(:build)
        .with(message_params)
        .and_return(message)

      message_service.send(:create_message, message_params)
    end

    it 'call save! method' do
      message.should_receive(:save!)
      message_service.send(:create_message, message_params)
    end

    it 'call create_or_update_mapping! method with correct parameters' do
      message_service.should_receive(:create_or_update_mapping!)
        .with(create_or_update_mapping_params)

      message_service.send(:create_message, message_params)
    end

    it 'call push_message method with correct parameter' do
      message_service.should_receive(:push_message)
        .with(message)

      message_service.send(:create_message, message_params)
    end

    it 'returns message object' do
      message_service.send(:create_message, message_params).should be_eql message
    end
  end

  describe '#create_or_update_mapping!' do
    before do
      message_mapping.stub(:update_attributes!)
      MessageMapping.stub(:create!)      
    end

    it 'call find_mapping method with correct parameters' do
      message_service.should_receive(:find_mapping)
        .with(create_or_update_mapping_params[:user_ids].min, create_or_update_mapping_params[:user_ids].max)
        .and_return(message_mapping)

      message_service.send(:create_or_update_mapping!, create_or_update_mapping_params)
    end

    context 'when find_mapping method returns message mapping object' do
      before do
        message_service.stub(:find_mapping).and_return(message_mapping)
      end

      it 'update existing message mapping object' do
        message_mapping.should_receive(:update_attributes!)
          .with(last_message: body, first_user_opened: true, second_user_opened: false)

        message_service.send(:create_or_update_mapping!, create_or_update_mapping_params)
      end

      context 'given the sender id is the same as first_user_id' do
        it 'updates first_user_opened flag to true and second_user_opened flag to false' do
          message_mapping.should_receive(:update_attributes!)
            .with(last_message: body, first_user_opened: true, second_user_opened: false)

          message_service.send(:create_or_update_mapping!, create_or_update_mapping_params)
        end
      end

      context 'given the sender id is not the same as first_user_id' do
        it 'updates first_user_opened flag to false and second_user_opened flag to true' do
          message_mapping.first_user_id = 100

          message_mapping.should_receive(:update_attributes!)
            .with(last_message: body, first_user_opened: false, second_user_opened: true)

          message_service.send(:create_or_update_mapping!, create_or_update_mapping_params)
        end
      end
    end

    context 'when find_mapping method returns nil' do
      it 'create new message mapping object' do
        message_service.stub(:find_mapping).and_return(nil)

        MessageMapping.should_receive(:create!)
          .with(first_user_id: create_or_update_mapping_params[:user_ids].min, 
                second_user_id: create_or_update_mapping_params[:user_ids].max,
                message_id: create_or_update_mapping_params[:message_id], 
                last_message: create_or_update_mapping_params[:last_message],
                first_user_opened: sender.id == create_or_update_mapping_params[:user_ids].min,
                second_user_opened: sender.id == create_or_update_mapping_params[:user_ids].max)

        message_service.send(:create_or_update_mapping!, create_or_update_mapping_params)
      end
    end
  end

  describe '#push_message' do
    it 'enqueue push message job to resque' do
      Resque.should_receive(:enqueue).with(Jobs::PushMessage, message.id, nil)
      message_service.send(:push_message, message)
    end
  end

  describe '#update_read_status' do
    it "returns nil when not passed message mapping object" do
      message_service.send(:update_read_status, nil).should be_nil
    end

    context 'when sender id is the same as message mapping first_user_id' do
      it 'update first_user_opened to column true' do
        message_mapping.first_user_id = 1
        message_mapping.should_receive(:update_column).with(:first_user_opened, true)

        message_service.send(:update_read_status, message_mapping)
      end
    end

    context 'when sender id is the same as message mapping second_user_id' do
      it 'update second_user_opened column to true' do
        sender.id = 10
        message_mapping.second_user_id = 10
        message_mapping.should_receive(:update_column).with(:second_user_opened, true)

        message_service.send(:update_read_status, message_mapping)
      end
    end
  end

  describe '#delete_conversation' do
    context 'when not given message mapping object as a parameter' do
      it 'returns false' do
        message_service.delete_conversation(nil).should be_false
      end
    end

    context 'when sender is a message participants' do
      let(:deleted_conversation)  { stub_model DeletedMessageMapping, id: 1 }
      let(:deleted_conversations) { [deleted_conversation] }

      before do
        message_mapping.stub_chain(:participant_ids, :include?)
          .and_return(true)

        sender.should_receive(:deleted_conversations)
          .and_return(deleted_conversations)

        deleted_conversations.stub_chain(:first_or_create, :persisted?)
          .and_return(true)
      end

      it 'creates new deleted message mapping record' do
        deleted_conversations.should_receive(:where)
          .with(message_mapping_id: message_mapping.id)
          .and_return(deleted_conversations)
        message_service.delete_conversation(message_mapping)
      end
    end

    context 'when sender is not a message participants' do
      it 'raise MessageError exception' do
        mapping = stub_model MessageMapping, first_user_id: 10, second_user_id: 100
        mapping.stub_chain(:participant_ids, :include?)
          .and_return(false)

        expect { message_service.delete_conversation(mapping) }
          .to raise_error(Exceptions::MessageError, 'Cannot delete this messages')

      end
    end
  end

  describe '#remove_excluded_conversations' do
    it 'deletes all excluded conversations' do
      message_service.instance_variable_set(:@mapping, message_mapping)
      DeletedMessageMapping.should_receive(:delete_all)
        .with(message_mapping_id: message_mapping.id)

      message_service.send(:remove_excluded_conversations)
    end
  end

end
