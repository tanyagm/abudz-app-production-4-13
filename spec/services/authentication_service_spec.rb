require 'spec_helper'

describe AuthenticationService do
  let(:user) { stub_model User, id:1, email: 'a@b.com', password: 'correct_password'}
  let(:auth_service) {AuthenticationService.new(user.email)}

  before do
    User.stub(:find_by).and_return(user)
    Doorkeeper::Application.stub_chain(:first, :id).and_return(1)
  end

  describe 'authenticate' do
    before do
      user.stub(:authenticate_with_password).with('correct_password').and_return(true)
      user.stub(:authenticate_with_password).with('wrong_password').and_return(false)
    end

    context 'when succeed' do
      it 'return user object' do
        expect(auth_service.authenticate(user.password)).to eq(user)
      end
    end
    
    context 'when fail' do
      it 'return nil' do
        expect(auth_service.authenticate("wrong_password")).to be_nil
      end
    end
  end  
end
