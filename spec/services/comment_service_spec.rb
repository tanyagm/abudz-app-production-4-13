require 'spec_helper'

describe CommentService do
  let(:commentable){ stub_model Post, id: 5001 }
  let(:user){ stub_model User, id: 13921 }
  let(:service){ CommentService.new(commentable, user) }
  
  let(:comment){ comment1 }
  let(:comment1){ stub_model Comment, id: 113921 }
  let(:comment2){ stub_model Comment, id: 140012 }
  let(:comments){ [comment1, comment2] }

  before do
    User.stub(:find_by).with(id: user.id).and_return(user)
    User.stub(:find).with(user.id).and_return(user)
    User.any_instance.stub_chain(:notification_setting, :comment_freq).and_return('immediate')
    NotificationMailer.stub_chain(:comments, :deliver).and_return(true)
    NotificationMailer.stub_chain(:comments, :deliver!).and_return(true)
  end


  describe 'create' do
    let(:params) { stub_model Hash }
    let(:prepared_params) { stub_model Hash }
    let(:commenter) { stub_model User, id: 12345 }
    let(:notification_setting) { stub_model NotificationSetting, comment_freq: 1 }

    before do
      commentable.stub(:comments).and_return(comments)
      commentable.stub(:user).and_return(commenter)
      service.stub(:prepare_params).and_return(prepared_params)
      comments.stub(:create!).with(prepared_params).and_return(comment)
      commenter.stub(:notification_setting).and_return(notification_setting)
      Comment.any_instance.stub(:notify).and_return(nil)
    end

    it 'create a new comment' do
      comments.should_receive(:create!).with(prepared_params)
      service.create(params)
    end

    it 'return the created comment' do
      expect(service.create(params)).to eql comment
    end
  end

  describe 'delete_comment' do

    before do
      comment.stub(:destroy).and_return(true)
    end

    context 'commentable owned by other' do
      before do
        commentable.stub(:owned_by?).with(user).and_return(false)
        commentable.stub(:comments).and_return(comments)
        comments.stub(:find).with(42).and_return(comment)
      end

      describe 'comment owned by self' do
        before do
          comments.stub(:owned_by).with(user).and_return(comments)
        end

        it 'destroy the comment' do
          comment.should_receive(:destroy)
          service.delete_comment(42)
        end
      end

      describe 'comment owned by other' do
        before do
          comments.stub(:owned_by).with(user).and_raise(ActiveRecord::RecordNotFound)
        end

        it 'raise Exception' do
          expect { service.delete_comment(42) }.to raise_error(ActiveRecord::RecordNotFound)
        end
      end
    end

    context 'commentable owned by self' do
      before do
        commentable.stub(:owned_by?).with(user).and_return(true)          
        commentable.stub(:comments).and_return(comments)
        comments.stub(:find).with(42).and_return(comment)
      end

      describe 'comment owned by self' do
        it 'destroy the comment' do
          comment.should_receive(:destroy)
          service.delete_comment(42)
        end
      end

      describe 'comment owned by other' do
         it 'destroy the comment' do
          comment.should_receive(:destroy)
          service.delete_comment(42)
        end
      end

    end
  end
end
