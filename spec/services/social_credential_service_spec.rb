require 'spec_helper'

describe SocialCredentialService do
  let(:user) { stub_model User, id:2001 }
  let(:service) {SocialCredentialService.new(user)}
  let(:social_credentials) { [tw_credential, fb_credential] }
  let(:credential) { tw_credential }
  let(:tw_credential) { stub_model SocialCredential, social_type: SocialCredential::TWITTER }
  let(:fb_credential) { stub_model SocialCredential, social_type: SocialCredential::FACEBOOK }

  describe 'add' do
    before do
      user.stub_chain(:social_credentials, :where_type, :first_or_initialize).and_return(credential)
      credential.stub(:update!).and_return(true)
    end

    context 'twitter account' do
      let(:params){ ActionController::Parameters.new({
                      social_type: "twitter",
                      token: "access_token",
                      secret: "access_token_secret"})
                    }
      let(:uid){ 42 }
      let(:credential){ tw_credential }

      before do 
        Twitter::REST::Client.any_instance.stub_chain(:user, :id).and_return(uid)
        Bookkeeping::BookkeepingAttachService.stub(:enqueue_job_for_credential)
      end

      it 'enqueue bookkeeping attach job' do
        Bookkeeping::BookkeepingAttachService.should_receive(:enqueue_job_for_credential).with(user.id, tw_credential)
        service.add(params)
      end

      it 'return newly created credential' do
        expect(service.add(params)).to eql(tw_credential)
      end 
    end

    context 'facebook account' do
      let(:params){ ActionController::Parameters.new({
                      social_type: "facebook",
                      token: "access_token" })
                    }
      let(:uid) { 180892 }
      let(:credential){ fb_credential }
      
      before do 
        FacebookService.any_instance.stub(:get_facebook_uid).and_return(uid)
      end

      it 'return newly created credential' do
        expect(service.add(params)).to eql(fb_credential)
      end  
    end
  end


  describe 'remove' do
    before do
      user.stub_chain(:social_credentials, :where_type, :first!).and_return(credential)
      credential.stub(:destroy).and_return(true)
    end

    context 'twitter account' do
      let(:social_type){ "twitter" }
      let(:credential){ tw_credential }

      it 'destroy the related credential' do
        credential.should_receive(:destroy)
        service.remove(social_type)
      end 
    end

    context 'facebook account' do
      let(:social_type){ "facebook" }
      let(:credential){ fb_credential }

      it 'destroy the related credential' do
        credential.should_receive(:destroy)
        service.remove(social_type)
      end 
    end
  end
end
