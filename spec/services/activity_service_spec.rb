require 'spec_helper'

describe ActivityService do
  let(:first_user) { stub_model User, id: 1, email: "first_user@test.com", first_name: "A", last_name: "User" }
  let(:activity_service) { ActivityService.new(first_user) }
  let(:pagination_offset) { 0 }
  let(:pagination_size) { 10 }

  describe 'add_tracker' do

    it 'add tracker to related class' do
      Comment.should_receive(:tracker).with(:user, an_instance_of(Proc))
      FriendRelation.should_receive(:tracker).with(:user, an_instance_of(Proc))
      Post.should_receive(:tracker).with(:user, an_instance_of(Proc))
      User.should_receive(:tracker).with(:user, an_instance_of(Proc))

      ActivityService.send(:add_tracker)
    end
  end

  describe 'all_activities' do

    it 'call all related activity' do
      activity_service.should_receive(:friendship_activities).with(pagination_offset, pagination_size).and_return([])
      activity_service.should_receive(:comment_activities).with(pagination_offset, pagination_size).and_return([])
      activity_service.should_receive(:like_activities).with(pagination_offset, pagination_size).and_return([])
      activity_service.should_receive(:endorse_activities).with(pagination_offset, pagination_size).and_return([])

      activity_service.send(:all_activities, pagination_offset, pagination_size)
    end
  end

  describe 'friendship_activities' do

    it 'should fetch activities from FriendRelation' do
      FriendRelation.should_receive(:fetch_activities).with(:user, first_user.id, an_instance_of(Hash))

      activity_service.send(:friendship_activities, pagination_offset, pagination_size)
    end
  end

  describe 'comment_activities' do

    it 'should fetch activities from Comment' do
      Comment.should_receive(:fetch_activities).with(:user, first_user.id, an_instance_of(Hash))

      activity_service.send(:comment_activities, pagination_offset, pagination_size)
    end
  end

  describe 'like_activities' do

    it 'should fetch activities from Post' do
      Post.should_receive(:fetch_activities).with(:user, first_user.id, an_instance_of(Hash))

      activity_service.send(:like_activities, pagination_offset, pagination_size)
    end
  end

  describe 'endorse_activities' do

    it 'should fetch activities from User' do
      User.should_receive(:fetch_activities).with(:user, first_user.id, an_instance_of(Hash))

      activity_service.send(:endorse_activities, pagination_offset, pagination_size)
    end
  end
end