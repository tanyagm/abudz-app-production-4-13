require 'spec_helper'

describe TribalRankService do
  let(:user) { stub_model User, id:2001, email: 'a@b.com', password: 'correct_password'}
  let(:target_user) { stub_model User, id:3030, email: 'a@b.com', password: 'correct_password'}
  let(:service) {TribalRankService.new(user)}

  before do
    target_user.stub(:tribal_rank).and_return(1337)
  end

  describe 'endorse' do
    before do 
      target_user.stub(:upvote_by)
      target_user.stub(:create_activity)
      NotificationService.stub(:send_endorse_notification)
    end

    context 'when target user is not friend' do
      before do
        user.stub(:is_friend?).with(target_user).and_return(false)
      end

      it 'return exception' do
        expect { service.endorse(target_user) }.to raise_error(Exceptions::NotAllowed)
      end
    end

    context 'when target user is friend' do
      before do
        user.stub(:is_friend?).with(target_user).and_return(true)
      end

      it 'create activity record' do
        target_user.should_receive(:create_activity).with(user_id: user.id)
        service.endorse(target_user)
      end

      it 'call upvote_by' do
        target_user.should_receive(:upvote_by).with(user)
        service.endorse(target_user)
      end

      it 'send endorse notification' do
        NotificationService.should_receive(:send_endorse_notification).with(user, target_user)
        service.endorse(target_user)
      end

      it 'return target user tribal_rank' do
        expect(service.endorse(target_user)).to eql(1337)
      end
    end
  end

  describe 'unendorse' do
    before do 
      target_user.stub(:unvote_up)
    end

    context 'when target user is not friend' do
      before do
        user.stub(:is_friend?).with(target_user).and_return(false)
      end

      it 'return exception' do
        expect { service.unendorse(target_user) }.to raise_error(Exceptions::NotAllowed)
      end
    end

    context 'when target user is friend' do
      before do
        user.stub(:is_friend?).with(target_user).and_return(true)
      end

      it 'call unvote_up' do
        target_user.should_receive(:unvote_up).with(user)
        service.unendorse(target_user)
      end

      it 'return target user tribal_rank' do
        expect(service.unendorse(target_user)).to eql(1337)
      end

    end
  end
end
