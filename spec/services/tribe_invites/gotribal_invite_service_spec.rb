require 'spec_helper'

describe TribeInvites::GotribalInviteService do
  let (:params) {{email: target, tribe: tribe}}  
  let (:target_user) { stub_model User, id: 123, first_name: 'John', last_name: 'Last' }
  let (:tribe){ stub_model Tribe, name: "Tribe name" }
  let (:user){ stub_model User, id: 13921, first_name: 'first', last_name: 'last' }
  let (:service){ TribeInvites::GotribalInviteService.new(user) }
  let!(:tribe_service) { TribeService.new(nil, target_user) }

  before do
    User.stub(:find).with(target_user.id).and_return(target_user)
    tribe.stub(:owner).and_return(target_user)
    TribeService.stub(:new).with(nil, target_user).and_return(tribe_service)
    tribe_service.stub(:join).and_return(tribe)
    NotificationService.any_instance.stub(:send_push_notification)
    NotificationMailer.any_instance.stub_chain(:tribe_invitations, :deliver)
  end

  describe 'send_invite' do
    before do
      target_user.stub_chain(:notification_setting, :tribe_invitation_freq).and_return('immediate')
    end

    it 'add user to group' do
      tribe_service.should_receive(:join).and_return(tribe)

      service.send_invite(target_user.id, tribe)
    end

    it 'send push notification' do
      NotificationService.any_instance.should_receive(:tribe_invitation_push_notification)

      service.send_invite(target_user.id, tribe)
    end

    context 'immediate setting' do
      it 'send mail notification' do
        NotificationMailer.any_instance.should_receive(:tribe_invitations)

        service.send_invite(target_user.id, tribe)
      end
    end

    context 'non immediate setting' do
      before do
        target_user.stub_chain(:notification_setting, :tribe_invitation_freq).and_return('weekly')
      end

      it 'not sending mail notification' do
        NotificationMailer.any_instance.should_not_receive(:tribe_invitations)

        service.send_invite(target_user.id, tribe)
      end
    end
  end
end