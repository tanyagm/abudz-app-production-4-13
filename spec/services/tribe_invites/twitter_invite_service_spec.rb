require 'spec_helper'

describe TribeInvites::TwitterInviteService do
  let (:params) {{twitter_id: target, tribe: tribe}}  
  let (:target) {1337}
  let (:user){ stub_model User, id: 13921 }
  let (:tribe){ stub_model Tribe, name: "Tribe name" }
  let (:service){ TribeInvites::TwitterInviteService.new(user) }
  let (:credential){ stub_model SocialCredential, token: 'sotoken', secret: 'sosecret' }
    
  before do
    user.stub_chain(:social_credentials, :twitter, :first).and_return(credential)
    Bookkeeping::BookkeepingCreateService.any_instance.stub(:create)
  end

  describe 'send_invite' do
    context 'new_user' do
      before do
        Twitter::REST::Client.any_instance.stub(:create_direct_message)
      end

      it 'send_invite_to_new_user' do
        Twitter::REST::Client.any_instance.should_receive(:create_direct_message).with(target, an_instance_of(String))
        service.send_invite(params)
      end

      it 'create a bookkeeping record' do
        Bookkeeping::BookkeepingCreateService.any_instance.should_receive(:create).with(ProspectiveUser::SOURCE_TWITTER, target)
        service.send_invite(params)
      end
    end

    context 'existing_user' do
      let(:existing_user){ stub_model User, id: 23291, email: "existing.user1@mail.com" }
      let(:target_credential){ stub_model SocialCredential, uid: target, user: existing_user}

      before do
        SocialCredential.stub_chain(:where, :includes, :first).and_return(target_credential)
        TribeService.any_instance.stub(:join)
      end

      it 'call existing_user_action' do
        service.should_receive(:existing_user_action)
        service.send_invite(params)
      end

      it 'make existing_user to join specified tribe' do
        TribeService.any_instance.should_receive(:join).with(tribe)
        service.send_invite(params)
      end
    end
  end

end
