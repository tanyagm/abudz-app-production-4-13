require 'spec_helper'

describe TribeInvites::EmailInviteService do
  let (:params) {{email: target, tribe: tribe}}  
  let (:target) {"target@mail.com"}
  let (:tribe){ stub_model Tribe, name: "Tribe name" }
  let (:user){ stub_model User, id: 13921, first_name: 'first', last_name: 'last' }
  let (:service){ TribeInvites::EmailInviteService.new(user) }
  
  describe 'send_invite' do
    context 'new_user' do
      before do
        InviteMailer.stub_chain(:send_tribe_invite, :deliver!)
        Bookkeeping::BookkeepingCreateService.any_instance.stub(:create)
      end

      it 'send_invite_to_new_user' do
        InviteMailer.should_receive(:send_tribe_invite).with(user.fullname, target, an_instance_of(ActiveSupport::HashWithIndifferentAccess))
        service.send_invite(params)
      end

      it 'create a bookkeeping record' do
        Bookkeeping::BookkeepingCreateService.any_instance.should_receive(:create).with(ProspectiveUser::SOURCE_EMAIL, target)
        service.send_invite(params)
      end
    end

    context 'existing_user' do
      let(:existing_user){ stub_model User, id: 23291, email: "existing.user1@mail.com" }

      before do
        User.stub_chain(:where, :first).and_return(existing_user)
        TribeService.any_instance.stub(:join)
      end

      it 'call existing_user_action' do
        service.should_receive(:existing_user_action)
        service.send_invite(params)
      end

      it 'make existing_user to join specified tribe' do
        TribeService.any_instance.should_receive(:join).with(tribe)
        service.send_invite(params)
      end
    end
  end

end
