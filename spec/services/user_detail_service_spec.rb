require 'spec_helper'

describe UserDetailService do
  let(:user) { stub_model User, id: 1001 }
  let(:user_detail_service) { UserDetailService.new(user) }

  describe 'set_mass' do
    # Refer to question_set_test.yml

    let(:params) {
      {"single_answer" => {"private" =>  1},
      "multiple_answer" => {"value" =>  1},
      "date" => {"value" =>  1, "private" =>  0},
      "string" => {"value" => 1, "private" =>  0}}
    }
    before do
      user_detail_service.stub(:set).and_return(true)
    end
    
    it 'set all specified details' do
      params.each { |fieldname, fieldparams| user_detail_service.should_receive(:set).with(fieldname, fieldparams)}
      user_detail_service.set_mass(params)
    end

    it 'do not set non-existant details' do
      params["non_existant_fieldname"] = 1
      user_detail_service.should_not_receive(:set).with("non_existant_fieldname", anything)
      user_detail_service.set_mass(params)
    end
  end

  describe 'sanity check' do
    context 'single_answer' do
      it 'set and get user detail correctly' do
        user_detail_service.set('single_answer', value: 1)
        expect(user_detail_service.get('single_answer')).to eq(1)
      end
    end
    context 'multiple_answer' do
      it 'set and get user detail correctly' do
        user_detail_service.set('multiple_answer', value: [1, 2, 3])
        expect(user_detail_service.get('multiple_answer')).to eq([1, 2, 3])
      end
    end
    context 'date' do
      it 'set and get user detail correctly' do
        user_detail_service.set('date', value: '1991/09/12'.to_time)
        expect(user_detail_service.get('date')).to eq('1991/09/12'.to_time)
      end
    end
    context 'string' do
      it 'set and get user detail correctly' do
        user_detail_service.set('string', value: "aaaa")
        expect(user_detail_service.get('string')).to eq("aaaa")
      end
    end
    context 'multiple_string' do
      it 'set and get user detail correctly' do
        user_detail_service.set('multiple_string', value: ["aa", "bbb", "cc"])
        expect(user_detail_service.get('multiple_string')).to eq(["aa", "bbb", "cc"])
      end
    end
  end

  describe 'set' do
    context 'when succeed' do
      context 'new user detail' do
        it 'create a new entry' do
          expect{user_detail_service.set('date', value: '1991/08/17')}.to change{user.details.count}.by(1)
        end
      end 
      
      context 'existing user detail' do
        it 'update existing entry' do
          expect{
            user_detail_service.set('date', value: '1991/08/17')
            user_detail_service.set('date', value: '1991/08/12')
          }.to change{user.details.count}.by(1)
        end
      end 
    end

    context 'when failed' do
      context 'invalid parameter' do
        it 'raise InvalidParams error' do
          expect{user_detail_service.set('string', '123')}.to raise_error(Exceptions::InvalidParams)
        end
      end
    end
  end

  describe 'get' do
    let(:user_detail) { stub_model UserDetail }

    before do
      user_detail_service.stub(:fetch_user_detail).and_return(user_detail)
      user_detail.stub(:value).and_return('answer')
    end

    context 'existing user detail' do
      it 'return the value' do
        expect(user_detail_service.get('single_answer')).to eql('answer')
      end
    end

    context 'non-existant user detail' do
      it 'return nil' do
        expect(user_detail_service.get('non-existant_field')).to be_nil
      end
    end
  end

  describe 'get_private' do


    let(:user) { stub_model User, id: 1001 }
    let(:user_detail) { stub_model UserDetail }
    let(:user_detail_service) { UserDetailService.new(user) }

    before do
      user_detail_service.stub(:fetch_user_detail).and_return(user_detail)
      user_detail.stub(:value).and_return('answer')
    end

    context 'existing user detail' do
      context 'private user detail' do
        let(:user_detail) { stub_model UserDetail, private: 1 }

        it 'return nil' do
          expect(user_detail_service.get_private('single_answer')).to be_nil
        end
      end

      context 'public user detail' do
        it 'return the value' do
          expect(user_detail_service.get_private('single_answer')).to eql('answer')
        end
      end
    end

    context 'non-existant user detail' do
      it 'return nil' do
        expect(user_detail_service.get_private('non-existant_field')).to be_nil
      end
    end
  end
end
