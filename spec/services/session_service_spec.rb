require 'spec_helper'

describe SessionService do
  let(:user) { stub_model User, id: 1 }
  let(:service) { SessionService.new user }
  let(:access_token) { stub_model Doorkeeper::AccessToken }

  describe '#sign_in' do
    context 'When user authenticated' do
      before do
        AuthenticationService.stub_chain(:new, :authenticate).and_return(user)
        service.stub(:issue_token).and_return(access_token)
      end

      it 'return user and token object' do
        service.sign_in('username', 'password').should be_eql([user, access_token])
      end
    end

    context 'When user not authenticated' do
      before do
        AuthenticationService.stub_chain(:new, :authenticate).and_return(nil)
      end

      it 'raise AuthenticationFailed exception' do
        expect { service.sign_in('username', 'password') }
          .to raise_error(Exceptions::AuthenticationFailed)
      end
    end
  end

  describe '#sign_out' do    
    let(:devices) { [stub_model(Device, id: 1)] }
    let(:tokens) { [access_token] }

    before do
      Device.stub_chain(:where, :delete_all).and_return(10)
      Doorkeeper::AccessToken.stub_chain(:where, :delete_all).and_return(10)
    end

    context 'Service instantiated without user object' do
      it 'returns nil' do
        SessionService.new.sign_out.should be_nil
      end
    end

    it 'Deletes all user Doorkeeper access tokens' do
      Device.should_receive(:where).with(user_id: user.id)
        .and_return(devices)
      devices.should_receive(:delete_all).and_return(10)

      service.sign_out
    end

    it 'Deletes all user registered devices' do
      Doorkeeper::AccessToken.should_receive(:where)
        .with(resource_owner_id: user.id)
        .and_return(tokens)
      tokens.should_receive(:delete_all).and_return(10)

      service.sign_out
    end
  end

  describe '#issue_token' do
    before do
      Doorkeeper::AccessToken.stub_chain(:where, :first_or_create).and_return(access_token)
      access_token.stub(:expired?).and_return(false)
    end

    context 'Service instantiated without user object' do
      it 'returns nil' do
        SessionService.new.issue_token.should be_nil
      end
    end

    context 'Service instantiated with user object' do
      context 'Token is expired' do
        before do
          access_token.stub(:expired?).and_return(true)
        end

        it 'updates the token' do
          access_token.should_receive(:update)
            .with(any_args)
            .and_return(true)

          service.issue_token
        end
      end      

      it 'find or create the token' do
        Doorkeeper::AccessToken.should_receive(:where)
          .with(resource_owner_id: user.id).and_return(access_token)
        access_token.should_receive(:first_or_create).and_return(access_token)

        service.issue_token
      end

      it 'return access_token' do
        expect(service.issue_token).to eq(access_token)
      end
    end        
  end

end