require 'spec_helper'

describe NotifSettingService do

  describe 'save' do
    let(:params) {{
      tribe_post_type: [:phone, :email], tribe_post_freq: 'daily', 
      comment_type: [:email], comment_freq: 'immediate'
    }}

    let(:notif) { stub_model NotificationSetting, 
      id: 1, tribe_post_type: [:phone, :email], tribe_post_freq: 'daily', 
      comment_type: [:email], comment_freq: 'immediate', user_id: 1808 }
    
    let(:user) { stub_model User, id: 1808 }
    let(:notif_service) { NotifSettingService.new(user) }

    before do
      NotificationSetting.stub_chain(:where, :first_or_create!).and_return(notif)
      notif_service.stub(:prepare_params).and_return(params)
      notif.stub(:update!).and_return(true)
    end

    it 'save the notif' do
      notif.should_receive(:update!).with(params)
      notif_service.save(params)
    end

  end
end
