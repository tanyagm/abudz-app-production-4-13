require 'spec_helper'

describe ScheduledNotificationService do
  let(:schedule_notification_service) { ScheduledNotificationService.new }
  let(:user1) { stub_model User, id: 1, email: 'test@test.com', first_name: 'Henriette', last_name: 'Thiel' }
  let(:user2) { stub_model User, id: 2 }
  let(:notification_setting) { stub_model NotificationSetting, id: 1, user_id: 1, comment_freq: 1 }

  describe 'schedule all notification' do

    before do
      User.stub_chain(:has_notifications, :find_each).and_yield(user1)
      user1.stub(:notification_setting).and_return(notification_setting)
      SearchService.any_instance.stub(:search_suggested).and_return(nil)
      SearchService.any_instance.stub(:compare_suggested_results).and_return([])
      schedule_notification_service.should_receive(:yield_by_date)
        .with(an_instance_of(Symbol))
        .at_least(:once)
        .and_yield
    end

    describe 'schedule comment notification' do

      context 'when comment settings is phone only' do

        before do
          notification_setting.stub(:comment_type).and_return([:phone])
        end

        it 'should send comment notification and not send email' do
          NotificationService.any_instance.should_receive(:send_bulk_comment_notification).with(user1)
          schedule_notification_service.should_not_receive(:send_comments_email)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when comment settings is email only' do

        before do
          notification_setting.stub(:comment_type).and_return([:email])
        end

        it 'should not send comment notification and send email' do
          NotificationService.any_instance.should_not_receive(:send_bulk_comment_notification)
          schedule_notification_service.should_receive(:send_comments_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when comment settings is phone and email' do

        before do
          notification_setting.stub(:comment_type).and_return([:phone, :email])
        end

        it 'should send comment notification and email' do
          NotificationService.any_instance.should_receive(:send_bulk_comment_notification).with(user1)
          schedule_notification_service.should_receive(:send_comments_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end
    end

    describe 'schedule activebudz request notification' do

      context 'when activebudz request settings is phone only' do

        before do
          notification_setting.stub(:activebudz_request_type).and_return([:phone])
        end

        it 'should send activebudz request notification and not send email' do
          NotificationService.any_instance.should_receive(:send_bulk_friend_notification).with(user1)
          schedule_notification_service.should_not_receive(:send_friend_requests_email)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when activebudz request settings is email only' do

        before do
          notification_setting.stub(:activebudz_request_type).and_return([:email])
        end

        it 'should not send activebudz request notification and send email' do
          NotificationService.any_instance.should_not_receive(:send_bulk_friend_notification)
          schedule_notification_service.should_receive(:send_friend_requests_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when activebudz request settings is phone and email' do

        before do
          notification_setting.stub(:activebudz_request_type).and_return([:phone, :email])
        end

        it 'should send activebudz request notification and email' do
          NotificationService.any_instance.should_receive(:send_bulk_friend_notification).with(user1)
          schedule_notification_service.should_receive(:send_friend_requests_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end
    end

    # describe 'schedule activebudz suggestion notification' do

    #   context 'when activebudz suggestion settings is phone only' do

    #     before do
    #       notification_setting.stub(:activebudz_suggested_type).and_return([:phone])
    #     end

    #     it 'should send activebudz suggestion notification and not send email' do
    #       NotificationService.any_instance.should_receive(:send_bulk_suggested_friend_notification).with(user1, an_instance_of(SearchService))
    #       schedule_notification_service.should_not_receive(:send_suggested_friend_email)

    #       schedule_notification_service.send(:schedule_all_notification)
    #     end
    #   end

    #   context 'when activebudz suggestion settings is email only' do

    #     before do
    #       notification_setting.stub(:activebudz_suggested_type).and_return([:email])
    #     end

    #     it 'should not send activebudz suggestion notification and send email' do
    #       NotificationService.any_instance.should_not_receive(:send_bulk_suggested_friend_notification)
    #       schedule_notification_service.should_receive(:send_suggested_friend_email).with(user1, an_instance_of(SearchService))

    #       schedule_notification_service.send(:schedule_all_notification)
    #     end
    #   end

    #   context 'when activebudz suggestion settings is phone and email' do

    #     before do
    #       notification_setting.stub(:activebudz_suggested_type).and_return([:phone, :email])
    #     end

    #     it 'should send activebudz suggestion notification and email' do
    #       NotificationService.any_instance.should_receive(:send_bulk_suggested_friend_notification).with(user1, an_instance_of(SearchService))
    #       schedule_notification_service.should_receive(:send_suggested_friend_email).with(user1, an_instance_of(SearchService))

    #       schedule_notification_service.send(:schedule_all_notification)
    #     end
    #   end
    # end

    describe 'schedule message notification' do

      context 'when message settings is phone only' do

        before do
          notification_setting.stub(:message_type).and_return([:phone])
        end

        it 'should send message notification and not send email' do
          NotificationService.any_instance.should_receive(:send_bulk_message_notification).with(user1)
          schedule_notification_service.should_not_receive(:send_messages_email)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when message settings is email only' do

        before do
          notification_setting.stub(:message_type).and_return([:email])
        end

        it 'should not send message notification and send email' do
          NotificationService.any_instance.should_not_receive(:send_bulk_message_notification)
          schedule_notification_service.should_receive(:send_messages_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when message settings is phone and email' do

        before do
          notification_setting.stub(:message_type).and_return([:phone, :email])
        end

        it 'should send message notification and email' do
          NotificationService.any_instance.should_receive(:send_bulk_message_notification).with(user1)
          schedule_notification_service.should_receive(:send_messages_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end
    end

    describe 'schedule tribe post notification' do

      context 'when tribe post settings is phone only' do

        before do
          notification_setting.stub(:tribe_post_type).and_return([:phone])
        end

        it 'should send tribe post notification and not send email' do
          NotificationService.any_instance.should_receive(:send_bulk_tribe_post_notification).with(user1)
          schedule_notification_service.should_not_receive(:send_tribe_posts_email)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when tribe post settings is email only' do

        before do
          notification_setting.stub(:tribe_post_type).and_return([:email])
        end

        it 'should not send tribe post notification and send email' do
          NotificationService.any_instance.should_not_receive(:send_bulk_tribe_post_notification)
          schedule_notification_service.should_receive(:send_tribe_posts_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when tribe post settings is phone and email' do

        before do
          notification_setting.stub(:tribe_post_type).and_return([:phone, :email])
        end

        it 'should send tribe post notification and email' do
          NotificationService.any_instance.should_receive(:send_bulk_tribe_post_notification).with(user1)
          schedule_notification_service.should_receive(:send_tribe_posts_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end
    end

    describe 'schedule tribe invitation notification' do

      context 'when tribe invitation settings is phone only' do

        before do
          notification_setting.stub(:tribe_invitation_type).and_return([:phone])
        end

        it 'should send tribe invitation notification and not send email' do
          NotificationService.any_instance.should_receive(:send_bulk_tribe_invitation_notification).with(user1)
          schedule_notification_service.should_not_receive(:send_tribe_invitations_email)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when tribe invitation settings is email only' do

        before do
          notification_setting.stub(:tribe_invitation_type).and_return([:email])
        end

        it 'should not send tribe invitation notification and send email' do
          NotificationService.any_instance.should_not_receive(:send_bulk_tribe_invitation_notification)
          schedule_notification_service.should_receive(:send_tribe_invitations_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end

      context 'when tribe invitation settings is phone and email' do

        before do
          notification_setting.stub(:tribe_invitation_type).and_return([:phone, :email])
        end

        it 'should send tribe invitation notification and email' do
          NotificationService.any_instance.should_receive(:send_bulk_tribe_invitation_notification).with(user1)
          schedule_notification_service.should_receive(:send_tribe_invitations_email).with(user1)

          schedule_notification_service.send(:schedule_all_notification)
        end
      end
    end
  end

  describe 'send friend requests email' do
    before do
      NotificationMailer.stub_chain(:friend_requests,:deliver!).and_return(nil)
    end

    context 'when notification count == 0' do
      let(:user_with_no_notifications) { stub_model User, id: 123, friend_notifications_count: 0 }

      it 'should not sent email' do
        NotificationMailer.should_not_receive(:friend_requests)

        schedule_notification_service.send(:send_friend_requests_email, user_with_no_notifications)
      end
    end

    context 'when notification count > 0' do
      let(:user_with_notifications) { stub_model User, id: 124, friend_notifications_count: 2 }

      it 'should sent email' do
        NotificationMailer.should_receive(:friend_requests).with(user_with_notifications.id)

        schedule_notification_service.send(:send_friend_requests_email, user_with_notifications)
      end
    end

  end

  describe 'send comments email' do
    before do
      NotificationMailer.stub_chain(:comments,:deliver!).and_return(nil)
    end

    context 'when notification count == 0' do
      let(:user_with_no_notifications) { stub_model User, id: 123, comment_notifications_count: 0 }

      it 'should not sent email' do
        NotificationMailer.should_not_receive(:comments)

        schedule_notification_service.send(:send_comments_email, user_with_no_notifications)
      end
    end

    context 'when notification count > 0' do
      let(:user_with_notifications) { stub_model User, id: 124, comment_notifications_count: 2 }

      it 'should sent email' do
        NotificationMailer.should_receive(:comments).with(user_with_notifications.id)

        schedule_notification_service.send(:send_comments_email, user_with_notifications)
      end
    end

  end

  describe 'send messages email' do
    before do
      NotificationMailer.stub_chain(:messages,:deliver!).and_return(nil)
    end

    context 'when notification count == 0' do
      let(:user_with_no_notifications) { stub_model User, id: 123, message_notifications_count: 0 }

      it 'should not sent email' do
        NotificationMailer.should_not_receive(:messages)

        schedule_notification_service.send(:send_messages_email, user_with_no_notifications)
      end
    end

    context 'when notification count > 0' do
      let(:user_with_notifications) { stub_model User, id: 124, message_notifications_count: 2 }

      it 'should sent email' do
        NotificationMailer.should_receive(:messages).with(user_with_notifications.id)

        schedule_notification_service.send(:send_messages_email, user_with_notifications)
      end
    end

  end

  describe 'send tribe posts email' do
    before do
      NotificationMailer.stub_chain(:tribe_posts,:deliver!).and_return(nil)
    end

    context 'when notification count == 0' do
      let(:user_with_no_notifications) { stub_model User, id: 123, tribe_post_notifications_count: 0 }

      it 'should not sent email' do
        NotificationMailer.should_not_receive(:tribe_posts)

        schedule_notification_service.send(:send_tribe_posts_email, user_with_no_notifications)
      end
    end

    context 'when notification count > 0' do
      let(:user_with_notifications) { stub_model User, id: 124, tribe_post_notifications_count: 2 }

      it 'should sent email' do
        NotificationMailer.should_receive(:tribe_posts).with(user_with_notifications.id)

        schedule_notification_service.send(:send_tribe_posts_email, user_with_notifications)
      end
    end

  end

  describe 'send tribe invitations email' do
    before do
      NotificationMailer.stub_chain(:tribe_invitations,:deliver!).and_return(nil)
    end

    context 'when notification count == 0' do
      let(:user_with_no_notifications) { stub_model User, id: 123, tribe_invitation_notifications_count: 0 }

      it 'should not sent email' do
        NotificationMailer.should_not_receive(:tribe_invitations)

        schedule_notification_service.send(:send_tribe_invitations_email, user_with_no_notifications)
      end
    end

    context 'when notification count > 0' do
      let(:user_with_notifications) { stub_model User, id: 124, tribe_invitation_notifications_count: 2 }

      it 'should sent email' do
        NotificationMailer.should_receive(:tribe_invitations).with(user_with_notifications.id)

        schedule_notification_service.send(:send_tribe_invitations_email, user_with_notifications)
      end
    end

  end

  describe 'yield by date' do
    describe 'call block depend on frequency' do
      it 'should yield daily frequency' do
        Date.stub(:today) { Date.new(2013, 1, 1) }

        expect { |b| schedule_notification_service.yield_by_date(:daily, &b) }.to yield_with_no_args
      end

      it 'should yield weekly frequency in the first day of week' do
        Date.stub(:today) { Date.new(2013, 1, 8) }

        expect { |b| schedule_notification_service.yield_by_date(:weekly, &b) }.to yield_with_no_args
      end

      it 'should not yield weekly frequency in the day other than the first day of week' do
        Date.stub(:today) { Date.new(2013, 1, 9) }

        expect { |b| schedule_notification_service.yield_by_date(:weekly, &b) }.not_to yield_with_no_args
      end

      it 'should yield monthly frequency in the first day of month' do
        Date.stub(:today) { Date.new(2013, 1, 1) }

        expect { |b| schedule_notification_service.yield_by_date(:monthly, &b) }.to yield_with_no_args
      end

      it 'should not yield monthly frequency in the day other than the first day of month' do
        Date.stub(:today) { Date.new(2013, 1, 9) }

        expect { |b| schedule_notification_service.yield_by_date(:monthly, &b) }.not_to yield_with_no_args
      end
    end
  end
end