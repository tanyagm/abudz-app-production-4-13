# == Schema Information
#
# Table name: users
#
#  id                           :integer          not null, primary key
#  email                        :string(255)
#  password_hash                :string(255)
#  password_salt                :string(255)
#  created_at                   :datetime
#  updated_at                   :datetime
#  first_name                   :string(255)
#  last_name                    :string(255)
#  avatar                       :string(255)
#  cached_votes_total           :integer          default(0)
#  password_recovery_token      :string(255)
#  password_recovery_expired_at :datetime
#

require 'spec_helper'

describe User do
  it_behaves_like 'notifiable'
  
  describe 'validation' do
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
    context 'password validation' do
      it { should validate_confirmation_of(:password) }
      it { should ensure_length_of(:password).is_at_least(8) }
    end
    context 'email validation' do
      it { should_not allow_value("blah").for(:email) }
      it { should allow_value("a@b.com").for(:email) }
    end
  end

  describe "association" do
    it { should have_many(:details) }
    it { should have_many(:deleted_conversations) }
    it { should have_many(:posts) }
    it { should have_and_belong_to_many(:tribes) }
    it { should have_many(:social_credentials) }
    it { should have_many(:invited_users) }
    it { should have_one(:notification_setting) }

    context 'friendable' do
      it { should have_many(:friends) }
      it { should have_many(:pending_request_users)}
      it { should have_many(:received_request_users)}
      it { should have_many(:friend_relations_to)}
      it { should have_many(:friend_relations)}
    end
  end

  describe 'authenticate_with_password' do
    let (:correct_passw) { 'aaa' }
    let (:wrong_passw) { 'bbb' }
    let (:user) { stub_model User, :password_hash  => 'aaa123' }

    before do
      user.stub(:encrypt_password).with(correct_passw).and_return(correct_passw+'123')
      user.stub(:encrypt_password).with(wrong_passw).and_return(wrong_passw+'123')
    end

    it 'with correct password' do
      expect(user.authenticate_with_password(correct_passw)).to be_true
    end      

    it 'with false password' do
      expect(user.authenticate_with_password(wrong_passw)).to be_false
    end      
  end


  describe 'tribal_rank' do
    context 'cached_votes_total = 0' do
      let (:user) { stub_model User, cached_votes_total: 0 }    
      it 'should equal 0' do
        expect(user.tribal_rank).to eql(0)
      end
    end

    context 'cached_votes_total = 1..3' do
      let (:user) { stub_model User, cached_votes_total: 2 }    
      it 'should equal cached_votes_total' do
        expect(user.tribal_rank).to eql(2)
      end
    end

    context 'cached_votes_total > 2' do
      let (:user) { stub_model User, cached_votes_total: 4 }    
      it 'should equal 3' do
        expect(user.tribal_rank).to eql(3)
      end
    end
  end
end
