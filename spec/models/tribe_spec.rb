# == Schema Information
#
# Table name: tribes
#
#  id            :integer          not null, primary key
#  name          :string(50)       not null
#  description   :string(255)
#  location_name :string(50)
#  activity      :string(50)       not null
#  lat           :float
#  lon           :float
#  image         :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  owner_id      :integer
#

require 'spec_helper'

describe Tribe do
  it_behaves_like 'notifiable'

  describe 'Validations' do
    it { should validate_presence_of(:activity) }
    it { should validate_presence_of(:name) }

    # it { should validate_uniqueness_of(:name) }
    
    it { should ensure_length_of(:activity).is_at_most(50) }
    it { should ensure_length_of(:name).is_at_most(50) }
    it { should ensure_length_of(:location_name).is_at_most(50) }
    it { should ensure_length_of(:description).is_at_most(255) }
    
  end

  describe 'Associations' do
    it { should belong_to(:owner) }
    it { should have_many(:posts) }
    it { should have_many(:users) }
    it { should have_many(:tribes_users) }
    it { should have_many(:invited_users) }
  end

  describe 'is_tribe_member?' do
    let(:tribe) {stub_model Tribe, id: 4301}
    let(:tribe_users) { [42, 1024, 65536] }
    before do
      tribe.stub_chain(:tribes_users, :pluck).and_return(tribe_users)
    end

    context 'user is a tribe member' do
      let(:user) {stub_model User, id: 42}
      it 'return true' do
        expect(tribe.is_tribe_member?(user)).to eql(true)
      end
    end

    context 'user is not a tribe member' do
      let(:user) {stub_model User, id: 10}
      it 'return false' do
        expect(tribe.is_tribe_member?(user)).to eql(false)
      end
    end
  end

end
