# == Schema Information
#
# Table name: friend_relations
#
#  id           :integer          not null, primary key
#  from_user_id :integer
#  to_user_id   :integer
#  status       :integer          default(0)
#  created_at   :datetime
#  updated_at   :datetime
#

require 'spec_helper'

describe FriendRelation do
  describe 'associations' do
    it { should belong_to(:from_user) }
    it { should belong_to(:to_user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:from_user_id) }
    it { should validate_presence_of(:to_user_id) }
    it { should validate_uniqueness_of(:from_user_id).scoped_to(:to_user_id)}
  end

  describe 'initial_status' do
    let(:relation) {stub_model FriendRelation}
    it 'should be pending' do
      expect(relation.status).to eql(FriendRelation::PENDING)
    end
  end

  describe 'update_status' do
    let(:relation) {stub_model FriendRelation}

    it 'change the status' do
      relation.should_receive(:update!).with({status: FriendRelation::APPROVED})
      relation.send(:update_status, FriendRelation::APPROVED)
    end
  end

  describe 'accept' do
    let(:relation) {stub_model FriendRelation, status: FriendRelation::PENDING}
    
    before do
      FriendRelation.any_instance.stub(:update_status).and_return(true)
    end

    it 'change the status to APPROVED' do
      relation.should_receive(:update_status).with(FriendRelation::APPROVED)
      relation.accept 
    end

    it 'create inverse entry' do
      relation.should_receive(:create_inverse)
      relation.accept 
    end
  end

  describe 'reject' do
    context 'APPROVED relations' do
      let(:relation) {stub_model FriendRelation, status: FriendRelation::APPROVED}
      it 'do not change the status' do
        relation.should_not_receive(:update_status)
        relation.reject
      end
    end
    
    context 'PENDING relations' do
      let(:relation) {stub_model FriendRelation, status: FriendRelation::PENDING}
      it 'change the status to REJECTED' do
        relation.should_receive(:update_status).with(FriendRelation::REJECTED)
        relation.reject
      end
    end
  end

  describe 'destroy' do
    let(:relation) {stub_model FriendRelation, status: FriendRelation::PENDING}
    let(:relation_inverse) {stub_model FriendRelation, status: FriendRelation::PENDING}
    before do
      relation.stub(:inverse_relation).and_return(relation_inverse)
      relation_inverse.stub(:first).and_return(relation_inverse)
    end

    it 'remove inverse relation' do
      relation_inverse.should_receive(:destroy_inverse)
      relation.destroy
    end
  end

  describe 'Trackable' do
    let(:relation) { stub_model FriendRelation }  

    describe 'after_save' do
      it 'should run trackable callback' do
        relation.should_receive(:record)
        relation.run_callbacks(:save)
      end
    end

    describe 'initialize' do
      it 'has time to live defined' do
        Comment.tracked_ttl.should_not be_nil
      end
    end
  end

  describe '#add_self_as_friend' do
    let (:friend_relation) { stub_model FriendRelation, from_user_id: 1, to_user_id: 1 }

    it 'Add errors to to_user_id' do
      friend_relation.send(:add_self_as_friend)
      friend_relation.should have(1).errors_on(:to_user_id)
    end
  end

end
