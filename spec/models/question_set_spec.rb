require 'spec_helper'

describe QuestionSet do
  let(:question_set) { QuestionSet.instance }

  describe 'exist?' do
    context 'fieldname exist' do
      it 'return true' do
        expect(question_set.exist? 'multiple_answer' ).to be_true
      end
    end

    context 'key exist' do
      it 'return true' do
        expect(question_set.exist? 10000002 ).to be_true
      end
    end
    context 'fieldname does not exist' do
      it 'return false' do
        expect(question_set.exist? 'wrong key' ).to be_false
      end     
    end

    context 'key does not exist' do
      it 'return false' do
        expect(question_set.exist?  1231 ).to be_false
      end
    end
  end

  describe 'get_question_type' do
    context 'exist' do
      it 'return specified parameter' do
        expect(question_set.get_question_type 'multiple_answer' ).to eq(2)
      end
    end
    context 'not exist' do
      it 'return nil' do
        expect(question_set.get_question_type 'birthdate' ).to be_nil
      end
    end
  end

  describe 'get_fieldname' do
    context 'exist' do
      it 'return specified parameter' do
        expect(question_set.get_fieldname 10000002 ).to eq('multiple_answer')
      end
    end
    context 'not exist' do
      it 'return nil' do
        expect(question_set.get_fieldname 1231 ).to be_nil
      end
    end
  end

  describe 'get_key' do
    context 'exist' do
      it 'return specified parameter' do
        expect(question_set.get_key 'multiple_answer' ).to eq(10000002)
      end
    end
    context 'not exist' do
      it 'return nil' do
        expect(question_set.get_key 'dum dum' ).to be_nil
      end
    end
  end
end
