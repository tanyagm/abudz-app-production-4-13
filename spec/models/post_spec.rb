# == Schema Information
#
# Table name: posts
#
#  id                 :integer          not null, primary key
#  user_id            :integer          not null
#  message            :string(255)      not null
#  image              :string(255)
#  location_name      :string(255)
#  lat                :float
#  lon                :float
#  created_at         :datetime
#  updated_at         :datetime
#  cached_votes_total :integer          default(0)
#  cached_votes_up    :integer          default(0)
#  tribe_id           :integer
#  comments_count     :integer          default(0)
#

require 'spec_helper'

describe Post do
  it_behaves_like 'notifiable'

  describe 'Associations' do
    it { should belong_to(:user) }
    it { should belong_to(:tribe) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:message) }
  end

  describe 'id_order scope' do
    let(:user2) { stub_model User, id: 2, first_name: "Abdul", last_name: "Haris Ilmawan" }
    let!(:post_a) { Post.create(id: 1, message: 'Message a', user: user2) }
    let!(:post_b) { Post.create(id: 2, message: 'Message b', user: user2) }
    
    it 'orders descendingly by id' do
      Post.all.id_order.should eq [post_b, post_a]
    end
  end

end
