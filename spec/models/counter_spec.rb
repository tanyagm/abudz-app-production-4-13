require 'spec_helper'

describe Counter do
  describe 'Validations' do
    it { should validate_presence_of(:counter_type) }
    it { should validate_presence_of(:user_id) }
  end

  describe 'Associations' do
    it { should belong_to(:user) }
  end
end