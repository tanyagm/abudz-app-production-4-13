# == Schema Information
#
# Table name: devices
#
#  id          :integer          not null, primary key
#  name        :string(50)       not null
#  token       :string(255)
#  uid         :string(255)
#  last_sync   :datetime
#  user_id     :integer
#  device_type :integer          default(0)
#

require 'spec_helper'

describe Device do
  describe 'association' do
    it { should belong_to(:user) }
  end

  describe 'validation' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:token) }
    it { should validate_presence_of(:uid) }
    it { should validate_presence_of(:device_type) }
    it { should validate_presence_of(:user_id) }
  end
end
