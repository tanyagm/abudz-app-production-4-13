# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  message          :text
#  commentable_id   :integer
#  commentable_type :string(255)
#  user_id          :integer
#  created_at       :datetime
#  updated_at       :datetime
#

require 'spec_helper'

describe Comment do
  it_behaves_like 'notifiable'

  describe 'Validations' do
    it { should validate_presence_of(:message) }
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:commentable_id) }
    it { should validate_presence_of(:commentable_type) }
  end

  describe 'Associations' do
    it { should belong_to(:user) }
    it { should belong_to(:commentable) }
  end

  describe 'Trackable' do
    let(:comment) { stub_model Comment, sender_id: 1, receiver_id: 1 }  

    describe 'after_save' do
      it 'should run trackable callback' do
        comment.should_receive(:record)
        comment.run_callbacks(:save)
      end
    end

    describe 'initialize' do
      it 'has time to live defined' do
        Comment.tracked_ttl.should_not be_nil
      end
    end
  end
end
