# == Schema Information
#
# Table name: user_tokens
#
#  id         :integer          not null, primary key
#  token      :string(255)      not null
#  expired_at :datetime
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe UserToken do
  describe 'validation' do
    it { should validate_presence_of(:token) }
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:expired_at) }
  end

  describe "association" do
    it { should belong_to(:user) }
  end

  describe "self.issue_token" do
    let(:user) {stub_model User, id: 4242}
    let(:user_token) {stub_model UserToken, id: 104242}
    before do
      UserToken.stub_chain(:where, :first_or_initialize).and_return(user_token)
      user_token.stub(:update!).and_return(true)
    end
    it "return a newly created UserToken" do
      expect(UserToken.issue_token(user)).to eql(user_token)
    end
  end
end
