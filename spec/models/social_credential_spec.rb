# == Schema Information
#
# Table name: social_credentials
#
#  id          :integer          not null, primary key
#  user_id     :integer          not null
#  uid         :integer          not null
#  token       :string(255)      not null
#  social_type :integer          default(0), not null
#  created_at  :datetime
#  updated_at  :datetime
#  secret      :string(255)
#

require 'spec_helper'

describe SocialCredential do
  describe 'Validations' do
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:uid) }
    it { should validate_presence_of(:token) }
    it { should validate_presence_of(:social_type) }
  end

  describe 'Associations' do
    it { should belong_to(:user) }
  end
end
