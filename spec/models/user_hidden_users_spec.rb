# == Schema Information
#
# Table name: user_hidden_users
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  hidden_user_id :integer
#  hidden_until   :datetime
#  created_at     :datetime
#  updated_at     :datetime
#

require 'spec_helper'

describe UserHiddenUsers do
  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:hidden_user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:hidden_user_id) }
    it { should validate_uniqueness_of(:user_id).scoped_to(:hidden_user_id)}
  end

  describe 'update_expiry' do
    let(:user_hidden_users) { stub_model UserHiddenUsers }
    let(:datetime){Time.now}

    before do
      user_hidden_users.stub(:update!)
    end

    context 'datetime supplied' do
      it 'update hidden_until with supplied datetime' do
        user_hidden_users.should_receive(:update!).with({hidden_until: datetime})
        user_hidden_users.update_expiry(datetime)
      end
    end
    
    context 'datetime not supplied' do
      before do
        user_hidden_users.stub(:default_expiry).and_return(datetime)
      end

      it 'update hidden_until with default_expiry' do
        user_hidden_users.should_receive(:update!).with({hidden_until: datetime})
        user_hidden_users.update_expiry
      end
    end
    
  end
end
