# == Schema Information
#
# Table name: user_details
#
#  id         :integer          not null, primary key
#  key        :integer
#  value      :text
#  private    :integer          default(0)
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe UserDetail do
  # Refer to test specific question set question_set_test.yml

  describe "validation" do
    it { should validate_presence_of(:user_id) }
    
    describe 'fieldname' do
      it { should_not allow_value("non_existant_field").for(:fieldname) }
      it { should allow_value("multiple_answer").for(:fieldname) }
    end

    describe 'value' do
      describe 'single_answer' do
        let(:user_detail) {stub_model UserDetail, fieldname: 'single_answer'}
        describe 'allowed' do
          it 'allow valid value' do
            user_detail.value = 0
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
          it 'allow empty value' do
            user_detail.value = nil
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
        end

        describe 'not allowed' do
          it 'do not allow out of bound value' do
            user_detail.value = 4
            user_detail.valid?
            expect(user_detail.errors.messages).to include :value
          end
          it 'do not allow negative value' do
            user_detail.value = -1
            user_detail.valid?
            expect(user_detail.errors.messages).to include :value
          end
        end
      end

      describe 'multiple_answer' do
        let(:user_detail) {stub_model UserDetail, fieldname: 'multiple_answer'}
        describe 'allowed' do
          it 'allow valid value' do
            user_detail.value = [0, 3, 4]
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
          it 'allow empty array' do
            user_detail.value = []
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
        end

        describe 'not allowed' do
          it 'does not allow non array value' do
            user_detail.value = 123
            user_detail.valid?
            expect(user_detail.errors.messages).to include :value
          end
          it 'do not allow out of bound answer' do
            user_detail.value = [0, 1, 5]
            user_detail.valid?
            expect(user_detail.errors.messages).to include :value
          end
          it 'do not allow negative value' do
            user_detail.value = [-1, 0]
            user_detail.valid?
            expect(user_detail.errors.messages).to include :value
          end
        end
      end

      describe 'date' do
        let(:user_detail) {stub_model UserDetail, fieldname: 'date'}
        describe 'allowed' do
          it 'allow valid yyyy/mm/dd format date' do
            user_detail.value = '1991/08/17'
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
          it 'allow valid dd/mm/yyyy format date' do
            user_detail.value = '17/08/1991'
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
        end

        describe 'not allowed' do
          it 'do not allow invalid date' do
            user_detail.value = '1/22/29'
            user_detail.valid?
            expect(user_detail.errors.messages).to include :value
          end
        end
      end

      describe 'string' do
        let(:user_detail) {stub_model UserDetail, fieldname: 'string'}
        describe 'allow' do
          it 'any string' do
            user_detail.value = 'heyheyhey'
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
          it 'empty string' do
            user_detail.value = ''
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
        end
      end

      describe 'multiple_string' do
        let(:user_detail) {stub_model UserDetail, fieldname: 'multiple_string'}
        describe 'allowed' do
          it 'allow valid value' do
            user_detail.value = ["aaa", "bbb", "ccc"]
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
          it 'allow empty array' do
            user_detail.value = []
            user_detail.valid?
            expect(user_detail.errors.messages).to_not include :value
          end
        end

        describe 'not allowed' do
          it 'does not allow non array value' do
            user_detail.value = 123
            user_detail.valid?
            expect(user_detail.errors.messages).to include :value
          end
        end
      end



    end

  end

  describe "association" do
    it { should belong_to(:user) }
  end

end
