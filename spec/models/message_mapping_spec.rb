# == Schema Information
#
# Table name: message_mappings
#
#  id                 :integer          not null, primary key
#  first_user_id      :integer          not null
#  second_user_id     :integer          not null
#  message_id         :integer          not null
#  last_message       :string(255)      not null
#  first_user_opened  :boolean          default(FALSE)
#  second_user_opened :boolean          default(FALSE)
#  created_at         :datetime
#  updated_at         :datetime
#  sort_order         :integer
#

require 'spec_helper'

describe MessageMapping do
  
	describe 'Associations' do
    it { should belong_to(:message) }
    it { should belong_to(:first_user) }
    it { should belong_to(:second_user) }
  end	

  describe MessageMapping, '#validate' do
    it_should_run_callbacks :same_users
  end

  describe MessageMapping, '#before_save' do
    it_should_run_callbacks :set_sort_order_timestamp
  end

	describe 'Validations' do
    it { should validate_presence_of(:first_user_id) }
    it { should validate_presence_of(:second_user_id) }
    it { should validate_presence_of(:message_id) }
    it { should validate_presence_of(:last_message) } 
  end	

  describe '#participant_ids' do
    let (:message_mapping) { stub_model MessageMapping, first_user_id: 1, second_user_id: 2 }

    it "returns an array of participant user ids" do
      message_mapping.participant_ids.should be_eql [1, 2]
    end
  end

  describe '#same_users' do
    let (:message_mapping) { stub_model MessageMapping, first_user_id: 1, second_user_id: 1 }

    it "Add errors to second_user_id" do
      message_mapping.send(:same_users)
      message_mapping.should have(1).errors_on(:second_user_id)
    end
  end

  describe '#set_sort_order_timestamp' do
    let (:message_mapping) { stub_model MessageMapping, id: 1 }

    before do
      Time.stub_chain(:now, :to_i).and_return(1234)
    end

    it "set sort order with UNIX timestamp" do
      message_mapping.send(:set_sort_order_timestamp)
      message_mapping.sort_order.should be_eql 1234
    end
  end

  describe '.unreads_count' do
    let(:user) { stub_model User, id: 1 }
    let(:unread_conversations) do
     [
      stub_model(MessageMapping, first_user_id: 1, first_user_opened: false),
      stub_model(MessageMapping, first_user_id: 2, first_user_opened: false),
      stub_model(MessageMapping, second_user_id: 1, second_user_opened: false),
      stub_model(MessageMapping, second_user_id: 1, second_user_opened: true),
     ]
   end

   it 'returns user unread conversations' do
    MessageMapping.stub(:list_of_conversation).and_return(unread_conversations)

    MessageMapping.unreads_count(user.id).should be_eql(2)
   end
  end

end
