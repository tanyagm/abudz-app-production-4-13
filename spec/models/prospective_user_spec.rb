# == Schema Information
#
# Table name: prospective_users
#
#  id           :integer          not null, primary key
#  source_type  :integer          default(0), not null
#  source_id    :string(255)      not null
#  inviter_id   :integer
#  inviter_type :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

require 'spec_helper'

describe ProspectiveUser do
  describe 'Validations' do
    it { should validate_presence_of(:source_type) }
    it { should validate_presence_of(:source_id) }
    it { should validate_presence_of(:inviter_id) }
  end

  describe 'Associations' do
    it { should belong_to(:inviter) }
  end
end
