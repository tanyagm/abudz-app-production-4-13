# == Schema Information
#
# Table name: deleted_message_mappings
#
#  id                 :integer          not null, primary key
#  user_id            :integer          not null
#  message_mapping_id :integer          not null
#  created_at         :datetime
#

require 'spec_helper'

describe DeletedMessageMapping do
  describe 'Associations' do
    it { should belong_to(:user) }
    it { should belong_to(:message_mapping) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:message_mapping_id) }
  end
end
