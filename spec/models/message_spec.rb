# == Schema Information
#
# Table name: messages
#
#  id              :integer          not null, primary key
#  sender_id       :integer          not null
#  receiver_id     :integer          not null
#  body            :string(255)      not null
#  sender_delete   :boolean          default(FALSE)
#  receiver_delete :boolean          default(FALSE)
#  root_id         :integer
#  opened          :boolean          default(FALSE)
#  created_at      :datetime
#  updated_at      :datetime
#

require 'spec_helper'

describe Message do
  it_behaves_like 'notifiable'
  
  describe 'Associations' do
    it { should belong_to(:sender) }
    it { should belong_to(:receiver) }
  end	

  describe 'Validations' do
    it { should validate_presence_of(:sender_id) }
    it { should validate_presence_of(:receiver_id) }
    it { should validate_presence_of(:body) } 
  end 	

  describe Message, '#validate' do
    it_should_run_callbacks :sent_to_self
  end

  describe '#participant_ids' do
    let (:message) { stub_model Message, sender_id: 1, receiver_id: 2 }

    it 'returns an array of participant user ids' do
      message.participant_ids.should be_eql [1, 2]
    end
  end

  describe '#sent_to_self' do
    let (:message) { stub_model Message, sender_id: 1, receiver_id: 1 }

    it 'Add errors to receiver_id' do
      message.send(:sent_to_self)
      message.should have(1).errors_on(:receiver_id)
    end
  end

end
