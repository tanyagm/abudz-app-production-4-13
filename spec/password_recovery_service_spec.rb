require 'spec_helper'

describe PasswordRecoveryService do
  let(:user){stub_model User, id: 4321}
  let(:service){PasswordRecoveryService.new(user)}
  let(:user_token){stub_model UserToken, id:132}

  describe 'send_reset_password_instruction' do
    before do
      user.stub(:issue_password_recovery_token).and_return(user_token)
      UserMailer.stub_chain(:reset_password_instruction, :deliver!)
    end

    it 'send reset_password_instruction email' do
      UserMailer.should_receive(:reset_password_instruction).with(user, user_token)
      service.send_reset_password_instruction
    end
  end

  describe 'enqueue_job' do
    before do
      Resque.stub(:enqueue_in)
    end

    it 'enqueue Jobs::BookkeepingAttach' do
      Resque.should_receive(:enqueue_in).with(
          an_instance_of(Fixnum), 
          Jobs::EmailPasswordRecovery,
          user.id)
      PasswordRecoveryService.enqueue_job(user.id)
    end
  end
end
