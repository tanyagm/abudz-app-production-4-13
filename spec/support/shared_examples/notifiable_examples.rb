shared_examples_for "notifiable" do
  describe 'notify' do
    let (:child) { stub_model described_class }
    let (:notification_target) { stub_model User, id: 123, first_name: 'first', last_name: 'last', email: 'red@test.com' }
    let (:counter) { stub_model Counter, id: 1 }

    before do
      notification_target.stub_chain(:counters, :find_or_create_by).and_return(counter)
      Counter.stub(:update_counters).and_return(2)
    end

    it 'create notifications' do
      child.notifications.should_receive(:create).with(notification_target: notification_target)

      child.send(:notify, notification_target)
    end

    it 'find or create counter record' do
      notification_target.counters.should_receive(:find_or_create_by).with(counter_type: child.class.counter_cache_column)
        .and_return(counter)

      child.send(:notify, notification_target)
    end

    it 'increment count column' do
      Counter.should_receive(:update_counters).with(counter.id, :count => 1)

      child.send(:notify, notification_target)
    end
  end
end