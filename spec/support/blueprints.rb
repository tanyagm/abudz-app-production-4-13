require 'machinist/active_record'

# Add your blueprints here.
#
# e.g.
#   Post.blueprint do
#     title { "Post #{sn}" }
#     body  { "Lorem ipsum..." }
#   end

User.blueprint do
  email { "email#{sn}@email.com" }
  password { "password01 "}
  password_confirmation { "password01 "}
  first_name { "#{sn}first_name"}
  last_name { "#{sn}last_name"}
end

Post.blueprint do
	# Attributes here
end

Message.blueprint do
  # Attributes here
end

MessageMapping.blueprint do
  # Attributes here
end

UserDetail.blueprint do
  user_id { 1 }
end

DeletedMessageMapping.blueprint do
  # Attributes here
end

FriendRelation.blueprint do
  from_user_id { 10001 }
  to_user_id { 30001 }
  status { FriendRelation::PENDING }
end

UserHiddenUsers.blueprint do

end

Tribe.blueprint do
  # Attributes here
end

SocialCredential.blueprint do
  # Attributes here
end

ProspectiveUser.blueprint do
  # Attributes here
end

UserToken.blueprint do
  # Attributes here
end
