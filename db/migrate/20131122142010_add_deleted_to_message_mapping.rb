class AddDeletedToMessageMapping < ActiveRecord::Migration
  def change
    add_column :message_mappings, :deleted, :boolean, default: false
    
    remove_index :message_mappings, :sort_order
    remove_index :message_mappings, [:first_user_id, :second_user_id]
    
    add_index :message_mappings, [:first_user_id, :second_user_id, :deleted], name: 'first_second_deleted_index'
  end
end
