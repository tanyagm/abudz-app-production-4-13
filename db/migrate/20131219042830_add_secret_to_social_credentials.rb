class AddSecretToSocialCredentials < ActiveRecord::Migration
  def change
    add_column :social_credentials, :secret, :string, null: false
  end
end
