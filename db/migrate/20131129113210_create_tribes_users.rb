class CreateTribesUsers < ActiveRecord::Migration
  def change
    create_table :tribes_users do |t|
      t.references :user
      t.references :tribe
    end

    add_index :tribes_users, :user_id
    add_index :tribes_users, :tribe_id
    add_index :tribes_users, [:user_id, :tribe_id], unique: true
  
  end
end
