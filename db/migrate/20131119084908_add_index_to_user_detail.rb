class AddIndexToUserDetail < ActiveRecord::Migration
  def change
    add_index :user_details, :user_id
  end
end
