class AddLastSuggestedUserToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_suggested_users, :string
    remove_column :users, :notified_at
  end
end
