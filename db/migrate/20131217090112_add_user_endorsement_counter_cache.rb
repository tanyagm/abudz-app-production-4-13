class AddUserEndorsementCounterCache < ActiveRecord::Migration
  def change
    add_column :users, :cached_votes_total, :integer, :default => 0
    add_index  :users, :cached_votes_total
  end
end
