class CreateComments < ActiveRecord::Migration
  def self.up
    create_table :comments do |t|
      t.text :message
      t.references :commentable, :polymorphic => true
      t.references :user
      t.timestamps
    end

    add_index :comments, :commentable_type
    add_index :comments, :commentable_id
    add_index :comments, :user_id

    add_column :posts, :comments_count, :integer, default: 0
  end

  def self.down
    drop_table :comments
    remove_column :posts, :comments_count
  end
end
