class ChangeDeviceTable < ActiveRecord::Migration
  def up
    remove_column :devices, :type
    add_column :devices, :device_type, :integer, default: 0
  end

  def down
    remove_column :devices, :device_type
    add_column :devices, :type, :string
  end
end
