class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :sender_id, null: false
      t.integer :receiver_id, null: false
      t.string  :body, null: false
      t.boolean :sender_delete, default: false
      t.boolean :receiver_delete, default: false
      t.integer :root_id
      t.boolean :opened, default: false

      t.timestamps
    end

    add_index :messages, :root_id
    add_index :messages, :sender_id
  end
end
