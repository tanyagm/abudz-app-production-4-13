class ChangeQuestionTypeForRolePreference < ActiveRecord::Migration
  def up
    UserDetail.where(key: 4).each {|x| x.update(value: [ x.value ])}
  end
  def down
    UserDetail.where(key: 4).each {|x| x.update(value: x.value.first)}
  end
end
