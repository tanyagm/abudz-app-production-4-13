class CreateDeletedMessageMappings < ActiveRecord::Migration
  def change
    create_table :deleted_message_mappings do |t|
      t.references :user, index: true, null: false
      t.references :message_mapping, index: true, null: false

      t.datetime :created_at
    end
  end
end
