class ChangeUidTypeToBigint < ActiveRecord::Migration
  def up
    change_column :social_credentials, :uid, :int8
  end

  def down
    change_column :social_credentials, :uid, :int, limit: 11
  end
end
