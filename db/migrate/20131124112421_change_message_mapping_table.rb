class ChangeMessageMappingTable < ActiveRecord::Migration
  def change
    remove_index :message_mappings, name: 'first_second_deleted_index'
    remove_column :message_mappings, :deleted
    add_index :message_mappings, [:first_user_id, :second_user_id]
  end
end
