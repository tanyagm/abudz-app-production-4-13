class AddNotificationCounterCache < ActiveRecord::Migration
  def change
    add_column :users, :comment_notifications_count, :integer, default: 0
    add_column :users, :friend_notifications_count, :integer, default: 0
    add_column :users, :message_notifications_count, :integer, default: 0
    add_column :users, :tribe_post_notifications_count, :integer, default: 0
    add_column :users, :tribe_invitation_notifications_count, :integer, default: 0
  end
end
