class CreateNotificationSettings < ActiveRecord::Migration
  def change
    create_table :notification_settings do |t|
      t.integer :user_id, unique: true
      t.integer :activebudz_request_type, limit: 1
      t.integer :activebudz_suggested_type, limit: 1
      t.integer :comment_type, limit: 1
      t.integer :message_type, limit: 1
      t.integer :tribe_post_type, limit: 1
      t.integer :tribe_invitation_type, limit: 1
      t.integer :activebudz_request_freq, limit: 1
      t.integer :activebudz_suggested_freq, limit: 1
      t.integer :comment_freq, limit: 1
      t.integer :message_freq, limit: 1
      t.integer :tribe_post_freq, limit: 1
      t.integer :tribe_invitation_freq, limit: 1
      t.timestamps
    end
  end
end
