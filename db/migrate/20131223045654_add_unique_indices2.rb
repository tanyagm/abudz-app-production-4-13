class AddUniqueIndices2 < ActiveRecord::Migration
  def up
    add_index :friend_relations, [:from_user_id, :to_user_id], unique: true

    remove_index :user_hidden_users, [:user_id, :hidden_user_id]
    add_index :user_hidden_users, [:user_id, :hidden_user_id], unique: true
  end

  def down
    remove_index :friend_relations, [:from_user_id, :to_user_id]
    
    remove_index :user_hidden_users, [:user_id, :hidden_user_id]
    add_index :user_hidden_users, [:user_id, :hidden_user_id]
  end
end
