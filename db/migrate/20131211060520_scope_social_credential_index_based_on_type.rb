class ScopeSocialCredentialIndexBasedOnType < ActiveRecord::Migration
  def up
    remove_index :social_credentials, :uid
    add_index :social_credentials, [:uid, :social_type], unique: true
  end

  def down
    remove_index :social_credentials, [:uid, :social_type]
    add_index :social_credentials, :uid, unique: true
  end
end
