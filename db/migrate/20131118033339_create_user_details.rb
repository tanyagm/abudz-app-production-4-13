class CreateUserDetails < ActiveRecord::Migration
  def change
    create_table :user_details do |t|
      t.integer :key
      t.string :value
      t.integer :private, default: 0
      t.integer :user_id
      t.timestamps
    end
  end
end
