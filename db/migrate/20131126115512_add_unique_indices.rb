class AddUniqueIndices < ActiveRecord::Migration
  def change
    add_index :users, :email, unique: true
    add_index :deleted_message_mappings, [ :user_id, :message_mapping_id ],
      unique: true
    add_index :user_details, [ :user_id, :key ], unique: true
  end
end
