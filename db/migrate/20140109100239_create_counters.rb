class CreateCounters < ActiveRecord::Migration
  def change
    create_table :counters do |t|
      t.integer :counter_type
      t.integer :count, default: 0
      t.integer :user_id
    end
  end
end
