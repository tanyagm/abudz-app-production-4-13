class AddSortOrderToMessageMapping < ActiveRecord::Migration
  def change
    add_column :message_mappings, :sort_order, :integer
    add_index :message_mappings, :sort_order
  end
end
