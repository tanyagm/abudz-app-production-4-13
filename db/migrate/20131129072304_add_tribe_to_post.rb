class AddTribeToPost < ActiveRecord::Migration
  
  def up
    add_column :posts, :tribe_id, :integer
    add_index :posts, :tribe_id
  end

  def down
    remove_column :posts, :tribe_id
    remove_index :posts, :tribe_id
  end

end
