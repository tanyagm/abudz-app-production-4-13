class RemoveNotificationCounterFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :comment_notifications_count
    remove_column :users, :friend_notifications_count
    remove_column :users, :message_notifications_count
    remove_column :users, :tribe_post_notifications_count
    remove_column :users, :tribe_invitation_notifications_count
  end

  def down
    add_column :users, :comment_notifications_count, :integer
    add_column :users, :friend_notifications_count, :integer
    add_column :users, :message_notifications_count, :integer
    add_column :users, :tribe_post_notifications_count, :integer
    add_column :users, :tribe_invitation_notifications_count, :integer
  end
end
