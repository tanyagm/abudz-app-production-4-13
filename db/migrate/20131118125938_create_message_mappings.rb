class CreateMessageMappings < ActiveRecord::Migration
  def change
    create_table :message_mappings do |t|
      t.integer :first_user_id, null: false
      t.integer :second_user_id, null: false
      t.integer :message_id, null: false
      t.string  :last_message, null: false
      t.boolean :first_user_opened, default: false
      t.boolean :second_user_opened, default: false

      t.timestamps
    end

    add_index :message_mappings, [:first_user_id, :second_user_id]
  end
end
