class ChangeUserDetailColumn < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("TRUNCATE user_details")
    change_column :user_details, :value, :text
  end

  def down
    change_column :user_details, :value, :string
  end
end
