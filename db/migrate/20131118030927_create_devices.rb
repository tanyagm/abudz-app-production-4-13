class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :name, :limit => 50, :null => false
      t.string :token
      t.string :uid
      t.string :type
      t.datetime :last_sync
      t.references :user
    end

    add_index :devices, :user_id
  end
end
