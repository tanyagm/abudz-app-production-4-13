class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
    	t.integer :user_id, :null => false
    	t.string :message, :null => false
    	t.string :image
    	t.string :location_name
    	t.float :lat
    	t.float :lon
    	t.timestamps
    end
  end
end
