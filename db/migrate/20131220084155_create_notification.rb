class CreateNotification < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :notifiable, polymorphic: true
      t.references :notification_target, polymorphic: true

      t.timestamps
    end
  end
end
