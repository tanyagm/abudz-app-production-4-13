class CreateFriendRelations < ActiveRecord::Migration
  def change
    create_table :friend_relations do |t|
      t.integer :from_user_id
      t.integer :to_user_id
      t.integer :status, default: 0
      t.timestamps
    end
    add_index :friend_relations, [:from_user_id, :to_user_id, :status]
  end
end
