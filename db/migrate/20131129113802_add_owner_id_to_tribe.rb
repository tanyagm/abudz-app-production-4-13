class AddOwnerIdToTribe < ActiveRecord::Migration
  def up
    add_column :tribes, :owner_id, :integer
  end

  def down
    add_column :tribes, :owner_id
  end
end
