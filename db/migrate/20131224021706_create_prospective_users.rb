class CreateProspectiveUsers < ActiveRecord::Migration
  def change
    create_table :prospective_users do |t|
      t.integer :source_type, null: false, default: 0
      t.string :source_id, null: false
      t.references :inviter, :polymorphic => true
      t.timestamps
    end
    add_index :prospective_users, [:source_type, :source_id]
  end
end
