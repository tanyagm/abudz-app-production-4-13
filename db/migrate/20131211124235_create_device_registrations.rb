class CreateDeviceRegistrations < ActiveRecord::Migration
  def change
    create_table :device_registrations do |t|
    	t.string :fullname, :limit => 50
      t.string :email, :null => false
      t.string :udid, :null => false
      t.string :device_name
      t.timestamps
    end
  end
end
