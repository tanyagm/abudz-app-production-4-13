class AlterSocialCredentialTable < ActiveRecord::Migration
  def up
    change_column :social_credentials, :secret, :string, default: nil, null: true
  end

  def down
    change_column :social_credentials, :secret, :string, null: false
  end
end
