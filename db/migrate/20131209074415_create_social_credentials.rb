class CreateSocialCredentials < ActiveRecord::Migration
  def change
    create_table :social_credentials do |t|
      t.integer :user_id, :null => false
      t.integer :uid, :null => false
      t.string :token, :null => false
      t.integer :social_type, :null => false, default: 0

      t.timestamps
    end

    add_index :social_credentials, :uid, unique: true
    
  end
end
