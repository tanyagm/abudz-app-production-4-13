class CreateUserTokens < ActiveRecord::Migration
  def change
    create_table :user_tokens do |t|
      t.string :token, null: false
      t.datetime :expired_at
      t.integer :user_id
      t.timestamps
    end
    add_index :user_tokens, [:token, :user_id], unique: true
  end
end
