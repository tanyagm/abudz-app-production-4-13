class ChangeNotifSettingDefaultValue < ActiveRecord::Migration
  def change
    change_column :notification_settings, :activebudz_request_type, :integer, :default => 1
    change_column :notification_settings, :activebudz_suggested_type, :integer, :default => 1
    change_column :notification_settings, :comment_type, :integer, :default => 1
    change_column :notification_settings, :message_type, :integer, :default => 1
    change_column :notification_settings, :tribe_post_type, :integer, :default => 1
    change_column :notification_settings, :tribe_invitation_type, :integer, :default => 1
    change_column :notification_settings, :activebudz_request_freq, :integer, :default => 3
    change_column :notification_settings, :activebudz_suggested_freq, :integer, :default => 3
    change_column :notification_settings, :comment_freq, :integer, :default => 2
    change_column :notification_settings, :message_freq, :integer, :default => 2
    change_column :notification_settings, :tribe_post_freq, :integer, :default => 2
    change_column :notification_settings, :tribe_invitation_freq, :integer, :default => 2

    add_index :notification_settings, :user_id, unique: true
  end
end
