class CreateTribes < ActiveRecord::Migration
  def change
    create_table :tribes do |t|
      t.string :name, null: false, limit: 50
      t.string :description
      t.string :location_name, limit: 50
      t.string :activity, null: false, limit: 50
      t.float :lat
      t.float :lon
      t.string :image

      t.timestamps
    end
    
    add_index :tribes, :name, unique: true

  end
end
