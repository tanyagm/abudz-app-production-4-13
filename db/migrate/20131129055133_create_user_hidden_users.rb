class CreateUserHiddenUsers < ActiveRecord::Migration
  def change
    create_table :user_hidden_users do |t|
      t.integer :user_id
      t.integer :hidden_user_id
      t.datetime :hidden_until
      t.timestamps
    end
    
    add_index :user_hidden_users, [:user_id, :hidden_user_id]
  end
end
