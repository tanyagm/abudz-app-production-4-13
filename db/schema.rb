# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140109100239) do

  create_table "comments", force: true do |t|
    t.text     "message"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["commentable_id"], name: "index_comments_on_commentable_id", using: :btree
  add_index "comments", ["commentable_type"], name: "index_comments_on_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "counters", force: true do |t|
    t.integer "counter_type"
    t.integer "count",        default: 0
    t.integer "user_id"
  end

  create_table "deleted_message_mappings", force: true do |t|
    t.integer  "user_id",            null: false
    t.integer  "message_mapping_id", null: false
    t.datetime "created_at"
  end

  add_index "deleted_message_mappings", ["message_mapping_id"], name: "index_deleted_message_mappings_on_message_mapping_id", using: :btree
  add_index "deleted_message_mappings", ["user_id", "message_mapping_id"], name: "index_deleted_message_mappings_on_user_id_and_message_mapping_id", unique: true, using: :btree
  add_index "deleted_message_mappings", ["user_id"], name: "index_deleted_message_mappings_on_user_id", using: :btree

  create_table "device_registrations", force: true do |t|
    t.string   "fullname",    limit: 50
    t.string   "email",                  null: false
    t.string   "udid",                   null: false
    t.string   "device_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "devices", force: true do |t|
    t.string   "name",        limit: 50,             null: false
    t.string   "token"
    t.string   "uid"
    t.datetime "last_sync"
    t.integer  "user_id"
    t.integer  "device_type",            default: 0
  end

  add_index "devices", ["user_id"], name: "index_devices_on_user_id", using: :btree

  create_table "friend_relations", force: true do |t|
    t.integer  "from_user_id"
    t.integer  "to_user_id"
    t.integer  "status",       default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "friend_relations", ["from_user_id", "to_user_id", "status"], name: "index_friend_relations_on_from_user_id_and_to_user_id_and_status", using: :btree
  add_index "friend_relations", ["from_user_id", "to_user_id"], name: "index_friend_relations_on_from_user_id_and_to_user_id", unique: true, using: :btree

  create_table "message_mappings", force: true do |t|
    t.integer  "first_user_id",                      null: false
    t.integer  "second_user_id",                     null: false
    t.integer  "message_id",                         null: false
    t.string   "last_message",                       null: false
    t.boolean  "first_user_opened",  default: false
    t.boolean  "second_user_opened", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sort_order"
  end

  add_index "message_mappings", ["first_user_id", "second_user_id"], name: "index_message_mappings_on_first_user_id_and_second_user_id", using: :btree

  create_table "messages", force: true do |t|
    t.integer  "sender_id",                       null: false
    t.integer  "receiver_id",                     null: false
    t.string   "body",                            null: false
    t.boolean  "sender_delete",   default: false
    t.boolean  "receiver_delete", default: false
    t.integer  "root_id"
    t.boolean  "opened",          default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["root_id"], name: "index_messages_on_root_id", using: :btree
  add_index "messages", ["sender_id"], name: "index_messages_on_sender_id", using: :btree

  create_table "notification_settings", force: true do |t|
    t.integer  "user_id"
    t.integer  "activebudz_request_type",   default: 1
    t.integer  "activebudz_suggested_type", default: 1
    t.integer  "comment_type",              default: 1
    t.integer  "message_type",              default: 1
    t.integer  "tribe_post_type",           default: 1
    t.integer  "tribe_invitation_type",     default: 1
    t.integer  "activebudz_request_freq",   default: 3
    t.integer  "activebudz_suggested_freq", default: 3
    t.integer  "comment_freq",              default: 2
    t.integer  "message_freq",              default: 2
    t.integer  "tribe_post_freq",           default: 2
    t.integer  "tribe_invitation_freq",     default: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", force: true do |t|
    t.integer  "notifiable_id"
    t.string   "notifiable_type"
    t.integer  "notification_target_id"
    t.string   "notification_target_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "oauth_access_grants", force: true do |t|
    t.integer  "resource_owner_id",              null: false
    t.integer  "application_id",                 null: false
    t.string   "token",                          null: false
    t.integer  "expires_in",                     null: false
    t.string   "redirect_uri",      limit: 2048, null: false
    t.datetime "created_at",                     null: false
    t.datetime "revoked_at"
    t.string   "scopes"
  end

  add_index "oauth_access_grants", ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree

  create_table "oauth_access_tokens", force: true do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id",    null: false
    t.string   "token",             null: false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",        null: false
    t.string   "scopes"
  end

  add_index "oauth_access_tokens", ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
  add_index "oauth_access_tokens", ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
  add_index "oauth_access_tokens", ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree

  create_table "oauth_applications", force: true do |t|
    t.string   "name",                      null: false
    t.string   "uid",                       null: false
    t.string   "secret",                    null: false
    t.string   "redirect_uri", limit: 2048, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "oauth_applications", ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree

  create_table "posts", force: true do |t|
    t.integer  "user_id",                        null: false
    t.string   "message",                        null: false
    t.string   "image"
    t.string   "location_name"
    t.float    "lat"
    t.float    "lon"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cached_votes_total", default: 0
    t.integer  "cached_votes_up",    default: 0
    t.integer  "tribe_id"
    t.integer  "comments_count",     default: 0
  end

  add_index "posts", ["cached_votes_total"], name: "index_posts_on_cached_votes_total", using: :btree
  add_index "posts", ["cached_votes_up"], name: "index_posts_on_cached_votes_up", using: :btree
  add_index "posts", ["tribe_id"], name: "index_posts_on_tribe_id", using: :btree

  create_table "prospective_users", force: true do |t|
    t.integer  "source_type",  default: 0, null: false
    t.string   "source_id",                null: false
    t.integer  "inviter_id"
    t.string   "inviter_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "prospective_users", ["source_type", "source_id"], name: "index_prospective_users_on_source_type_and_source_id", using: :btree

  create_table "social_credentials", force: true do |t|
    t.integer  "user_id",                           null: false
    t.integer  "uid",         limit: 8,             null: false
    t.string   "token",                             null: false
    t.integer  "social_type",           default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "secret"
  end

  add_index "social_credentials", ["uid", "social_type"], name: "index_social_credentials_on_uid_and_social_type", unique: true, using: :btree

  create_table "tribes", force: true do |t|
    t.string   "name",          limit: 50, null: false
    t.string   "description"
    t.string   "location_name", limit: 50
    t.string   "activity",      limit: 50, null: false
    t.float    "lat"
    t.float    "lon"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owner_id"
  end

  add_index "tribes", ["name"], name: "index_tribes_on_name", unique: true, using: :btree

  create_table "tribes_users", force: true do |t|
    t.integer "user_id"
    t.integer "tribe_id"
  end

  add_index "tribes_users", ["tribe_id"], name: "index_tribes_users_on_tribe_id", using: :btree
  add_index "tribes_users", ["user_id", "tribe_id"], name: "index_tribes_users_on_user_id_and_tribe_id", unique: true, using: :btree
  add_index "tribes_users", ["user_id"], name: "index_tribes_users_on_user_id", using: :btree

  create_table "user_details", force: true do |t|
    t.integer  "key"
    t.text     "value"
    t.integer  "private",    default: 0
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_details", ["user_id", "key"], name: "index_user_details_on_user_id_and_key", unique: true, using: :btree
  add_index "user_details", ["user_id"], name: "index_user_details_on_user_id", using: :btree

  create_table "user_hidden_users", force: true do |t|
    t.integer  "user_id"
    t.integer  "hidden_user_id"
    t.datetime "hidden_until"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_hidden_users", ["user_id", "hidden_user_id"], name: "index_user_hidden_users_on_user_id_and_hidden_user_id", unique: true, using: :btree

  create_table "user_tokens", force: true do |t|
    t.string   "token",      null: false
    t.datetime "expired_at"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_tokens", ["token", "user_id"], name: "index_user_tokens_on_token_and_user_id", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar"
    t.integer  "cached_votes_total",   default: 0
    t.string   "last_suggested_users"
  end

  add_index "users", ["cached_votes_total"], name: "index_users_on_cached_votes_total", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  create_table "votes", force: true do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

end
