require "bundler/capistrano"
require "capistrano/ext/multistage"
require "rvm/capistrano"

# environments development to aws dev; staging to aws stage; production to aws prod
set :stages, %w(production staging development sandbox)
set :default_stage, "sandbox"

# app
set :application, "gotribal"

# server details
set :deploy_via, :remote_cache
set :use_sudo, false
set :keep_releases, 3


# repo
set :scm, "git"
# set :branch, fetch(:branch, "master")
set :git_enable_submodules, 1

# rvm
set :rvm_type, :system

###mac error dont commit
#set :rvm_ruby_string, "1.9.3"
##set :rvm_path, "/usr/local/rvm"

#####
set :bundle_without, [:development, :test]

# Load recipes
load "config/deploy/recipes/capistrano_database"
load "config/deploy/recipes/capistrano_logrotation"

#after "deploy:finalize_update", "deploy:create_more_symlinks"
after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

set :cleanup_targets,     %w(log tmp)
set :release_directories, %w(tmp)
set :shared_symlinks, {
  "config/database.yml" => "config/database.yml",
  "config/unicorn.rb" => "config/unicorn.rb",
  # "#{shared_path}/config/database.yml.prod" => "config/database.yml",
  # "config/hoptoad.yml"  => "config/hoptoad.yml",
  # "config/newrelic.yml" => "config/newrelic.yml",
  "log"                 => "/",
  "cache"               => "tmp/cache",
  "pids"                => "tmp/pids",
  "sockets"             => "tmp/sockets"
}

namespace :deploy do
  desc "Create symlinks to stage specific configuration files and shared assets"
  task :create_more_symlinks, :except => { :no_release => true } do
    # command = cleanup_targets.map { |target| "rm -fr #{current_path}/#{target}" }
    # command += release_directories.map { |directory| "mkdir -p #{directory}" }
    command = shared_symlinks.map {
      |from, to| "rm -fr #{current_path}/#{to} && ln -sf #{shared_path}/#{from} #{release_path}/#{to}"
    }

    run "cd #{release_path} && #{command.join(" && ")}"
##to do once all config done
    if stage != :production
      run "cp -f #{release_path}/config/settings/#{stage}.yml #{release_path}/config/settings/production.yml"
      run "cp -f #{release_path}/config/environments/#{stage}.rb #{release_path}/config/environments/production.rb"
      run "cp -f #{release_path}/config/unicorn/gotribal.unicorn.rb.#{stage} #{release_path}/config/unicorn/gotribal.unicorn.rb"
    end

#    if stage == :development
#    run "cp -f #{release_path}/config/environments/sandbox.rb #{release_path}/config/environments/production.rb"
#      run "cp -f /home/ubuntu/deployweb/current/config/unicorn/gotribal.unicorn.rb.#{stage} #{release_path}/config/unicorn/gotribalweb.unicorn.rb"
#      run "cp -f /home/ubuntu/deployweb/current/config/unicorn/gotribalweb.conf.#{stage} #{release_path}/config/unicorn/gotribalweb.conf"
# end
end

  [:start, :stop, :restart, :upgrade].each do |t|
    desc "#{t}ing unicorn"
    task t, :roles => :app do
     run "sudo service unicorn #{t}"
    end
  end
######webrick
#  task :stop, :roles => :app, :on_error => :continue  do
#	run "sudo /bin/kill -9 `cat /home/ubuntu/deploy/shared/pids/server.pid`"
#  end
#  task :start, :roles => :app  do
#	#run "cd #{latest_release} && sudo rm -rf #{latest_release}/tmp/cache && sudo rm -rf #{latest_release}/tmp/sockets && bundle exec rails s -p 8000 -d"
#	run "echo pls start manually due to sudo"
#	#run "cd #{latest_release} && sudo rm -rf #{latest_release}/tmp/cache && sudo rm -rf #{latest_release}/tmp/sockets && bundle exec rvmsudo /usr/local/rvm/rubies/ruby-1.9.3-p392/bin/ruby script/rails s -p80 -d"
 # end

#   task :copy_owncloud_config, :roles => :rd, :on_error => :continue do
#      run "cp -Rf #{current_path}/uniqadapter /var/www/owncloud/apps/"
#      run "cp -f #{current_path}/uniqadapter/appinfo/constants_#{stage}.php #{current_path}/uniqadapter/appinfo/constants.php"
#   end

  task :restart do
	upgrade
	restart_resque 
#	restart_resque_sched
  end

#  namespace :assets do
#    task :precompile, :roles => :web, :except => { :no_release => true } do
#      from = source.next_revision(current_revision)
#      if capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
#        run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
#      else
#        logger.info "Skipping asset pre-compilation because there were no asset changes"
#      end
#    end
#  end

   task :pipeline_precompile do
     run "cd #{latest_release}; RAILS_ENV=#{stage} bundle exec rake assets:precompile"
  end


  task :ln_assets do
#    run <<-CMD

    run "if [[ ! -d #{shared_path}/uploads ]]; then mkdir #{shared_path}/uploads ; fi"

#    run  "rm -rf #{latest_release}/public/assets &&
#         mkdir -p #{shared_path}/assets &&
#         ln -s #{shared_path}/assets #{latest_release}/public/assets"
#   run "rm -rf #{latest_release}/public/images && rm -rf #{latest_release}/public/stylesheets && rm -rf #{latest_release}/public/javascripts && 
#	mkdir -p #{shared_path}/images && mkdir -p #{shared_path}/stylesheets && mkdir -p #{shared_path}/javascripts &&
#	ln -s #{shared_path}/images #{latest_release}/public/images && ln -s #{shared_path}/stylesheets #{latest_release}/public/stylesheets && ln -s #{shared_path}/javascripts #{latest_release}/public/javascripts"
#     CMD
  end

  task :assets do
    update_code
    ln_assets

                run_locally "rake assets:precompile --trace"
                run_locally "cd public; tar -zcvf assets.tar.gz assets"
                #top.upload "public/assets.tar.gz", "#{shared_path}", :via => :scp
                top.upload "public/assets.tar.gz", "#{latest_release}/public", :via => :scp
                #run "cd #{shared_path}; rm -rf assets; tar -zxvf assets.tar.gz"
                run "cd #{latest_release}/public; tar -zxvf assets.tar.gz"
                run_locally "rm public/assets.tar.gz"
                run_locally "rm -rf public/assets"

    create_more_symlinks
    create_symlink
    migrations
    restart
  end

  task :start_resque, :roles => :rd do
     run "cd #{latest_release}; bundle exec resque-pool -d -p log/resque-pool.pid -E production"
  end

  task :stop_resque, :on_error => :continue, :roles => :rd do
     run "cd #{latest_release}; kill -QUIT `cat log/resque-pool.pid`"
  end

 task :restart_resque, :on_error => :continue do
     stop_resque
     sleep 5
     start_resque
  end

 task :start_resque_sched do
        #run "cd #{latest_release}; RAILS_ENV=#{stage} PIDFILE=log/resque-scheduler.pid BACKGROUND=yes bundle exec rake resque:scheduler"
        run "cd #{latest_release}; RAILS_ENV=production PIDFILE=log/resque-scheduler.pid BACKGROUND=yes bundle exec rake resque:scheduler"
 end

 task :stop_resque_sched, :on_error => :continue do
        run "cd #{latest_release}; kill -QUIT `cat log/resque-scheduler.pid`"
 end

 task :restart_resque_sched, :on_error => :continue do
     stop_resque_sched
     sleep 5
     start_resque_sched
  end


  task :info do
    run "rvm info"
    run "bundle --version"
    run "pwd"
    run ""
  end
end
