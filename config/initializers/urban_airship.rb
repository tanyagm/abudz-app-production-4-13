Urbanairship.application_key = AppConfig.urbanairship.key
Urbanairship.application_secret = AppConfig.urbanairship.secret
Urbanairship.master_secret = AppConfig.urbanairship.master
Urbanairship.logger = Rails.logger
Urbanairship.request_timeout = 5