IOSCertEnrollment.configure do |config|
  config.ssl_certificate_path = AppConfig.enrollment.certificate
  config.ssl_key_path = AppConfig.enrollment.key
  config.base_url = AppConfig.enrollment.url
  config.identifier = "com.gotribal"
  config.display_name = "GOTRIbal"
  config.organization = "gotribalnow.com"
end