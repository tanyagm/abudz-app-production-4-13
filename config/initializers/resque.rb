require 'resque_scheduler'
require 'resque_scheduler/server'

Resque.schedule = YAML.load_file(Rails.root.join('config', 'resque_schedule.yml'))
Resque.inline = Rails.env.test?

Resque.redis = Redis.new(:host => AppConfig.redis.host, :port => AppConfig.redis.port)

Resque::Pool.after_prefork do
  ActiveRecord::Base.connection.reconnect!
  Resque.redis.client.reconnect
end