Cloudinary.config do |config|
  config.cloud_name = AppConfig.cloudinary.cloud_name
  config.api_key = AppConfig.cloudinary.api_key
  config.api_secret = AppConfig.cloudinary.api_secret
  config.cdn_subdomain = true
end