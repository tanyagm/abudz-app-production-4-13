require 'redmon/config'
require 'redmon/redis'
require 'redmon/app'

#
# Optional config overrides
#
Redmon.configure do |config|
  config.redis_url = "redis://#{AppConfig.redis.host}:#{AppConfig.redis.port}"
  config.namespace = 'redmon'
end

if AppConfig.redmon.enabled
  Resque.enqueue(Jobs::RedmonWorker)
end