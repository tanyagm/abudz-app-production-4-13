GoTribal::Application.routes.draw do
  use_doorkeeper

  # TODO: Because we haven't implemented the administrative features
  # Not mounting the resque-web in production and staging environments using Rails configuration.
  if AppConfig.resque.mount_web_page
    mount Resque::Server => "/resque"
  end

  if AppConfig.redmon.enabled
    mount Redmon::App => '/redmon'
  end

  if AppConfig.elasticsearch.simple_browser.enabled
    get '/elasticsearch', to: 'elasticsearch#index'
  end

  namespace :api do
    namespace :v1 do
      get '/me', to: 'users#me'
      get '/me/stats', to: 'users#stats'
      get '/users/:id', to: 'users#show'
      get '/users/:id/connection', to: 'users#connection'
      get '/suggestion(/:from/:size)', to: 'users#suggestion'
      get '/activities(/:from/:size)', to: 'activities#index'

      # Create User
      post '/sign_up', to: 'users#create'
      post '/sign_in_fb', to: 'session#sign_in_fb'
      post '/sign_in', to: 'session#sign_in'
      delete '/sign_out', to: 'session#sign_out'

      # Update User / Fitness Profile
      put '/me', to: 'users#update'
      put '/fitness_profile', to: 'users#update'

      # Tribal rank endpoints
      post '/users/:id/endorse', to: 'users#endorse'
      delete '/users/:id/endorse', to: 'users#unendorse'

      # User Devices
      get '/devices', to: 'devices#index'
      post '/devices', to: 'devices#create'
      delete '/devices/:id', to: 'devices#destroy'

      # Friends (ActiveBudz)
      get '/friends', to: 'friends#index'
      get '/friends/pending_approval', to: 'friends#pending_request_users'
      get '/friends/received_request', to: 'friends#received_request_users'
      post '/friends/:id', to: 'friends#add'
      delete '/friends/:id', to: 'friends#remove'

      # Post Activity stream
      get '/posts', to: 'posts#index'
      post '/posts', to: 'posts#create'
      delete '/posts/:id', to: 'posts#delete'
      post '/posts/:id/vote', to: 'posts#like'
      delete '/posts/:id/vote', to: 'posts#unlike'

      # Comment on Post 
      get '/posts/:post_id/comments', to: 'comments#index'
      post '/posts/:post_id/comments', to: 'comments#create'
      delete '/posts/:post_id/comments/:id', to: 'comments#destroy'

      # Direct Messages
      get '/direct_messages', to: 'direct_messages#index'
      get '/direct_messages/:id', to: 'direct_messages#show'
      get '/direct_messages/:id/conversation', to: 'direct_messages#conversation'
      post '/direct_messages/new', to: 'direct_messages#new'
      post '/direct_messages', to: 'direct_messages#create'
      delete '/direct_messages/:id', to: 'direct_messages#destroy'
      post '/direct_messages/auth', to: 'direct_messages#auth'      

      # Tribes
      post '/tribes', to: 'tribes#create'
      get '/tribes', to: 'tribes#index'
      get '/tribes/joined', to: 'tribes#joined'
      get '/tribes/owned', to: 'tribes#owned'
      get '/tribes/:id/members', to: 'tribes/members#index'
      post '/tribes/:id/join', to: 'tribes#join'
      delete '/tribes/:id/leave', to: 'tribes#leave'
      post '/tribes/:id/posts', to: 'tribes/posts#create'
      get '/tribes/:id/posts', to: 'tribes/posts#index'
      get '/tribes/search(/:from/:size)', to: 'tribes#search'
      
      # Social credentials
      post '/socials', to: 'social_credentials#create'
      delete '/socials', to: 'social_credentials#destroy'

      # Invites
      post '/invites', to: 'invites#send_invites'
      post '/tribes/:tribe_id/invites', to: 'tribes/invites#send_invites'
      get '/tribes/:tribe_id/inviteable', to: 'tribes/invites#inviteable'
      
      # Password Recovery
      post '/recover_password/', to: 'password_recoveries#request_instruction'

      # Notification setting
      post '/notif_settings', to: 'notification_settings#set_notif'
      get '/notif_settings', to: 'notification_settings#show'

      match '*unmatched_route', to: 'application#raise_not_found!', via: [:get, :post, :put, :patch, :delete]
    end
  end

  # Password Recovery
  get '/password_recovery/:token', to: 'passwords#edit'
  put '/password_recovery/:token', to: 'passwords#update'

  get '/udid', to: 'udid#initialize'
  get '/udid/start', to: 'udid#start'
  get '/udid/install', to: 'udid#install'
  post '/udid/profile', to: 'udid#profile'
  post '/udid/scep' => 'udid#scep'
  get '/udid/scep' => 'udid#show_scep'
  get '/udid/enroll' => 'udid#enroll'
  get '/udid/done' => 'udid#done'
  get '/udid/start' => 'udid#start'
  get '/udid/step' => 'udid#step'

  root to: 'home#raise_not_found!', via: [:get, :post, :put, :patch, :delete]
  match '*unmatched_route', to: 'home#raise_not_found!', via: [:get, :post, :put, :patch, :delete]

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
