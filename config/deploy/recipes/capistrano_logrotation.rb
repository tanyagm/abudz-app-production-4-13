unless Capistrano::Configuration.respond_to?(:instance)
  abort "This extension requires Capistrano 2"
end

namespace :deploy do
  desc "Creates logrotate file"
  task :install_logrotation, :roles => :app do
    logrotate = <<-BASH
      #{shared_path}/log/*.log {
        daily
        missingok
        notifempty
        rotate 7
        compress
        dateext
        size 2M
        delaycompress
        sharedscripts
        lastaction
        pid=/home/ubuntu/deploystar/shared/pids/unicorn.pid
        test -s $pid && kill -USR1 "$(cat $pid)"
        endscript }
    BASH
    tmpfile = "/tmp/#{application}.logrotate"

    put(logrotate, tmpfile)
    run "#{sudo} chown root:root #{tmpfile} && #{sudo} mv -f #{tmpfile} /etc/logrotate.d/#{application}"
  end
end