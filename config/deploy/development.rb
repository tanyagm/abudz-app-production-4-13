##### Development
# server "ec2-54-251-208-42.ap-southeast-1.compute.amazonaws.com", :app, :web, :db, :primary => true
server "54.254.204.240", :app, :web, :db, :rd, :primary => true

#ssh_options[:keys] = [".keys/ring-sg-key.pem"]

set :deploy_to, "/home/ubuntu/deploy"
set :rails_env, "production"
set :user, "ubuntu"
set :repository, "git@bitbucket.org:tanyagm/gotribal-backend.git"
set :branch, "develop"
