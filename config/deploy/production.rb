##### production
server "ec2-54-193-20-246.us-west-1.compute.amazonaws.com", :app, :web, :rd, :db, :primary => true

##server "54.254.204.240", :app, :web, :db, :rd, :primary => true

set :deploy_to, "/home/ubuntu/deploy"
set :rails_env, "production"
set :user, "ubuntu"
set :repository, "git@bitbucket.org:tanyagm/gotribal-backend.git"
